copy "..\kernel.bin" "isofiles\boot\"
copy "..\modules\*.bin" "isofiles\boot\modules\"
mkisofs -R -b boot/grub/grldr -no-emul-boot -boot-load-size 4 -boot-info-table -o bootcd.iso isofiles
pause