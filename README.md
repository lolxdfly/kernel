Todo:

- Paging/Heap:
	* offer a heap for userspace to allocate memory
	* change new(malloc) to virt addr return
- Multitasking:
	* find the bug why we cannot use our linkedPriorityQueue implementation
	* taskmain current_task access via param, so that user tasks have access and can map kernel pages with PAGE_ACCESS_SUPERVISOR
- add more syscalls
- Modules
	* move kernelcode into modules
- C/C++ Library
- Shell

later:

- SMP
- Filesystem
- VM86
- ACPI
- Sound
- USB
- VGA
	* SVGA?
- Network
- Paging:
	* PAE
- extend i386 support to other architectures