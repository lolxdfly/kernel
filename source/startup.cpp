/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

#include "VersionConf.h"
#include "kernel/include/panic.h"
#include "kernel/include/heap.h"
#include <stdint.h>
#include <stddef.h>

typedef void (*constructor)();
 
// link.ld
extern "C" constructor start_ctors;
extern "C" constructor end_ctors;
 
extern "C" void initialiseConstructors();
 
//call ctors
void initialiseConstructors()
{
  for(constructor* i = &start_ctors; i != &end_ctors; ++i)
    (*i)();
}

// Stack Smahing Protector
#if UINT32_MAX == UINTPTR_MAX
#define STACK_CHK_GUARD 0XE2DEE396
#else
#define STACK_CHK_GUARD 0X595E9FBD94FDA766
#endif
 
uintptr_t __stack_chk_guard = STACK_CHK_GUARD;

// TODO: check this.. this seems to work on ubuntu with g++ 8.1
/*__attribute__((noreturn)) //GCC expect somehow __stack_chk_fail_ to be non function
void __stack_chk_fail_()
{
	PANIC("SSP", __FILE__, __FUNCTION__, __LINE__);
	__builtin_unreachable();
}*/

// malloc/free
void *operator new(size_t size)
{
	void* m = Heap::malloc(size);
	if(m == nullptr)
		PANIC("Kernel Heap Out Of Memory", __FILE__, __FUNCTION__, __LINE__);
    return m;
} 
void *operator new[](size_t size)
{
	void* m = Heap::malloc(size);
	if(m == nullptr)
		PANIC("Kernel Heap Out Of Memory", __FILE__, __FUNCTION__, __LINE__);
    return m;
}
void operator delete(void *p)
{
    Heap::free(p);
} 
void operator delete[](void *p)
{
    Heap::free(p);
}
void operator delete(void *p, size_t size)
{
	(void)size;
    ::operator delete(p);
} 
void operator delete[](void *p, size_t size)
{
	(void)size;
    ::operator delete[](p);
}
