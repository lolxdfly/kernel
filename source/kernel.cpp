/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

#include "VersionConf.h"

#include <stdlib.h>
#include <stdio.h>

#include "kernel/include/multiboot.h"
#include "kernel/include/video.h"
#include "kernel/include/gdt.h"
#include "kernel/include/idt.h"
#include "kernel/include/port.h"
#include "kernel/include/pitimer.h"
#include "kernel/include/cmos.h"
#include "kernel/include/beep.h"
#include "kernel/include/heap.h"
#include "kernel/include/task.h"
#include "kernel/include/cpuid.h"
#include "kernel/include/panic.h"
#include "kernel/include/paging.h"
#include "kernel/include/ps2.h"
#include "kernel/include/keyboard.h"
#include "kernel/include/memorymap.h"
#include "kernel/include/syscallmng.h"
#include "kernel/include/elf.h"
/*
static void kbc_handler(uint8_t keycode, bool breakcode)
{
	(void)keycode;
	(void)breakcode;
	//screen << "TestKBC: 0x" << itoa(keycode, 16) << " " << (int)breakcode << endl;
	//if(breakcode)
	//	screen << Keyboard::getInstance()->getKey() << endl;
	//	screen << (char)Keyboard::getInstance()->kctoc(keycode);
}
static void time_hander(uint32_t time)
{
	(void)time;
	screen << "TestPIT: " << (int)time << endl;
}*/
void task_A()
{
	for(;;);
	
	//screen << "A" << endl;
}
void task_B()
{
	//for(;;)
	
	screen << "BB" << endl;
}

void main_task()
{
	/***Test Area***/
    //IDT::getInstance()->register_interrupt_handler(IRQ01, &kbc_handler);
	//PITimer::getInstance()->register_update_handler(10000, &time_hander);
	int a = TaskManager::getInstance()->register_user_task(task_A);
	int b = TaskManager::getInstance()->register_kernel_task(task_B);
	//TaskManager::getInstance()->unregister_task(a);
	//for(;;)
	//	screen << "CCCCCCCC" << endl;
	//uint32_t *do_page_fault = (uint32_t*)0xA0000000;
	//*do_page_fault = 5;
	//Keyboard::getInstance()->register_key_handler(&kbc_handler);
	//string s = Keyboard::getInstance()->getInput();
	//screen << endl << "string: " << s << endl;
	//Keyboard::getInstance()->unregister_key_handler(&kbc_handler);
	//screen << "0x" << itoa(MemoryMap::getInstance()->ptrBDA->GraphicInfo.graphicport, 16) << endl;
	//syscall_write("Hello Syscall!");
	//string ret = syscall_read();
	//screen << ret << endl;
	/***************/
	
	//inf loop
	for(;;)
		asm volatile("hlt");	
}

extern "C" void kernel_main(const multiboot_info& multiboot_structur, uint32_t multiboot_magic)
{
	//set print color
	screen.setBackgroundColor(VGA_Color::VGA_COLOR_BLUE);
	screen.setColor(VGA_Color::VGA_COLOR_RED);

	//check multiboot bootloader
	if (multiboot_magic != MULTIBOOT_BOOTLOADER_MAGIC)
		PANIC("Not loaded with multibootable bootloader", __FILE__, __FUNCTION__, __LINE__);

	//write bootloader name
	screen << "Bootloader: " << (const char*)multiboot_structur.boot_loader_name << endl;
	
	//move kernel end behind multiboot modules
	multiboot_mod_list* modules = (multiboot_mod_list*)multiboot_structur.mods_addr;
	MemoryMap::memoryEnd = modules[multiboot_structur.mods_count - 1].mod_end;
	
	//Heap
	screen << "Init Heap... ";
	Heap::kinit();
	screen << "success" << endl;

	//Memory Map
	screen << "Read BDA and IVT...";
	MemoryMap::getInstance()->init();
	screen << "success" << endl;
	
	//Video (read graphics io port from BDA)
	screen.init();
	
	//Global Descriptor Table
	screen << "Init GDT... ";
	GDT::getInstance()->init();
	screen << "success" << endl;
	
	//Interrupt Descriptor Table
	screen << "Init IDT... ";
	IDT::getInstance()->init();
	screen << "success" << endl;
	
	//PS/2
	screen << "Init PS/2... ";
	PS2::getInstance()->init();
	screen << "success" << endl;	
	
	//Keyboard
	screen << "Init Keyboard... ";
	Keyboard::getInstance()->init();
	screen << "success" << endl;

	//Paging
	screen << "Init Paging... ";
	Paging::getInstance()->init();
	screen << "success" << endl;
	
	//CPUID
	screen << "Get CPU Information... ";
	CPUID::getInstance()->getCPUInfo(); //printCPUInfo();
	screen << "success" << endl;
	
	//Programmable Interval Timer
	screen << "Init PIT... ";
	PITimer::getInstance()->init();
	screen << "success" << endl;	
	
	//Multitasking
	screen << "Init TaskManager... ";
	TaskManager::getInstance()->init();
	screen << "success" << endl;
	
	//Syscalls
	screen << "Init Syscalls... ";
	SyscallMng::getInstance()->init();
	screen << "success" << endl;
	
	//CMOS
	//print timestamp
	screen << "Timestamp: ";
	timestamp_t time = CMOS::getInstance()->getTimestamp();
	screen << (int)time.day << "." << (int)time.month << "." << (int)time.year << " " << (int)time.hour << ":" << (int)time.minute << ":" << (int)time.second << endl;
	
	//call multiboot modules
	asm volatile("cli"); //disable multitasking
	for(uint32_t i = 0; i < multiboot_structur.mods_count; i++)
		TaskManager::getInstance()->register_elf_task((void*)modules[i].mod_start, false);

	//register main task
	TaskManager::getInstance()->register_kernel_task(main_task);
	asm volatile("sti"); //enable multitasking

	//inf loop
	for(;;)
		asm volatile("hlt");
}
