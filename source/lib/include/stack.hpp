/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

#pragma once
#include "../../VersionConf.h"
#include <stddef.h>

/**
 * @brief a Stack class used to hold data into stack 
 * 
 * @tparam E the type of the data inside the Stack
 */
template <typename E> class Stack
{	
private:
	/**
	 * @brief The Entry class for the stack
	 * 
	 * @tparam T the Type of the Entry
	 */
	template <typename T> class Entry
	{
	public:
		/// the date hold by the Entry
		const T data; //const T& data;

		/// the next Entry
		Entry<T>* next;
		
		/**
		 * @brief Construct a new Entry object
		 * 
		 * @param elem the data which the Entry is connected with
		 */
		Entry(const T& elem) : data(elem), next(nullptr) { }
	};

	/// the first Entry
	Entry<E>* m_first;

public:
	/**
	 * @brief Construct a new Stack object
	 * 
	 */
	Stack() : m_first(nullptr) { }

	/**
	 * @brief Construct a new Stack object
	 * 
	 * @param s the stack to (flat-)copy from
	 */
	Stack(const Stack& s) : m_first(s.m_first) { }

	/**
	 * @brief Destroy the Stack object
	 * 
	 */
	~Stack();

	/**
	 * @brief push a new element on the stack
	 * 
	 * @param elem the new element
	 */
	void push(const E& elem);

	/**
	 * @brief get the element on top of the stack and remove it
	 * 
	 * @return const E& the top element
	 */
	const E& pop();

	/**
	 * @brief get the data of the first element
	 * 
	 * @return const E& the data of the first element
	 */
	const E& get() const { return m_first->data; }

	/**
	 * @brief remove an element
	 * 
	 * @param elem the element to be removed
	 */
	void remove(const E& elem);
	
	/**
	 * @brief check if stack is empty
	 * 
	 * @return true if empty
	 * @return false if not empty
	 */
	bool empty() const { return !m_first; }
};

template <typename E> Stack<E>::~Stack<E>()
{
	while(!empty())
		pop();		
}

template <typename E> void Stack<E>::push(const E& elem)
{
	Entry<E>* tmp = new Entry<E>(elem);
	tmp->next = m_first;
	m_first = tmp;
}

template <typename E> const E& Stack<E>::pop()
{
	Entry<E>* todel = m_first;
	const E& ret = todel->data;	
	m_first = todel->next;
	delete todel;
	
	return ret;
}

template <typename E> void Stack<E>::remove(const E& elem)
{
	if(empty()) // stack empty?
		return;
	else if(m_first->data == elem) // first elem?
		m_first = m_first->next;
	else if(m_first->next != nullptr) // other elems?
	{
		// search
		Entry<E>* tmp = m_first;		
		while( tmp->next != nullptr && tmp->next->data != elem )
			tmp = tmp->next;
		
		// skip in queue
		if(tmp->next != nullptr)
			tmp->next = tmp->next->next;
	}
}