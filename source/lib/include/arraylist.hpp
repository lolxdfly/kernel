/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

#pragma once
#include "../../VersionConf.h"
#include <stddef.h>
#include <memory.h>
#include "arraycollection.hpp"

/**
 * @brief a dynamic ArrayList class like c++s vector class
 * 
 * @tparam E the type of the data inside the Array List 
 */
template <typename E> class ArrayList : public ArrayCollection<E>
{
public:
	/**
	 * @brief adds a new element to the Array List
	 * 
	 * @param elem the new element to add
	 */
	void add(const E& elem);
	
	/**
	 * @brief overwrites the element at a specific index
	 * 
	 * @param elem the new element
	 * @param idx the index where the element will be replaced
	 */
	void set(const E& elem, size_t idx);
	
	/**
	 * @brief removes an element from the Array List
	 * 
	 * @param elem the element to be removed
	 */
	void remove(const E& elem);

	/**
	 * @brief removes an element from the Array List
	 * 
	 * @param idx the index of the element which has to be removed
	 */
	void remove(size_t idx);
};

template <typename E> void ArrayList<E>::add(const E& elem)
{
	if(this->m_length + 1 >= this->m_maxlength)
		this->resize();
	
	this->m_data[this->m_length++] = elem;
}

template <typename E> void ArrayList<E>::set(const E& elem, size_t idx)
{
	if(idx >= this->m_maxlength)
		this->resize();
	
	this->m_data[idx] = elem;
}

template <typename E> void ArrayList<E>::remove(const E& elem)
{
	size_t i = 0;
	for(; i < this->m_length && elem != this->m_data[i]; i++);
	
	memmove(this->m_data + i, this->m_data + i + 1, sizeof(E) * (this->m_length - i));
	this->m_length--;
}
