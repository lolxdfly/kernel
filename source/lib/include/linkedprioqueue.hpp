/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

#pragma once
#include "../../VersionConf.h"
#include <stddef.h>

/**
 * @brief the Priority classes
 * 
 */
enum class Priority {
	PASSIVE,
	LOW,
	NORMAL,
	HIGH,
	HYPERACTIVE
};

/**
 * @brief a LinkedPriorityQueue class used to hold prioritised data into a linked list
 * 
 * @tparam E the type of the data inside the LinkedPriorityQueue 
 */
template <typename E> class LinkedPriorityQueue
{
private:
	/**
	 * @brief The Entry class for the LinkedPriorityQueue
	 * 
	 * @tparam T the Type of the PrioEntry
	 */
	template <typename T> class PrioEntry
	{
	public:
		/// the date hold by the PrioEntry
		const T data; //const T& data;

		/// the next PrioEntry
		PrioEntry<T>* next;

		/// the priority class
		const Priority prio;

		/// counter used for priority
		size_t cnt;
		
		/**
		 * @brief Construct a new Prio Entry object
		 * 
		 * @param elem elem the data which the Entry is connected with
		 * @param p the Priority class
		 */
		PrioEntry(const T& elem, Priority p) :data(elem), next(nullptr), prio(p), cnt(0) { }
	};
	
	/// the first element of the queue
	PrioEntry<E>* m_first;

	/// the current element
	PrioEntry<E>* m_curelem;

	/// the last element of the queue
	PrioEntry<E>* m_last;
	
	/**
	 * @brief delete the entry
	 * 
	 * @param e the Entry to be deleted
	 */
	void delEntry(PrioEntry<E>* e);
	
public:
	/**
	 * @brief Construct a new Linked Priority Queue object
	 * 
	 */
	LinkedPriorityQueue() : m_first(nullptr), m_curelem(nullptr), m_last(nullptr) { }
	
	/**
	 * @brief Construct a new Linked Priority Queue object
	 * 
	 * @param lq the Linked Priority Queue to (flat-)copy from
	 */
	LinkedPriorityQueue(const LinkedPriorityQueue& lq) : m_first(lq.m_first), m_curelem(lq.m_first), m_last(lq.m_last) { }
	
	/**
	 * @brief Destroy the Linked Priority Queue object
	 * 
	 */
	~LinkedPriorityQueue();
	
	/**
	 * @brief enqueue a new element
	 * 
	 * @param elem elem the new element to be enqueued
	 * @param p the priority of the new element
	 */
	void enqueue(const E& elem, Priority p = Priority::NORMAL);

	/**
	 * @brief get the current element and move on
	 * 
	 * @return const E& the current element
	 */
	const E& next();

	/**
	 * @brief get the current element
	 * 
	 * @return const E& the current element
	 */
	const E& get() const { return m_curelem->data; }

	/**
	 * @brief dequeue the current element
	 * 
	 */
	void dequeue();

	/**
	 * @brief dequeue a given element
	 * 
	 * @param elem the element to be dequeued
	 */
	void dequeue(const E& elem);
	
	/**
	 * @brief check if LinkedPriorityQueue is empty
	 * 
	 * @return true if empty
	 * @return false if not empty
	 */
	bool empty() const { return !m_first; }
};

template <typename E> LinkedPriorityQueue<E>::~LinkedPriorityQueue<E>()
{
	while( m_first->next != nullptr )
	{
		PrioEntry<E>* todel = m_first;
		m_first = m_first->next;
		delete todel;
	}
}

template <typename E> void LinkedPriorityQueue<E>::enqueue(const E& elem, Priority p)
{
	// create first elem and init first, curelem and last
	if(empty())
	{
		m_curelem = m_last = m_first = new PrioEntry<E>(elem, p);
		return;
	}
	
	// add elem
	m_last->next = new PrioEntry<E>(elem, p);	
	m_last = m_last->next;
}

template <typename E> const E& LinkedPriorityQueue<E>::next()
{
	// Todo: better prio algo
	while(m_curelem->cnt != (size_t)m_curelem->prio)
	{
		m_curelem->cnt++;
		
		// continue in queue
		if(m_curelem->next != nullptr)
			m_curelem = m_curelem->next;
		else
			m_curelem = m_first;
	}
	
	// reset cnt
	m_curelem->cnt = 0;
	
	// get return
	const E& ret = m_curelem->data;
	
	// continue in queue
	if(m_curelem->next != nullptr)
		m_curelem = m_curelem->next;
	else
		m_curelem = m_first;	
	
	return ret;
}

template <typename E> void LinkedPriorityQueue<E>::delEntry(PrioEntry<E>* ebefore)
{
	// point to "to delte" Entry
	PrioEntry<E>* todel = ebefore->next;
	
	// link Entry before to "to delte" Entry's next
	ebefore->next = ebefore->next->next;
	
	// free memory of "to delte" Entry
	delete todel;
}

template <typename E> void LinkedPriorityQueue<E>::dequeue()
{
	if(m_first == m_curelem) // first elem?
	{
		PrioEntry<E>* todel = m_first;
		m_first = m_first->next;
		delete todel;
	}
	else
	{	
		// search elem before
		PrioEntry<E>* tmp = m_first;
		while(tmp->next != m_curelem) tmp = tmp->next;
		
		// remove curelem
		delEntry(tmp);
		
		// move on
		m_curelem = m_curelem->next;
	}
}

template <typename E> void LinkedPriorityQueue<E>::dequeue(const E& elem)
{
	if(empty()) // Queue empty?
		return;
	else if(m_first->data == elem) // first elem?
	{
		PrioEntry<E>* todel = m_first;
		m_first = m_first->next;
		delete todel;
	}
	else if(m_first->next != nullptr) // other elems?
	{
		// search
		PrioEntry<E>* tmp = m_first;
		while( tmp->next != nullptr && tmp->next->data != elem )
			tmp = tmp->next;
		
		if(tmp->next != nullptr) // no Entry found
			delEntry(tmp);
	}
	
	// reset curelem if curelem deleted
	if(m_curelem->data == elem)
		m_curelem = m_first;
}