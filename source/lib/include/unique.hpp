/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

#pragma once
#include "../../VersionConf.h"
#include <stdint.h>
#include "stack.hpp"

/**
 * @brief a unique factory to create unique identifier
 * 
 * @tparam E the type of the identifiers
 */
template <typename E> class unique
{
private:
	// the current new unique
	E m_cur;

	// the freed objects which can be used
	Stack<E> m_freeIds;
	
public:
	/**
	 * @brief Construct a new unique object
	 * 
	 */
	unique() : m_cur(0) { }

	/**
	 * @brief Get a Unique object
	 * 
	 * @return E the unique object
	 */
	E getUnique();

	/**
	 * @brief free the unique object so it can be used later on
	 * 
	 * @param id the object to free
	 */
	void freeUnique(E id) { m_freeIds.push(id); }

	/**
	 * @brief clear used and freed unique objects
	 * 
	 */
	void clear();
};

template <typename E> E unique<E>::getUnique()
{
	if(!m_freeIds.empty())
		return m_freeIds.pop();
	
	return m_cur++;
}

template <typename E> void unique<E>::clear()
{
	m_cur = 0;
	while(!m_freeIds.empty())
		m_freeIds.pop();
}
