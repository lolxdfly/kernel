/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

#pragma once
#include "../../VersionConf.h"
#include <stddef.h>

#ifdef __STRINGS
/**
 * @brief a string class to use extended string functions
 * 
 */
class string
{
public:
	/// the char* to hold the data
	char* m_cstr;

	/// the length of the string
	size_t m_length;

	/**
	 * @brief Construct a new string object
	 * 
	 */
	string();

	/**
	 * @brief Construct a new string object
	 * 
	 * @param str 
	 */
	string(const string& str);

	/**
	 * @brief Construct a new string object
	 * 
	 * @param str 
	 */
	string(const char* str);

	/**
	 * @brief Construct a new string object
	 * 
	 * @param c 
	 */
	string(const char c);

	/**
	 * @brief Destroy the string object
	 * 
	 */
	~string();
	
	/// get char* of the string
	operator char*() const { return m_cstr; }

	/// get const char* of the string
	operator const char*() const { return m_cstr; }

	/// concatenate strings
	string operator+(const string& str) const;

	/// add string
	string operator+=(const string& str);

	/// set string
	string operator=(const string& str);

	// char access via []
    char operator[](int i) const { return m_cstr[i]; }

	// char& access via []
    char& operator[](int i){ return m_cstr[i]; }
	
	/**
	 * @brief get pointer to char array
	 * 
	 * @return const char* the pointer to the char array
	 */
	const char* c_str() const { return m_cstr; }

	/**
	 * @brief get the length of the string
	 * 
	 * @return size_t the length of the string
	 */
	size_t length() const { return m_length; }

	/**
	 * @brief compares two strings
	 * 
	 * @param str the other string to be compared
	 * @return <0 the first character that does not match has a lower value
	 * @return  0 the string are equal
	 * @return >0 the first character that does not match has a greater value
	 */
	int compare(string str);
};
#else
typedef const char* string;
#endif // __STRINGS