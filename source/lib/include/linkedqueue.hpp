/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

#pragma once
#include "../../VersionConf.h"
#include <stddef.h>

/**
 * @brief a LinkedQueue class used to hold data into a linked list
 * 
 * @tparam E the type of the data inside the LinkedQueue 
 */
template <typename E> class LinkedQueue
{	
private:
	/**
	 * @brief The Entry class for the LinkedQueue
	 * 
	 * @tparam T the Type of the Entry
	 */
	template <typename T> class Entry
	{
	public:
		/// the date hold by the Entry
		const T data; //const T& data;

		/// the next Entry
		Entry<T>* next;
		
		/**
		 * @brief Construct a new Entry object
		 * 
		 * @param elem the data which the Entry is connected with
		 */
		Entry(const T& elem) : data(elem), next(nullptr) { }
	};

	/// the first element of the queue
	Entry<E>* m_first;

	/// the current element
	Entry<E>* m_curelem;

	/// the last element of the queue
	Entry<E>* m_last;
	
	/**
	 * @brief delete the entry
	 * 
	 * @param e the Entry to be deleted
	 */
	void delEntry(Entry<E>* e);

public:
	/**
	 * @brief Construct a new Linked Queue object
	 * 
	 */
	LinkedQueue() : m_first(nullptr), m_curelem(nullptr), m_last(nullptr) { }

	/**
	 * @brief Construct a new Linked Queue object
	 * 
	 * @param lq the Linked Queue to (flat-)copy from
	 */
	LinkedQueue(const LinkedQueue& lq) : m_first(lq.m_first), m_curelem(lq.m_first), m_last(lq.m_last) { }

	/**
	 * @brief Destroy the Linked Queue object
	 * 
	 */
	~LinkedQueue();

	/**
	 * @brief enqueue a new element
	 * 
	 * @param elem the new element to be enqueued
	 */
	void enqueue(const E& elem);

	/**
	 * @brief get the current element and move on
	 * 
	 * @return const E& the current element
	 */
	const E& next();

	/**
	 * @brief get the current element
	 * 
	 * @return const E& the current element
	 */
	const E& get() const { return m_curelem->data; }

	/**
	 * @brief dequeue the current element
	 * 
	 */
	void dequeue();

	/**
	 * @brief dequeue a given element
	 * 
	 * @param elem the element to be dequeued
	 */
	void dequeue(const E& elem);

	/**
	 * @brief check if Queue is empty
	 * 
	 * @return true if empty
	 * @return false if not empty
	 */
	bool empty() const { return !m_first; }
};

template <typename E> LinkedQueue<E>::~LinkedQueue<E>()
{
	while( m_first->next != nullptr )
	{
		Entry<E>* todel = m_first;
		m_first = m_first->next;
		delete todel;
	}
}

template <typename E> void LinkedQueue<E>::enqueue(const E& elem)
{
	// create first elem and init first, m_curelem and last
	if(empty())
	{
		m_curelem = m_last = m_first = new Entry<E>(elem);
		return;
	}
	
	// add elem
	m_last->next = new Entry<E>(elem);	
	m_last = m_last->next;	
}

template <typename E> const E& LinkedQueue<E>::next()
{
	// get return
	const E& ret = m_curelem->data;
	
	// continue in queue
	if(m_curelem->next != nullptr)
		m_curelem = m_curelem->next;
	else
		m_curelem = m_first;	
	
	return ret;
}

template <typename E> void LinkedQueue<E>::delEntry(Entry<E>* ebefore)
{
	// point to "to delte" Entry
	Entry<E>* todel = ebefore->next;
	
	// link Entry before to "to delte" Entry's next
	ebefore->next = ebefore->next->next;
	
	// free memory of "to delte" Entry
	delete todel;
}

template <typename E> void LinkedQueue<E>::dequeue()
{
	if(m_first == m_curelem) // first elem?
	{
		Entry<E>* todel = m_first;
		m_first = m_first->next;
		delete todel;
	}
	else
	{	
		// search elem before
		Entry<E>* tmp = m_first;
		while(tmp->next != m_curelem) tmp = tmp->next;
		
		// remove curelem
		delEntry(tmp);
		
		// move on
		m_curelem = m_curelem->next;
	}
}

template <typename E> void LinkedQueue<E>::dequeue(const E& elem)
{
	if(empty()) // Queue empty?
		return;
	else if(m_first->data == elem) // first elem?
	{
		Entry<E>* todel = m_first;
		m_first = m_first->next;
		delete todel;
	}
	else if(m_first->next != nullptr) // other elems?
	{
		// search
		Entry<E>* tmp = m_first;
		while( tmp->next != nullptr && tmp->next->data != elem )
			tmp = tmp->next;
		
		if(tmp->next != nullptr) // no Entry found
			delEntry(tmp);
	}
	
	// reset curelem if curelem deleted
	if(m_curelem->data == elem)
		m_curelem = m_first;
}