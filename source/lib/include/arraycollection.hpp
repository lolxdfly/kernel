/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

#pragma once
#include "../../VersionConf.h"
#include <stddef.h>
#include <memory.h>

#define DEFAULT_SIZE 10

/**
 * @brief a Collection base class to hold data in dynamic array structures
 * 
 * @tparam E the type of the data inside the Collection 
 */
template <typename E> class ArrayCollection
{
protected:
	/// Collection content
	E* m_data;

	/// max number of elements
	size_t m_maxlength;
	
	/// number of elements
	size_t m_length;
	
	/**
	 * @brief resizes the Array Colletion to n^2 of it's size
	 * 
	 * @tparam E type of the Array Collection
	 */
	void resize();
	
public:
	/// ctor with default size
	ArrayCollection() : ArrayCollection(DEFAULT_SIZE) { };
	
	/**
	 * @brief Construct a new Array Collection< E>:: Array Collection object
	 * 
	 * @tparam E type of the Array Collection
	 * @param size the expected size of the Array Collection
	 */
	ArrayCollection(size_t size);
	
	/**
	 * @brief Construct a new Array Collection< E>:: Array Collection object from an existing one
	 * 
	 * @tparam E type of the Array Collection
	 * @param l the existing Array Collection to copy from
	 */
	ArrayCollection(const ArrayCollection<E>& l);
	
	/**
	 * @brief Destroy the Array Collection< E>:: Array Collection object
	 * 
	 * @tparam E type of the Array Collection
	 */
	~ArrayCollection();
	
	/// access via []
    E& operator[](size_t idx){ return m_data[idx]; }

	/**
	 * @brief get elem at given index of Collection
	 * 
	 * @param idx the index of the element
	 * @return E the element
	 */
	E get(size_t idx) const { return m_data[idx]; }
	
	/**
	 * @brief get the length of the Collection
	 * 
	 * @return size_t the length
	 */
	size_t size() const { return m_length; }
	
	/**
	 * @brief check if Collection is empty
	 * 
	 * @return true if empty
	 * @return false if not empty
	 */
	bool empty() const { return (m_length == 0); }
	
	/**
	 * @brief optimizes the Array Colletion at the current size
	 * 
	 * @tparam E type of the Array Collection
	 */
	void optimize();
};

template <typename E> ArrayCollection<E>::ArrayCollection(size_t size)
{
	m_data = new E[size];
	m_length = 0;
	m_maxlength = size;
}

template <typename E> ArrayCollection<E>::ArrayCollection(const ArrayCollection<E>& l)
{
	m_data = new E[l.m_length];
	m_length = l.m_length;
	m_maxlength = l.m_length;
	
	memcpy(m_data, l.m_data, m_length * sizeof(E));
}

template <typename E> ArrayCollection<E>::~ArrayCollection()
{
	delete[] m_data;
}

template <typename E> void ArrayCollection<E>::resize()
{
	const E* oldm_data = m_data;
	m_maxlength = m_maxlength * m_maxlength; // n^2
	m_data = new E[m_maxlength];
	
	memcpy(m_data, oldm_data, m_length * sizeof(E));
	delete[] oldm_data;
}

template <typename E> void ArrayCollection<E>::optimize()
{
	const E* oldm_data = m_data;
	m_data = new E[m_length]; // match number of elements
	
	memcpy(m_data, oldm_data, m_length * sizeof(E));
	delete[] oldm_data;
}