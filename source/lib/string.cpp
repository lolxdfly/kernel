/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

#include "../VersionConf.h"

#ifdef __STRINGS

#include <string.h>
#include "include/string.h"

string::string()
{
	m_cstr = nullptr;
	m_length = 0;
}

string::~string()
{
	delete[] m_cstr;
}

string::string(const string& str)
{
	m_cstr = new char[str.m_length];
	strcpy(m_cstr, str.m_cstr);
	m_length = str.m_length;
}

string::string(const char* str)
{
	m_length = strlen(str) + 1;
	m_cstr = new char[m_length];
	strcpy(m_cstr, str);
	m_cstr[m_length - 1] = '\0';
}

string::string(const char c)
{
	m_length = 2;
	m_cstr = new char[m_length];
	m_cstr[0] = c;
	m_cstr[1] = '\0';
}

string string::operator+(const string& str) const
{
	string s;
	s.m_length = strlen(m_cstr) + strlen(str.m_cstr);
	s.m_cstr = new char[s.m_length];
	strcpy(s.m_cstr, m_cstr);
	strcat(s.m_cstr, str.m_cstr);
	return s;
}

string string::operator+=(const string& str)
{
	if(m_cstr)
	{	
		m_length = strlen(m_cstr) + strlen(str.m_cstr);
		char* old = m_cstr;
		m_cstr = new char[m_length];
		strcpy(m_cstr, old);
		strcat(m_cstr, str.m_cstr);
		delete[] old;
	}
	else
	{
		m_cstr = new char[str.m_length];
		strcpy(m_cstr, str.m_cstr);
		m_length = str.m_length;
	}
	return *this;
}

string string::operator=(const string& str)
{
	if(m_cstr)
		delete[] m_cstr;
	
	m_cstr = new char[str.m_length];
	strcpy(m_cstr, str.m_cstr);
	m_length = str.m_length;
	
	return *this;
}

int string::compare(string str)
{
	return strcmp(m_cstr, str.m_cstr);
}
#endif // __STRINGS
