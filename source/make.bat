::compile assembler stuff
i686-elf-as boot.s -o out/boot.o
i686-elf-as kernel/gdtflush.s -o out/gdtflush.o
i686-elf-as kernel/idtentries.s -o out/idtentries.o

::include libc
set CPATH=%CPATH%;%CD%\libc\src\include

::compile cpp stuff
i686-elf-g++ -c kernel/video.cpp -o out/video.o -fstack-protector -fno-use-cxa-atexit -ffreestanding -O2 -Wall -Wextra -fno-exceptions -fno-rtti
i686-elf-g++ -c kernel.cpp -o out/kernel.o -fstack-protector -fno-use-cxa-atexit -ffreestanding -O2 -Wall -Wextra -fno-exceptions -fno-rtti
i686-elf-g++ -c startup.cpp -o out/startup.o -fstack-protector -fno-use-cxa-atexit -ffreestanding -O2 -Wall -Wextra -fno-exceptions -fno-rtti
i686-elf-g++ -c kernel/gdt.cpp -o out/gdt.o -fstack-protector -fno-use-cxa-atexit -ffreestanding -O2 -Wall -Wextra -fno-exceptions -fno-rtti
i686-elf-g++ -c kernel/panic.cpp -o out/panic.o -fstack-protector -fno-use-cxa-atexit -ffreestanding -O2 -Wall -Wextra -fno-exceptions -fno-rtti
i686-elf-g++ -c kernel/idt.cpp -o out/idt.o -fstack-protector -fno-use-cxa-atexit -ffreestanding -O2 -Wall -Wextra -fno-exceptions -fno-rtti
i686-elf-g++ -c kernel/pitimer.cpp -o out/pitimer.o -fstack-protector -fno-use-cxa-atexit -ffreestanding -O2 -Wall -Wextra -fno-exceptions -fno-rtti
i686-elf-g++ -c kernel/cmos.cpp -o out/cmos.o -fstack-protector -fno-use-cxa-atexit -ffreestanding -O2 -Wall -Wextra -fno-exceptions -fno-rtti
i686-elf-g++ -c kernel/beep.cpp -o out/beep.o -fstack-protector -fno-use-cxa-atexit -ffreestanding -O2 -Wall -Wextra -fno-exceptions -fno-rtti
i686-elf-g++ -c kernel/heap.cpp -o out/heap.o -fstack-protector -fno-use-cxa-atexit -ffreestanding -O2 -Wall -Wextra -fno-exceptions -fno-rtti
i686-elf-g++ -c kernel/task.cpp -o out/task.o -fstack-protector -fno-use-cxa-atexit -ffreestanding -O2 -Wall -Wextra -fno-exceptions -fno-rtti
i686-elf-g++ -c kernel/cpuid.cpp -o out/cpuid.o -fstack-protector -fno-use-cxa-atexit -ffreestanding -O2 -Wall -Wextra -fno-exceptions -fno-rtti
i686-elf-g++ -c kernel/paging.cpp -o out/paging.o -fstack-protector -fno-use-cxa-atexit -ffreestanding -O2 -Wall -Wextra -fno-exceptions -fno-rtti
i686-elf-g++ -c kernel/ps2.cpp -o out/ps2.o -fstack-protector -fno-use-cxa-atexit -ffreestanding -O2 -Wall -Wextra -fno-exceptions -fno-rtti
i686-elf-g++ -c kernel/keyboard.cpp -o out/keyboard.o -fstack-protector -fno-use-cxa-atexit -ffreestanding -O2 -Wall -Wextra -fno-exceptions -fno-rtti
i686-elf-g++ -c kernel/memorymap.cpp -o out/memorymap.o -fstack-protector -fno-use-cxa-atexit -ffreestanding -O2 -Wall -Wextra -fno-exceptions -fno-rtti
i686-elf-g++ -c kernel/syscall.cpp -o out/syscall.o -fstack-protector -fno-use-cxa-atexit -ffreestanding -O2 -Wall -Wextra -fno-exceptions -fno-rtti
i686-elf-g++ -c kernel/syscallmng.cpp -o out/syscallmng.o -fstack-protector -fno-use-cxa-atexit -ffreestanding -O2 -Wall -Wextra -fno-exceptions -fno-rtti

::link
i686-elf-g++ -T linker.ld -o ../kernel.bin -fstack-protector -fno-use-cxa-atexit -ffreestanding -O2 -lgcc -nostdlib^
	out/libkc.a^
	out/ps2.o^
	out/syscallmng.o^
	out/syscall.o^
	out/keyboard.o^
	out/memorymap.o^
	out/boot.o^
	out/startup.o^
	out/cpuid.o^
	out/panic.o^
	out/string.o^
	out/paging.o^
	out/heap.o^
	out/video.o^
	out/gdtflush.o^
	out/gdt.o^
	out/cmos.o^
	out/idtentries.o^
	out/idt.o^
	out/task.o^
	out/beep.o^
	out/pitimer.o^
	out/kernel.o

PAUSE