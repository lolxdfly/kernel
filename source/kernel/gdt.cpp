/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

#include "../VersionConf.h"
#include "include/gdt.h"

// Lets us access our ASM functions from our C++ code.
extern "C" void gdt_flush(uint32_t);

GDT* GDT::_instance = nullptr;
uint32_t GDT::tss[32] = { 0, 0, 0x10 };

void GDT::init()
{
	// set limit to amout of mem - 1
    m_gdt_ptr.limit = (sizeof(gdt_entry_t) * NUM_GDT_ENTRIES) - 1;
	
	// let base point to the entry-table
    m_gdt_ptr.base  = (uint32_t)&m_gdt_entries;	
	
	// init the table (flat setup)
    setgate(0, 0,				0, 				0, 0);                // Null segment
    setgate(1, 0,				0xFFFFFFFF, 	GDT_ACCESS_PRESENT | GDT_ACCESS_PRIVILEGE_RING0 | GDT_ACCESS_CODE_DATA | GDT_ACCESS_CODE | GDT_ACCESS_DIRECTION_UP | GDT_ACCESS_READ_WRITE, GDT_GRAN_4KIB | GDT_GRAN_32BIT_PROTECTED | GDT_GRAN_PROTECTED); // Code segment
    setgate(2, 0,				0xFFFFFFFF, 	GDT_ACCESS_PRESENT | GDT_ACCESS_PRIVILEGE_RING0 | GDT_ACCESS_CODE_DATA | GDT_ACCESS_DATA | GDT_ACCESS_DIRECTION_UP | GDT_ACCESS_READ_WRITE, GDT_GRAN_4KIB | GDT_GRAN_32BIT_PROTECTED | GDT_GRAN_PROTECTED); // Data segment
    setgate(3, 0,				0xFFFFFFFF, 	GDT_ACCESS_PRESENT | GDT_ACCESS_PRIVILEGE_RING3 | GDT_ACCESS_CODE_DATA | GDT_ACCESS_CODE | GDT_ACCESS_DIRECTION_UP | GDT_ACCESS_READ_WRITE, GDT_GRAN_4KIB | GDT_GRAN_32BIT_PROTECTED | GDT_GRAN_PROTECTED); // User mode code segment
    setgate(4, 0,				0xFFFFFFFF, 	GDT_ACCESS_PRESENT | GDT_ACCESS_PRIVILEGE_RING3 | GDT_ACCESS_CODE_DATA | GDT_ACCESS_DATA | GDT_ACCESS_DIRECTION_UP | GDT_ACCESS_READ_WRITE, GDT_GRAN_4KIB | GDT_GRAN_32BIT_PROTECTED | GDT_GRAN_PROTECTED); // User mode data segment
	setgate(5, (uint32_t)tss, 	sizeof(tss), 	GDT_ACCESS_PRESENT | GDT_ACCESS_PRIVILEGE_RING3 | GDT_ACCESS_GATE_TSS | 0x9 /*Segment Type*/, 0x00); // TSS segment
    
	// flush the gdt
    gdt_flush((uint32_t)&m_gdt_ptr);
	
	// Index is (5 << 3)
	asm volatile("ltr %%ax" : : "a" (5 << 3));
}

void GDT::setgate(uint8_t num, uint32_t base, uint32_t limit, uint8_t access, uint8_t gran)
{
	// more info of entry-structure: http://wiki.osdev.org/Global_Descriptor_Table
    m_gdt_entries[num].base_low    = (base & 0xFFFF);
    m_gdt_entries[num].base_middle = (base >> 16) & 0xFF;
    m_gdt_entries[num].base_high   = (base >> 24) & 0xFF;

    m_gdt_entries[num].limit_low   = (limit & 0xFFFF);
    m_gdt_entries[num].granularity = (limit >> 16) & 0x0F;
    
    m_gdt_entries[num].granularity |= (gran <<= 4) & 0xF0; // XXXX 0000 (only use first 4 bit)
    m_gdt_entries[num].access      = access;	
}