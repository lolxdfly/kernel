/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

#include "../VersionConf.h"
#include "include/panic.h"
#include <stdio.h>

void PANIC(const char* message, const char* file, const char* function, int line)
{
    // We encountered a massive problem and have to stop.
    asm volatile("cli"); // Disable interrupts.

	// print panic message
	printf("PANIC: %s\nat %s in function %s:%i", message, file, function, line);

    // Halt by going into an infinite loop.
    for(;;) asm volatile("hlt");
}