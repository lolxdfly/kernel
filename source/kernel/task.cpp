/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

#include "../VersionConf.h"
#include <memory.h>
#include <string.h>
#include "include/task.h"
#include "include/paging.h"
#include "include/syscall.h"
#include "include/memorymap.h"

TaskManager* TaskManager::_instance = nullptr;
task_t* TaskManager::m_current_task = nullptr;
task_t* TaskManager::m_first_task = nullptr;

registers_t& TaskManager::schedule_task(registers_t &regs)
{
	if(TaskManager::m_first_task == nullptr)
		return regs;
	
    if (TaskManager::m_current_task != nullptr)
        TaskManager::m_current_task->state = &regs;
	
    if (TaskManager::m_current_task == nullptr)
        TaskManager::m_current_task = m_first_task;
    else
	{
		do
		{
			TaskManager::m_current_task = TaskManager::m_current_task->next;
			if (TaskManager::m_current_task == nullptr)
				TaskManager::m_current_task = m_first_task;
		}
		while(TaskManager::m_current_task->blocked);
    }
	
	Paging::activatePageDirectory(TaskManager::m_current_task->page_directory);
	return *TaskManager::m_current_task->state;
}

//Todo:
//	find the bug why we cannot use our linkedPriorityQueue implementation
//	map user task pages

void TaskManager::init()
{		

}

void taskmain_user()
{
	//NOTE: This might cause the Pagefault when PAGE_ACCESS_SUPERVISOR is set because the memory where task_start_info_t is
	//allocated is still inside the kernelmemory => TODO: allocate task_start_info_t in the taskmemory of the task itself
	task_start_info_t* tsi = (task_start_info_t*)syscall_getTaskInfo();
	
	//call global ctors
	if(tsi->ctors != nullptr)
	{
		uint32_t* ctor_table = (uint32_t*)tsi->ctors->sh_addr;
		for(unsigned int i = 0; i < tsi->ctors->sh_size / sizeof(void *); i++)
		{
			task_func_t ctor = (task_func_t)ctor_table[i];
			ctor();
		}
	}
	
	//call entry
	tsi->func();
	
	//call global dtors
	if(tsi->dtors != nullptr)
	{
		uint32_t* dtor_table = (uint32_t*)tsi->dtors->sh_addr;
		for(unsigned int i = 0; i < tsi->dtors->sh_size / sizeof(void *); i++)
		{
			task_func_t dtor = (task_func_t)dtor_table[i];
			dtor();
		}
	}

	//todo: move this to deltask (kernelmode)
	//free page directory
	//Paging::getInstance()->freePageDirectory(TaskManager::m_current_task->page_directory);
			
	//remove task
	syscall_killProcess(tsi->pid);
	
	//wait until task is completely removed
	for(;;);
}

void taskmain_kernel()
{
	//call global ctors
	if(TaskManager::m_current_task->info->ctors != nullptr)
	{
		uint32_t* ctor_table = (uint32_t*)TaskManager::m_current_task->info->ctors->sh_addr;
		for(unsigned int i = 0; i < TaskManager::m_current_task->info->ctors->sh_size / sizeof(void *); i++)
		{
			task_func_t ctor = (task_func_t)ctor_table[i];
			ctor();
		}
	}
	
	//call entry
	TaskManager::m_current_task->info->func();
	
	//call global dtors
	if(TaskManager::m_current_task->info->dtors != nullptr)
	{
		uint32_t* dtor_table = (uint32_t*)TaskManager::m_current_task->info->dtors->sh_addr;
		for(unsigned int i = 0; i < TaskManager::m_current_task->info->dtors->sh_size / sizeof(void *); i++)
		{
			task_func_t dtor = (task_func_t)dtor_table[i];
			dtor();
		}
	}

	//unregister task after task finish
	TaskManager::getInstance()->unregister_task(TaskManager::m_current_task->info->pid);
		
	//wait until task is completely removed
	for(;;)
		asm volatile("hlt");
}

task_t* TaskManager::create_kernel_task(task_func_t task, bool blocked)
{
	//create new task
	task_t* t = new task_t();
	
	t->stack = new uint8_t[TASK_STACK_SIZE];
	t->user_stack = nullptr;
	
	//set up registers
	registers_t reg;
    reg.eax = 0;
    reg.ebx = 0;
    reg.ecx = 0;
    reg.edx = 0;
    reg.esi = 0;
    reg.edi = 0;
    reg.ebp = 0;
	//reg.esp = (uint32_t)user_stack;
	reg.eip = (uint32_t)taskmain_kernel;
	reg.cs = 0x08; //select kernel code segment
	reg.ss = 0x10; //select kernel data segment
	reg.eflags = 0x202; //0010 0000 0010 (https://en.wikipedia.org/wiki/FLAGS_register) => IF = 1 (interrupts enabled)
		
	//set up stack
	t->state = (registers_t*) (t->stack + TASK_STACK_SIZE - sizeof(registers_t));
	*t->state = reg;
	
	t->info = new task_start_info_t();
	
	//set entry point
	t->info->func = task;
	
	//set pid
	t->info->pid = m_PIDGen.getUnique();
	
	//set type and blocked
	t->type = tasktype_t::kernel;
	t->blocked = blocked;
	
	//copy kernel Page Directory
	t->page_directory = Paging::getInstance()->getKernelPageDirectory();
	
	//enqueue
	asm volatile("cli"); //thread safety
    t->next = m_first_task;
    m_first_task = t;
	asm volatile("sti"); //thread safety
	return t;
}

task_t* TaskManager::create_user_task(task_func_t task, bool blocked)
{
	//create new task
	task_t* t = new task_t();
	
	t->stack = new uint8_t[TASK_STACK_SIZE];
	t->user_stack = new uint8_t[TASK_STACK_SIZE];
	
	//set up registers
	registers_t reg;
    reg.eax = 0;
    reg.ebx = 0;
    reg.ecx = 0;
    reg.edx = 0;
    reg.esi = 0;
    reg.edi = 0;
    reg.ebp = 0;
	reg.esp = (uint32_t)t->user_stack + TASK_STACK_SIZE;
	reg.eip = (uint32_t)taskmain_user;
	reg.cs = 0x18 | 0x03; //select User code segment
	reg.ss = 0x20 | 0x03; //select User data segment
	reg.eflags = 0x202; //0010 0000 0010 (https://en.wikipedia.org/wiki/FLAGS_register) => IF = 1 (interrupts enabled)
		
	//set up stack
	t->state = (registers_t*) (t->stack + TASK_STACK_SIZE - sizeof(registers_t));
	*t->state = reg;
	
	t->info = new task_start_info_t();
	
	//set entry point
	t->info->func = task;
	
	//set pid
	t->info->pid = m_PIDGen.getUnique();
	
	//set type and blocked
	t->type = tasktype_t::user;
	t->blocked = blocked;
	
	//create new Page Directory
	t->page_directory = Paging::getInstance()->allocPageDirectory(PAGE_ACCESS_SUPERVISOR | PAGE_READ_ONLY | PAGE_NOT_PRESENT);
	
	//mapping the kernel
    for(uint32_t i = 0; i < MemoryMap::memoryEnd; i += 0x1000)
		Paging::getInstance()->mapPage(t->page_directory, i, i, PAGE_ACCESS_ALL | PAGE_READ_WRITE); //this should be PAGE_ACCESS_SUPERVISOR
	
	//map 1 MB for the task
	//why has our virt_addr to be MemoryMap::memoryEnd and not 0x300000(ph->p_vaddr)??
    for(uint32_t i = MemoryMap::memoryEnd; i < MemoryMap::memoryEnd + 0x100000; i += 0x1000)
		Paging::getInstance()->mapPage(t->page_directory, i + (0x100000 * t->info->pid), i, PAGE_ACCESS_ALL | PAGE_READ_WRITE);
	//map task info
	Paging::getInstance()->mapPage(t->page_directory, (uint32_t)&t->info, (uint32_t)&t->info, PAGE_ACCESS_ALL | PAGE_READ_WRITE);
	
	//enqueue
	asm volatile("cli"); //thread safety
    t->next = m_first_task;
    m_first_task = t;
	asm volatile("sti"); //thread safety
	return t;
}

int TaskManager::register_kernel_task(task_func_t task, bool blocked)
{
	return create_kernel_task(task, blocked)->info->pid;
}

int TaskManager::register_user_task(task_func_t task, bool blocked)
{
	return create_user_task(task, blocked)->info->pid;
}

int TaskManager::register_elf_task(void* image, bool kernel)
{
	elf_header_32bit_t* header = (elf_header_32bit_t*)image;

	//check ELF format
	if(header->magic != ELF_MAGIC)
		return -1;
	
	//create new task
	task_t* t = kernel ? create_kernel_task((task_func_t)header->e_entry, true) : create_user_task((task_func_t)header->e_entry, true);
	
	//get program header
	elf_program_header_32bit_t* ph = (elf_program_header_32bit_t*) (((char*) image) + header->e_phoff);
	for(int i = 0; i < header->e_phnum; i++, ph++)
	{
		//check for load type
		if (ph->p_type != ELF_PH_SEGTYPE_LOAD)
			continue;

		//get dest and src
		void* dest = (void*)(ph->p_vaddr + (t->info->pid * 0x100000));
		void* src = ((char*)image) + ph->p_offset;
		
		//zero the dest
		memset(dest, 0, ph->p_memsz);
		
		//cpy code to dest
		memcpy(dest, src, ph->p_filesz);
	}	
	
	//get section header
	elf_section_header_32bit_t* sh = (elf_section_header_32bit_t*) (((char*) image) + header->e_shoff);
	//get string table
	uint32_t st = sh[header->e_shstrndex].sh_offset;
	for(int i = 0; i < header->e_shnum; i++, sh++)
	{
		//get name of section
		char* name = (char*)(((char*)image) + st + sh->sh_name);
				
		//check for ctor
		if(strcmp(name, (char*)".ctors") == 0)
			t->info->ctors = sh;
		else if(strcmp(name, (char*)".dtors") == 0)
			t->info->dtors = sh;
	}	

	//start the task
	t->blocked = false;
	
	return t->info->pid;
}

void TaskManager::delete_task(task_t* tbefore)
{
	//point to "to delte" task
	task_t* todel = tbefore->next;
	
	//free pid
	m_PIDGen.freeUnique(todel->info->pid);	
	
	asm volatile("cli"); //thread safety
	
	//link task before to "to delte" task's next
	tbefore->next = tbefore->next->next;
	
	asm volatile("sti"); //thread safety
	
	//free memory of "to delte" task
	delete todel->stack;
	if(todel->user_stack != nullptr)
		delete todel->user_stack;
	delete todel;
}

void TaskManager::unregister_task(uint16_t pid)
{
	if(m_first_task->info->pid == pid)
	{
		task_t* todel = m_first_task;
		asm volatile("cli"); //thread safety
		m_first_task = m_first_task->next;
		asm volatile("sti"); //thread safety
		m_PIDGen.freeUnique(todel->info->pid);
		delete todel;
	}
	else if(m_first_task->next != nullptr)
	{
		//search
		task_t* tmp = m_first_task;
		while( tmp->next != nullptr && tmp->next->info->pid != pid )
			tmp = tmp->next;
	
		if(tmp->next != nullptr)
			delete_task(tmp);
	}
}

void TaskManager::block_task(uint16_t pid) const
{
	task_t* tmp = m_first_task;
	while(tmp->next != nullptr && tmp->info->pid != pid)
		tmp = tmp->next;
	
	if(tmp->info->pid == pid)
		tmp->blocked = true;		
}

void TaskManager::unblock_task(uint16_t pid) const
{
	task_t* tmp = m_first_task;
	while(tmp->next != nullptr && tmp->info->pid != pid)
		tmp = tmp->next;
	
	if(tmp->info->pid == pid)
		tmp->blocked = false;		
}
