/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

#include "../VersionConf.h"
#include <stdint.h>
#include <memory.h>
#include <stdlib.h>
#include "include/paging.h"
#include "include/idt.h"
#include "include/video.h"
#include "include/panic.h"
#include "include/heap.h"
#include "include/memorymap.h"

Paging* Paging::_instance = nullptr;

static void page_fault(registers_t &regs)
{
    // A page fault has occurred.
    // The faulting address is stored in the CR2 register.
    uint32_t faulting_address;
    asm ("mov %%cr2, %0" : "=r" (faulting_address));
    
    // The error code gives us details of what happened.
    int present   = !(regs.error & 0x1); // Page not present
    int rw = regs.error & 0x2;           // Write operation?
    int us = regs.error & 0x4;           // Processor was in user-mode?
    int reserved = regs.error & 0x8;     // Overwritten CPU-reserved bits of page entry?
    int id = regs.error & 0x10;          // Caused by an instruction fetch?

    // Output an error message.
	screen.setColor(VGA_Color::VGA_COLOR_RED);
	screen.setBackgroundColor(VGA_Color::VGA_COLOR_BLACK);
    screen << "Page fault! ( ";
    if (present) screen << "present ";
    if (rw) screen << "read-only ";
    if (us) screen << "user-mode ";
    if (reserved) screen << "reserved ";
    if (id) screen << "instruction fetch ";
    screen << ") at 0x" << lltoa(faulting_address, 16) << endl;
    PANIC("Page fault", __FILE__, __FUNCTION__, __LINE__);
}

/*
Todo:
	- extend to PAE
*/

void Paging::init()
{
	// create Kernel Page Directory
	m_kernel_page_directory = allocPageDirectory(PAGE_ACCESS_SUPERVISOR | PAGE_READ_ONLY | PAGE_NOT_PRESENT);
	
	// mapping the first 14 MiB (Todo: map more)
    for(uint32_t i = 0; i < EXT_MEM_1_END; i += 0x1000)
		mapPage(m_kernel_page_directory, i, i, PAGE_ACCESS_SUPERVISOR | PAGE_READ_WRITE);
	
	// activate kernel_page_directory
	activatePageDirectory(m_kernel_page_directory);
	
    // Before we enable paging, we must register our page fault handler
    IDT::getInstance()->register_interrupt_handler(ISR14, &page_fault);
	
	// reads cr0, switches the "paging enable" bit, and writes it back.
	uint32_t cr0;
	asm volatile("mov %%cr0, %0" : "=r" (cr0));
	cr0 |= 0x80000000;
	asm volatile("mov %0, %%cr0" : : "r" (cr0));
}

uint32_t* Paging::allocPageDirectory(uint8_t flags)
{
	// heap has to allocates aligned page_directory
	uint32_t* page_directory = (uint32_t*)Heap::malloc(1024 * sizeof(uint32_t), 12);
	
	// init Page Directory
	memset((uint8_t*)page_directory, flags, 1024);
	
	// return Page Directory
	return page_directory;
}

void Paging::mapPage(uint32_t* page_directory, uint32_t phyaddr, uint32_t virtaddr, uint8_t flags)
{
	// get indices
    uint32_t pageidx = virtaddr / 0x1000;
    uint32_t pdidx = pageidx / 1024;
    uint32_t ptidx = pageidx % 1024;
	uint32_t* page_table = nullptr;
	
	// check if page is already allocated
	if(page_directory[pdidx] & PAGE_PRESENT)
		page_table = (uint32_t*)GET_PAGE(page_directory[pdidx]);
	else
	{
		// allocate new page
		page_table = (uint32_t*)Heap::malloc(1024 * sizeof(uint32_t), 12);
		memset((uint8_t*)page_table, 0, 1024);
		
		// set the page to the desired page directory place
		page_directory[pdidx] = (uint32_t)page_table | flags | PAGE_PRESENT;		
	}
	
	// map phys addr to page table
	page_table[ptidx] = phyaddr | flags | PAGE_PRESENT;
	
	// invalidate TLB Entry
	asm volatile("invlpg (%0)" : : "b"((void*)virtaddr) : "memory");
}

void Paging::activatePageDirectory(uint32_t* page_directory)
{	
	// moves page_directory into the cr3 register
	asm volatile("mov %0, %%cr3" : : "r" (page_directory));	
}

void Paging::freePageDirectory(uint32_t* page_directory)
{
	// free all pages
	for(int i = 0; i < 1024; i++)
	{
		if(page_directory[i] & PAGE_PRESENT) // if there is a page present
			delete (uint32_t*)GET_PAGE(page_directory[i]);
	}
	
	// free Page Directory
	delete page_directory;
}
