/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

#include "../VersionConf.h"
#include "include/beep.h"
#include "include/port.h"
#include "include/pitimer.h"

void Beep::soundOn() 
{
	// set 1st and 2nd bit to turn speaker on
	outb(0x61, inb(0x61) | 0x03);
}

void soundOff(uint32_t time) 
{
	(void)time;
	
	// unset 1st and 2nd bit to turn speaker off
	outb(0x61, inb(0x61) & ~0x03);
}

void Beep::makeBeep(uint32_t freq, uint32_t time)
{
	uint16_t div;
	div = 1193180 / freq;
	outb(PIT_CMDREG, 0xB6);
	outb(PIT_CHANNEL2, div & 0xFF);
	outb(PIT_CHANNEL2, div >> 8);
	soundOn();
    PITimer::getInstance()->register_timeout_handler(time, &soundOff);
}