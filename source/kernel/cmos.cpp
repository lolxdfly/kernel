/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

#include "../VersionConf.h"
#include "include/cmos.h"
#include "include/port.h"

CMOS* CMOS::_instance = nullptr;

uint8_t CMOS::read(uint8_t offset) const
{
	uint8_t tmp = inb(CMOS_PORT_ADDRESS);
	outb(CMOS_PORT_ADDRESS, (tmp & 0x80) | (offset & 0x7F));
	return inb(CMOS_PORT_DATA);
}

void CMOS::write(uint8_t offset, uint8_t val)
{
	uint8_t tmp = inb(CMOS_PORT_ADDRESS);
	outb(CMOS_PORT_ADDRESS, (tmp & 0x80) | (offset & 0x7F));
	outb(CMOS_PORT_DATA, val);
}
 
int CMOS::get_update_in_progress_flag() const
{
    outb(CMOS_PORT_ADDRESS, 0x0A);
    return (inb(CMOS_PORT_DATA) & 0x80);
}
 
timestamp_t CMOS::getTimestamp() const
{
	timestamp_t time;
	int century_register = 0x00; // Set by ACPI table parsing code if possible
    uint8_t century;
    uint8_t last_second;
    uint8_t last_minute;
    uint8_t last_hour;
    uint8_t last_day;
    uint8_t last_month;
    uint8_t last_year;
    uint8_t last_century;
    uint8_t registerB;
	
	// Note: This uses the "read registers until you get the same values twice in a row" technique
    //       to avoid getting dodgy/inconsistent values due to RTC updates
	
	// Make sure an update isn't in progress
    while (get_update_in_progress_flag());
	
    time.second = read(CMOS_OFFSET_SECONDS);
    time.minute = read(CMOS_OFFSET_MINUTE);
    time.hour = read(CMOS_OFFSET_HOUR);
    time.day = read(CMOS_OFFSET_DAYOFMONTH);
    time.month = read(CMOS_OFFSET_MONTH);
    time.year = read(CMOS_OFFSET_YEAR);
	
    if(century_register != 0)
        century = read(century_register);
	
    do 
	{
        last_second = time.second;
        last_minute = time.minute;
        last_hour = time.hour;
        last_day = time.day;
        last_month = time.month;
        last_year = time.year;
        last_century = century;
    
		// Make sure an update isn't in progress
        while (get_update_in_progress_flag());
		
        time.second = read(CMOS_OFFSET_SECONDS);
        time.minute = read(CMOS_OFFSET_MINUTE);
        time.hour = read(CMOS_OFFSET_HOUR);
        time.day = read(CMOS_OFFSET_DAYOFMONTH);
        time.month = read(CMOS_OFFSET_MONTH);
        time.year = read(CMOS_OFFSET_YEAR);
		
        if(century_register != 0)
            century = read(century_register);
    } 
	while( (last_second != time.second) || (last_minute != time.minute) || (last_hour != time.hour) ||
             (last_day != time.day) || (last_month != time.month) || (last_year != time.year) ||
             (last_century != century) );
			 
    registerB = read(CMOS_OFFSET_STATUS_B);
    
    // Convert BCD to binary values if necessary    
    if (!(registerB & 0x04))
	{
        time.second = BCD2BIN(time.second);
        time.minute = BCD2BIN(time.minute);
		// time.hour = ( (time.hour & 0x0F) + (((time.hour & 0x70) / 16) * 10) ) | (time.hour & 0x80); // Todo: check this instead of BCD2BIN
        time.hour = BCD2BIN(time.hour);
        time.day = BCD2BIN(time.day);
        time.month = BCD2BIN(time.month);
        time.year = BCD2BIN(time.year);
        if(century_register != 0)
			century = BCD2BIN(century);
    }
    
    // Convert 12 hour clock to 24 hour clock if necessary
    if (!(registerB & 0x02) && (time.hour & 0x80))
        time.hour = ((time.hour & 0x7F) + 12) % 24;
    
    // Calculate the full (4-digit) year    
    if(century_register != 0)
        time.year += century * 100;
    else
	{
        time.year += (CURRENT_YEAR / 100) * 100;
        if(time.year < CURRENT_YEAR)
			time.year += 100;
    }
	
	return time;
}