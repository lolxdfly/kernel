/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

#include "../VersionConf.h"
#include "include/heap.h"
#include "include/video.h"
#include "include/panic.h"
#include "include/memorymap.h"

KHEAPBM Heap::m_kheap;

void Heap::kinit()
{
	init();
	
	// align memoryEnd
	MemoryMap::memoryEnd &= 0xFFFFF000;
	
	// add 0x1000 for alignment and 0x1000 for space - management memory
	MemoryMap::memoryEnd += 0x2000 - sizeof(KHEAPBLOCKBM);
	
	// allocate 1MiB Ram + management memory
	AddBlock(MemoryMap::memoryEnd, KHEAP_SIZE + sizeof(KHEAPBLOCKBM), 16);
	
	// increase new memoryEnd
	MemoryMap::memoryEnd += KHEAP_SIZE + sizeof(KHEAPBLOCKBM);
}

/*
	Leonard Kevin McGuire Jr (kmcg3413@gmail.com) (www.kmcg3413.net) 
	lolxdfly (lolxdfly@gmail.com)
*/
void Heap::init(KHEAPBM *heap)
{
	heap->fblock = 0;	
}

void* Heap::malloc(uint32_t size, uint8_t bound, KHEAPBM *heap)
{
	// create mask
	uint32_t mask = ~(~(uint32_t)0 << bound);
	
	// iterate blocks
	for(KHEAPBLOCKBM* b = heap->fblock; b; b = b->next)
	{
		// check if block has enough room
		if(b->size - (b->used * b->bsize) >= size)
		{
			uint32_t bcnt = b->size / b->bsize;
			// bneed = (size / b->bsize) * b->bsize < size ? size / b->bsize + 1 : size / b->bsize;
			uint32_t bneed = (size + b->bsize - 1) / b->bsize;
			uint8_t* bm = (uint8_t*)&b[1];
 
			for(uint32_t x = (b->lfb + 1 >= bcnt ? 0 : b->lfb + 1); x != b->lfb; ++x)
			{
				// just wrap around
				if (x >= bcnt)
					x = 0;	
 
				// this is used to allocate on specified boundaries larger than the block size				
				if(((x * b->bsize) + (uintptr_t)&b[1]) & mask)
					continue;
 
				if(bm[x] == 0)
				{	
					// count free blocks
					uint32_t max = bcnt - x;
					uint32_t y = 0;
					for(; bm[x + y] == 0 && y < bneed && y < max; ++y);
 
					// we have enough, now allocate them
					if(y == bneed)
					{
						// find ID that does not match left or right
						uint8_t nid = GetNID(bm[x - 1], bm[x + y]);
 
						// allocate by setting id
						for (uint32_t z = 0; z < y; ++z)
							bm[x + z] = nid;
 
						// optimization
						b->lfb = (x + bneed) - 2;
 
						// count used blocks NOT bytes
						b->used += y;
 
						return (void*)((x * b->bsize) + (uintptr_t)&b[1]);
					}
 
					// x will be incremented by one ONCE more in our FOR loop
					x += (y - 1);
				}
			}
		}
	}
 
	return nullptr;
}
	
void Heap::free(void *ptr, KHEAPBM *heap)
{
	for(KHEAPBLOCKBM* b = heap->fblock; b; b = b->next)
	{
		if((uintptr_t)ptr > (uintptr_t)b && (uintptr_t)ptr < (uintptr_t)b + sizeof(KHEAPBLOCKBM) + b->size)
		{
			// found block
			uintptr_t ptroff = (uintptr_t)ptr - (uintptr_t)&b[1];  // get offset to get block
			// block offset in BM
			uint32_t bi = ptroff / b->bsize;
			// ..
			uint8_t* bm = (uint8_t*)&b[1];
			// clear allocation
			uint8_t id = bm[bi];
			// oddly.. GCC did not optimize this
			uint32_t max = b->size / b->bsize;
			uint32_t x = bi;
			for(; bm[x] == id && x < max; ++x)
				bm[x] = 0;
			
			// update free block count
			b->used -= x - bi;
			return;
		}
	}
 
 	// block not found
	screen << "free error: " << (int)ptr << endl;
}
	
void Heap::AddBlock(uintptr_t addr, uint32_t size, uint32_t bsize, KHEAPBM *heap)
{
	KHEAPBLOCKBM* b = (KHEAPBLOCKBM*)addr;
	b->size = size - sizeof(KHEAPBLOCKBM);
	b->bsize = bsize;
 
	b->next = heap->fblock;
	heap->fblock = b;
 
	uint32_t bcnt = b->size / b->bsize;
	uint8_t* bm = (uint8_t*)&b[1];
 
	// clear bitmap
	for(uint32_t x = 0; x < bcnt; ++x)
		bm[x] = 0;
 
	// reserve room for bitmap
	// bcnt = (bcnt / bsize) * bsize < bcnt ? bcnt / bsize + 1 : bcnt / bsize;
	bcnt = (bcnt + bsize - 1) / bsize;
	for(uint32_t x = 0; x < bcnt; ++x)
		bm[x] = 5;
 
	b->lfb = bcnt - 1;
 
	b->used = bcnt;
}

uint8_t Heap::GetNID(uint8_t a, uint8_t b)
{
	uint8_t c;	
	for(c = a + 1; c == b || c == 0; ++c);
	return c;
}
