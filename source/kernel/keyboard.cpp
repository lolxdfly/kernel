/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

#include "../VersionConf.h"
#include <stdlib.h>
#include "include/keyboard.h"
#include "include/ps2.h"
#include "include/port.h"
#include "include/idt.h"
#include "include/video.h"
#include "include/panic.h"

Keyboard* Keyboard::_instance = nullptr;
bool Keyboard::m_shift = false;
bool Keyboard::m_alt = false;
bool Keyboard::m_ctrl = false;
bool Keyboard::m_caps_lock = false;
bool Keyboard::m_scroll_lock = false;
bool Keyboard::m_num_lock = false;

static const uint8_t sc_to_kc_normal[] = {
    KC_NUL, KC_ESC, KC_1, KC_2, KC_3, KC_4, KC_5, KC_6, KC_7, KC_8,
    KC_9, KC_0, KC_MINUS, KC_EQUAL, KC_BACKSPACE, KC_TAB, KC_Q, KC_W, KC_E, KC_R,
    KC_T, KC_Y, KC_U, KC_I, KC_O, KC_P, KC_SQ_BRACKET_O, KC_SQ_BRACKET_C, KC_ENTER, KC_LEFT_CTRL,
    KC_A, KC_S, KC_D, KC_F, KC_G, KC_H, KC_J, KC_K, KC_L, KC_SEMICOLON,
    KC_SINGLE_QUOTE, KC_BACK_TICK, KC_LEFT_SHIFT, KC_BACK_SLASH, KC_Z, KC_X, KC_C, KC_V, KC_B, KC_N,
    KC_M, KC_COMMA, KC_DOT, KC_SLASH, KC_RIGHT_SHIFT, KC_KP_MARK, KC_LEFT_ALT, KC_SPACE, KC_CAPS_LOCK, KC_F1,
    KC_F2, KC_F3, KC_F4, KC_F5, KC_F6, KC_F7, KC_F8, KC_F9, KC_F10, KC_NUM_LOCK,
    KC_SCROLL_LOCK, KC_KP_7, KC_KP_8, KC_KP_9, KC_KP_MINUS, KC_KP_4, KC_KP_5, KC_KP_6, KC_KP_PLUS, KC_KP_1,
    KC_KP_2, KC_KP_3, KC_KP_0, KC_KP_DOT, KC_NUL, KC_NUL, KC_NUL, KC_F11, KC_F12, KC_NUL,
    KC_NUL, KC_NUL, KC_NUL, KC_NUL, KC_NUL, KC_NUL, KC_NUL, KC_NUL, KC_NUL, KC_NUL,
    KC_NUL, KC_NUL, KC_NUL, KC_NUL, KC_NUL, KC_NUL, KC_NUL, KC_NUL, KC_NUL, KC_NUL,
    KC_NUL, KC_NUL, KC_NUL, KC_NUL, KC_NUL, KC_NUL, KC_NUL, KC_NUL, KC_NUL, KC_NUL,
    KC_NUL, KC_NUL, KC_NUL, KC_NUL, KC_NUL, KC_NUL, KC_NUL, KC_NUL
};

static const uint8_t sc_to_kc_e0[] = {
	KC_NUL, KC_NUL, KC_NUL, KC_NUL, KC_NUL, KC_NUL, KC_NUL, KC_NUL, KC_NUL, KC_NUL,
    KC_NUL, KC_NUL, KC_NUL, KC_NUL, KC_NUL, KC_NUL, KC_NUL, KC_NUL, KC_NUL, KC_NUL,
    KC_NUL, KC_NUL, KC_NUL, KC_NUL, KC_NUL, KC_NUL, KC_NUL, KC_NUL, KC_KP_ENTER, KC_RIGHT_CTRL,
    KC_NUL, KC_NUL, KC_NUL, KC_NUL, KC_NUL, KC_NUL, KC_NUL, KC_NUL, KC_NUL, KC_NUL,
    KC_NUL, KC_NUL, KC_NUL, KC_NUL, KC_NUL, KC_NUL, KC_NUL, KC_NUL, KC_NUL, KC_NUL,
    KC_NUL, KC_NUL, KC_NUL, KC_KP_SLASH, KC_NUL, KC_PRINT, KC_RIGHT_ALT, KC_NUL, KC_NUL, KC_NUL,
    KC_NUL, KC_NUL, KC_NUL, KC_NUL, KC_NUL, KC_NUL, KC_NUL, KC_NUL, KC_NUL, KC_NUL,
    KC_NUL, KC_HOME, KC_CUR_UP, KC_PAGE_UP, KC_NUL, KC_CUR_LEFT, KC_NUL, KC_CUR_RIGHT, KC_NUL, KC_END,
    KC_CUR_DOWN, KC_PAGE_DOWN, KC_INSERT, KC_DELETE, KC_NUL, KC_NUL, KC_NUL, KC_NUL, KC_NUL, KC_NUL,
	KC_NUL, KC_LEFT_GUI, KC_RIGHT_GUI, KC_APPS, KC_NUL, KC_NUL, KC_NUL, KC_NUL, KC_NUL, KC_NUL,
	KC_NUL, KC_NUL, KC_NUL, KC_NUL, KC_NUL, KC_NUL, KC_NUL, KC_NUL, KC_NUL, KC_NUL,
	KC_NUL, KC_NUL, KC_NUL, KC_NUL, KC_NUL, KC_NUL, KC_NUL, KC_NUL, KC_NUL, KC_NUL,
	KC_NUL, KC_NUL, KC_NUL, KC_NUL, KC_NUL, KC_NUL, KC_NUL, KC_NUL
};

static const char kc_to_c[] = {
	'\0', '\0', '1', '2', '3', '4', '5', '6', '7', '8',
	'9', '0', '-', '=', backspace, tab, 'q', 'w', 'e', 'r',
	't', 'y', 'u', 'i', 'o', 'p', '[', ']', endl, '\0',
	'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', ';',
	'\'', '`', '\0', '\\', 'z', 'x', 'c', 'v', 'b', 'n',
	'm', ',', '.', '/', '\0', '*', '\0', ' ', '\0', '\0',
	'\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0',
	'\0', '7', '8', '9', '-', '4', '5', '6', '+', '1',
	'2', '3', '0', '\0', '\0', endl, '\0', '/', '\0', '\0',
	'\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0',
	'\0', '\0', '\0', '\0'
};

static const char kc_to_c_shifted[] = {
	'\0', '\0', '!', '@', '#', '$', '%', '^', '&', '*',
	'(', ')', '_', '+', backspace, tab, 'Q', 'W', 'E', 'R',
	'T', 'Y', 'U', 'I', 'O', 'P', '{', '}', endl, '\0',
	'A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L', ':',
	'"', '~', '\0', '|', 'Z', 'X', 'C', 'V', 'B', 'N',
	'M', '<', '>', '?', '\0', '*', '\0', ' ', '\0', '\0',
	'\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0',
	'\0', '7', '8', '9', '-', '4', '5', '6', '+', '1',
	'2', '3', '0', '\0', '\0', endl, '\0', '/', '\0', '\0',
	'\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0',
	'\0', '\0', '\0', '\0'
};

static void kbc_handler(registers_t &regs)
{
	(void)regs;
	uint8_t scancode = inb(PS2_PORT_IO);	
	uint8_t keycode = KC_NUL;
	bool breakcode = false;
    static bool e0_code = false;
    static uint8_t e1_code = 0;
    static uint16_t e1_prev = 0;

	// handle breakcode
    if((scancode & SC_BREAK) &&
        (e1_code || (scancode != SC_EXT1)) &&
        (e0_code || (scancode != SC_EXT0)))
    {
        breakcode = true;
        scancode &= ~SC_BREAK;
    }

	// handle e0
    if(e0_code)
	{
		// prebyte of print screen key	
		if(scancode == 0x2A)	
		{
			e0_code = false;
			return;
		}
		
		keycode = Keyboard::sctokc(1, scancode);			
		e0_code = false;
    }
	// hanlde e1
	else if(e1_code == 1)
	{
        e1_prev = scancode;
        e1_code++;
    }
	else if(e1_code == 2)
	{
        e1_prev |= ((uint16_t) scancode << 8);
        keycode = Keyboard::sctokc(2, e1_prev);
        e1_code = 0;
    }
	// check e0
	else if(scancode == SC_EXT0) 
	{
        e0_code = true;
    }
	// check e1
	else if(scancode == SC_EXT1)
	{
        e1_code = 1;
    }
	// normal scancode
	else
	{
		if((Keyboard::m_num_lock || Keyboard::m_shift || Keyboard::m_caps_lock) && Keyboard::isNumpad(scancode))
			keycode = Keyboard::sctokc(1, scancode);
		else
			keycode = Keyboard::sctokc(0, scancode);
	}

    if(keycode != KC_NUL)
		Keyboard::getInstance()->handleKeycode(keycode, breakcode);
}

bool Keyboard::isNumpad(uint8_t scancode)
{
	switch(scancode)
	{
		case KC_KP_MARK:
		case KC_KP_7:
		case KC_KP_8:
		case KC_KP_9:
		case KC_KP_MINUS:
		case KC_KP_4:
		case KC_KP_5:
		case KC_KP_6:
		case KC_KP_PLUS:
		case KC_KP_1:
		case KC_KP_2:
		case KC_KP_3:
		case KC_KP_0:
		case KC_KP_DOT:
		case KC_KP_ENTER:
		case KC_KP_SLASH:
			return true;
		default:
			return false;
	}
}

void Keyboard::init()
{
	// do we have ps/2?
	if(PS2::getInstance()->getNumberOfPorts() >= 1)
	{
		// check avaible ports for keyboard
		for(int i = 1; i <= PS2::getInstance()->getNumberOfPorts(); i++)
		{
			switch(PS2::getInstance()->detectDevice(i))
			{
				case ANCIENT_AT_KEYBOARD_TRANS:
				case MF2_KEYBOARD:
				case MF2_KEYBOARD_TRANS1:
				case MF2_KEYBOARD_TRANS2:
				{
					m_ps2_port = i;
					initPS2();
					return;
				}
				default:
					screen << "No PS/2 Keyboard detected" << endl;
					break;
			}
		}
	}
	
	// check USB
	// ...
}

void Keyboard::initPS2()
{
	// Flush Output Buffer
	while (inb(PS2_PORT_STAT_CMD) & PS2_STAT_OUT_FULL)
		inb(PS2_PORT_IO);
	
	// use port
	outb(PS2_PORT_STAT_CMD, m_ps2_port == 1 ? PS2_ENABLE_1ST_PS2 : PS2_ENABLE_2ND_PS2);
		
	// turn all LEDs off
	PS2::getInstance()->writeDev(PS2_SET_INDICATORS);
	if(PS2::getInstance()->readDev(m_ps2_port) != PS2_ACK)
		PANIC("PS/2 Keyboard LED init CMD failed", __FILE__, __FUNCTION__, __LINE__);
	PS2::getInstance()->writeDev(0x00);
	if(PS2::getInstance()->readDev(m_ps2_port) != PS2_ACK)
		PANIC("PS/2 Keyboard LED init Data failed", __FILE__, __FUNCTION__, __LINE__);
	
	// set fastest clock
	PS2::getInstance()->writeDev(PS2_SET_TYPEMATIC);
	if(PS2::getInstance()->readDev(m_ps2_port) != PS2_ACK)
		PANIC("PS/2 Keyboard clock init CMD failed", __FILE__, __FUNCTION__, __LINE__);
	PS2::getInstance()->writeDev(0x00);
	if(PS2::getInstance()->readDev(m_ps2_port) != PS2_ACK)
		PANIC("PS/2 Keyboard clock init Data failed", __FILE__, __FUNCTION__, __LINE__);
	
	// enable Scanning for Input
	PS2::getInstance()->writeDev(PS2_SCAN_EN);
	if(PS2::getInstance()->readDev(m_ps2_port) != PS2_ACK)
		PANIC("PS/2 Keyboard enable CMD failed", __FILE__, __FUNCTION__, __LINE__);
	
	// register irq handler
    IDT::getInstance()->register_interrupt_handler( m_ps2_port == 1 ? IRQ01 : IRQ12, &kbc_handler);	
}

char Keyboard::kctoc(uint8_t kc) const
{
	if(m_shift || m_caps_lock)
		return kc_to_c_shifted[kc];
	
	return kc_to_c[kc];
}

uint8_t Keyboard::sctokc(uint8_t set, uint16_t scancode)
{
    switch (set)
	{
        case 0:	
		return sc_to_kc_normal[scancode];

        // e0
        case 1:	
		return sc_to_kc_e0[scancode];

        // e1
        case 2:
            switch (scancode)
			{
                case 0x451D:
                    return KC_PAUSE;

                default:
                   return KC_NUL;
            };
            break;
    }
	return KC_NUL;
}

void Keyboard::handleKeycode(uint8_t keycode, bool breakcode)
{
	if(!breakcode)
	{
		// set pressedKey
		m_pressedKeycode = keycode;
		
		// handle locks
		if(keycode == KC_CAPS_LOCK)
			m_caps_lock = !m_caps_lock;
		else if(keycode == KC_SCROLL_LOCK)
			m_scroll_lock = !m_scroll_lock;
		else if(keycode == KC_NUM_LOCK)
			m_num_lock = !m_num_lock;
	}
	else
		m_pressedKeycode = KC_NUL;
	
	// set flags
	switch(keycode)
	{
		case KC_RIGHT_SHIFT:
		case KC_LEFT_SHIFT: m_shift = !breakcode; break;
		case KC_RIGHT_CTRL:
		case KC_LEFT_CTRL: m_ctrl = !breakcode; break;
		case KC_RIGHT_ALT:
		case KC_LEFT_ALT: m_alt = !breakcode; break;
	}
	
	// call handlers
	for(unsigned int i = 0; i < m_handlers.size(); i++)
	{
		key_handler_t kh = m_handlers[i];
		kh(keycode, breakcode);
	}
}

string Keyboard::getInput(uint8_t end)
{
	// enable cursor
	screen.enableCursor(14, 15);
	
	char* ret = new char[128];
	uint16_t pos = 0;
	for(uint8_t kc = 0, oldkc = 0; kc != end; kc = m_pressedKeycode)
	{
		if(kc != oldkc)
		{		
			// get input
			char in = kctoc(kc);
			
			if(in != '\0')
			{
				// decrease pos on backspace
				if(in == backspace)
				{
					// dont run out of input
					if(pos != 0)
						screen << in;
					
					pos--;
				}
				else
				{
					// print input
					screen << in;
				
					// append on return string
					ret[pos++] = in;
				}
			}
		}
		
		oldkc = kc;
		
		// wait for next interrupt
		asm volatile("hlt");
	}
	
	// disable cursor
	screen.disableCursor();
	
	ret[pos++] = '\0';
	return string(ret);
}

void Keyboard::register_key_handler(key_handler_t handler)
{
	m_handlers.add(handler);
}

void Keyboard::unregister_key_handler(key_handler_t handler)
{
	m_handlers.remove(handler);
}