/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

#include "../VersionConf.h"
#include <stdlib.h>
#include "include/video.h"
#include "include/beep.h"
#include "include/port.h"
#include "include/memorymap.h"

// global instance
Video screen;

Video::Video()
 : m_videomem((uint16_t*) VGA_BUFF_ADDR), m_gPort(VGA_CUR_PORT_CMD), m_off(0), m_pos(0), m_color(0x0700) // default colors: light grey on black background
{
	clear();
	disableCursor();
	// enableCursor(14, 15);
}

void Video::init()
{
	m_gPort = MemoryMap::getInstance()->m_ptrBDA->GraphicInfo.graphicport;
}

void Video::clear()
{
	// fill screen with whitespaces
	for(unsigned int i = 0; i < (VGA_WIDTH * VGA_HEIGHT); i++)
		m_videomem[i] = (unsigned char)' ' | m_color;

	// reset cursor
	m_pos = 0;
	m_off = 0;							
}

void Video::disableCursor() const
{
	// LOW cursor shape port to vga INDEX register
	outb(m_gPort, VGA_CUR_START);
	
	// bits 6-7 must be 0, if bit 5 set the cursor is disable, bits 0-4 controll the cursor shape.
	outb(VGA_CUR_PORT_DATA, 0x3F);
}

void Video::enableCursor(uint8_t start, uint8_t end) const
{
	// start
    outb(m_gPort, VGA_CUR_START);
    outb(VGA_CUR_PORT_DATA, (inb(VGA_CUR_PORT_DATA) & 0xC0) | start);
	
	// end
    outb(m_gPort, VGA_CUR_END);
    outb(VGA_CUR_PORT_DATA, (inb(0x3E0) & 0xE0) | end);
}
   
void Video::updateCursor() const
{
    uint16_t pos = m_off + m_pos;
	
	// cursor HIGH port to vga INDEX register
    outb(m_gPort, 0x0E);
    outb(VGA_CUR_PORT_DATA, pos >> 8);
	
	// cursor LOW port to vga INDEX register
    outb(m_gPort, 0x0F);
    outb(VGA_CUR_PORT_DATA, pos);
}

void Video::setColor(VGA_Color color)
{		
	m_color = (static_cast<uint16_t>(color) << 8) | (m_color & 0xF000);	
}

void Video::setBackgroundColor(VGA_Color color)
{						
	m_color = (static_cast<uint16_t>(color) << 12) | (m_color & 0x0F00);
}

Video& Video::operator << (const char* str)
{
	// use write method
	write(str);
	return *this;
}

Video& Video::operator << (const char c)
{
	// put c
	put(c);
	return *this;
}

Video& Video::operator << (escapes e)
{
	put(e);
	return *this;
}

Video& Video::operator << (int n)
{
	write(itoa(n));	
	return *this;
}

void Video::write(const char* str)
{
	// put chars of s until null
	while (*str != '\0')
		put(*str++);
}

void Video::put(char c)
{
	// catch escape sequencs
	switch(c)
	{
		case endl:
		{
			// new line
			m_pos = 0;
			m_off += VGA_WIDTH;
			updateCursor();
			return;
			
			break;
		}
		case carriageret:
		{
			// set cursor pos to left
			m_pos = 0;
			updateCursor();
			return;
			
			break;
		}
		case tab:
		{
			// print tab (4x whitespace)
			screen << "    ";
			return;
			
			break;
		}
		case backspace:
		{
			// decrease cursor pos
			m_pos--;
			m_videomem[m_off + m_pos] = (uint16_t)' ' | m_color;
			updateCursor();
			return;
			
			break;
		}
		case alert:
		{
			Beep::makeBeep(300, 1000);
			return;

			break;
		}
	}
	
	// cursor reaches max width
	if(m_pos >= VGA_WIDTH)
	{
		// new line
		m_pos = 0;
		m_off += VGA_WIDTH;
	}
	
	// complete screen is written
	if(m_off >= (VGA_WIDTH * VGA_HEIGHT))
		clear();

	// set char with color and increase cursor
	m_videomem[m_off + m_pos] = (uint16_t)c | m_color;
	m_pos++;
	
	// move cursor
	updateCursor();
}