# This macro creates a stub for an ISR which does NOT pass it's own
# error code (adds a dummy errcode byte).
.macro ISR_NOERRCODE id
  .global isr\id
  isr\id:
    cli                   	# Disable interrupts firstly.
    push $0                 # Push a dummy error code.
    push \id               	# Push the interrupt number.
    jmp isr_common_stub     # Go to our common handler code.
.endm

# This macro creates a stub for an ISR which passes it's own
# error code.
.macro ISR_ERRCODE id
  .global isr\id
  isr\id:
    cli                     # Disable interrupts.
	# cpu itself pushes error code
    push \id               	# Push the interrupt number
    jmp isr_common_stub
.endm
	
# This macro creates a stub for an IRQ - the first parameter is
# the IRQ number, the second is the ISR number it is remapped to.
.macro IRQ num, id
  .global irq\num
  irq\num:
    cli
    push $0
    push \id
    jmp irq_common_stub
.endm

# ISR
ISR_NOERRCODE $0
ISR_NOERRCODE $1
ISR_NOERRCODE $2
ISR_NOERRCODE $3
ISR_NOERRCODE $4
ISR_NOERRCODE $5
ISR_NOERRCODE $6
ISR_NOERRCODE $7
ISR_ERRCODE   $8
ISR_NOERRCODE $9
ISR_ERRCODE   $10
ISR_ERRCODE   $11
ISR_ERRCODE   $12
ISR_ERRCODE   $13
ISR_ERRCODE   $14
ISR_NOERRCODE $15
ISR_NOERRCODE $16
ISR_ERRCODE   $17
ISR_NOERRCODE $18
ISR_NOERRCODE $19
ISR_NOERRCODE $20
ISR_NOERRCODE $21
ISR_NOERRCODE $22
ISR_NOERRCODE $23
ISR_NOERRCODE $24
ISR_NOERRCODE $25
ISR_NOERRCODE $26
ISR_NOERRCODE $27
ISR_NOERRCODE $28
ISR_NOERRCODE $29
ISR_NOERRCODE $30
ISR_ERRCODE   $31

# IRQ
IRQ $0, $32
IRQ $1, $33
IRQ $2, $34
IRQ $3, $35
IRQ $4, $36
IRQ $5, $37
IRQ $6, $38
IRQ $7, $39
IRQ $8, $40
IRQ $9, $41
IRQ $10, $42
IRQ $11, $43
IRQ $12, $44
IRQ $13, $45
IRQ $14, $46
IRQ $15, $47

# Syscall
.global syscall$128
syscall$128:
    cli                   	# Disable interrupts firstly.
    push $0                 # Push a dummy error code.
    push $128              	# Push the interrupt number.
    jmp isr_common_stub     # Go to our common handler code.


# Saves the processor state,
# pass it to the handler and restores it
# may need to add segment registers
.extern isr_handler
isr_common_stub:
	
	# push all registers except esp
    push %ebp
    push %edi
    push %esi
    push %edx
    push %ecx
    push %ebx
    push %eax

	# load our kernel segments
    mov $0x10, %ax
    mov %ax, %ds
    mov %ax, %es
	
    cld                     # Make sure direction flag is forward. We don't know what state it is in when interrupt occurs
    push %esp               # Pass a reference of registers
    call isr_handler		# call c++ handler
	mov %eax, %esp			# change stack to restore regs (may useless since reg is pointer type)
	# add $4, %esp			# clean up reference

	# load our user segments
    mov $0x23, %ax
    mov %ax, %ds
    mov %ax, %es	

	# write all registers back execpt esp
    pop %eax
    pop %ebx
    pop %ecx
    pop %edx
    pop %esi
    pop %edi
    pop %ebp

    add $8, %esp  			# Cleans up the pushed error code and pushed ISR number
    iret           			# returns and enable ints

	
.extern irq_handler
irq_common_stub:

	# push all registers except esp
    push %ebp
    push %edi
    push %esi
    push %edx
    push %ecx
    push %ebx
    push %eax
	
	# load our kernel segments
    mov $0x10, %ax
    mov %ax, %ds
    mov %ax, %es

    cld                     # Make sure direction flag is forward. We don't know what state it is in when interrupt occurs
    push %esp               # Pass a reference of registers
    call irq_handler		# call c++ handler
	mov %eax, %esp			# change stack to restore regs (may useless since reg is pointer type)
	# add $4, %esp			# clean up reference

	# load our user segments
    mov $0x23, %ax
    mov %ax, %ds
    mov %ax, %es	

	# write all registers back execpt esp
    pop %eax
    pop %ebx
    pop %ecx
    pop %edx
    pop %esi
    pop %edi
    pop %ebp

    add $8, %esp  			# Cleans up the pushed error code and pushed IRQ number
    iret           			# returns and enable ints
	