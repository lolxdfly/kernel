/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

#include "../VersionConf.h"
#include <memory.h>
#include "include/memorymap.h"
#include "include/video.h"

MemoryMap* MemoryMap::_instance = nullptr;
extern uint8_t end; // end of kernel code
uintptr_t MemoryMap::memoryEnd = (uintptr_t)&end;

void MemoryMap::init()
{
	// IVT
    memcpy(&m_ivt, IVT_START, (IVT_END - IVT_START) + 1);
	
	// BDA
	m_ptrBDA = new BDA_t();
	memcpy(m_ptrBDA, (uint8_t*)BDA_START, (BDA_END - BDA_START) + 1);

	// EBDA
	// m_ptrEBDA = new EBDA_t();
	// memcpy(ptrEBDA, (uint8_t*)m_ptrBDA->ptrEBDA, 1024);
}
