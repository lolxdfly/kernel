/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

#include "../VersionConf.h"
#include "include/syscall.h"

void syscall_write(const char str)
{
	asm ("int $0x80" : : "a" (SYS_SCREEN_WRITE), "b" (str));
}

void syscall_write(const char* str)
{
	for(int i = 0; str[i] != '\0'; i++)
		syscall_write(str[i]);
}

const char* syscall_read()
{
	const char* ret;
	asm ("int $0x80" : "=a" (ret) : "0" (SYS_SCREEN_READ));
	return ret;
}

uint16_t syscall_createProcess(void* entry)
{
	uint16_t ret;
	asm ("int $0x80" : "=a" (ret) : "0" (SYS_CREATE_PROC), "b" (entry));
	return ret;
}

void syscall_killProcess(uint16_t pid)
{
	asm ("int $0x80" : : "a" (SYS_KILL_PROC), "b" (pid));
}

void* syscall_getTaskInfo()
{
	uint32_t ret;
	asm ("int $0x80" : "=a" (ret) : "0" (SYS_GET_TASK_INFO));
	return (void*)ret;
}