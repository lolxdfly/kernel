/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

#pragma once
#include "../../VersionConf.h"
#include <stdint.h>
#include "idt.h"

#define PIT_CHANNEL0	0x40 // PIT Channel 0's Data Register Port
#define PIT_CHANNEL1	0x41 // PIT Channels 1's Data Register Port, we wont be using this here
#define PIT_CHANNEL2	0x42 // PIT Channels 2's Data Register Port
#define PIT_CMDREG		0x43 // PIT Chip's Command Register Port

#define PIT_TICK_TIME 54.9254 // ms
#define MAX_TIMER_HANDLER	32 // Max timer registered

// callbacks for timing actions
typedef void (*pitcall_t)(uint32_t delta);

/**
 * @brief a structure to hold pit handler info
 * 
 */
typedef struct pithandler_info
{
	bool isCooldown;
	uint32_t time;
	uint32_t nexttick;
	pitcall_t handler;
} pithandler_info_t;

/**
 * @brief a class to manage Timers with the programmable Interval timer
 * 
 */
class PITimer
{
public:
	/**
	 * @brief Get the Instance object
	 * 
	 * @return PITimer* a PITimer instance
	 */
    static PITimer* getInstance()
    {
		if (!_instance)
			_instance = new PITimer();
		return _instance;
    }
	
	/// a global tick variable
	static uint32_t m_tick;
	
	/// Timer handlers
	static pithandler_info_t* m_timer_handlers[MAX_TIMER_HANDLER];
	
	/**
	 * @brief init the Programmable Interval Timer
	 * 
	 */
	void init();
	
	/**
	 * @brief register cooldown handler (time in ms)
	 * 
	 * @param time the time in ms
	 * @param handler the handler
	 * @return int the handlerID
	 */
	int register_timeout_handler(uint32_t time, pitcall_t handler);

	/**
	 * @brief register update handler (dtime in ms)
	 * 
	 * @param dtime the timmer difference
	 * @param handler the handler
	 * @return int the handlerID
	 */
	int register_update_handler(uint32_t dtime, pitcall_t handler);	

	/**
	 * @brief unregister handler
	 * 
	 * @param id the handlerID
	 */
	void unregister_handler(int id);

private:
	/// PITimer instance
	static PITimer* _instance;
	
	/// ctor
	PITimer(){ }
};