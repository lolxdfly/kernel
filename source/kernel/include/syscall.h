/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

#pragma once
#include "../../VersionConf.h"
#include <stdint.h>

/**
 * @brief the syscall numbers
 * 
 */
enum syscallnr : const uint8_t
{
	SYS_SCREEN_WRITE = 0,
	SYS_SCREEN_READ = 1,
	SYS_CREATE_PROC = 2,
	SYS_KILL_PROC = 3,
	SYS_GET_TASK_INFO = 4
};

/****** List of syscalls ******/

/**
 * @brief write to the screen
 * 
 * @param c the character
 */
void syscall_write(const char c);

/**
 * @brief write to the screen
 * 
 * @param str the string
 */
void syscall_write(const char* str);

/**
 * @brief get Keyboard input
 * 
 * @return const char* the input
 */
const char* syscall_read();

/**
 * @brief create a new process
 * 
 * @param entry the entry point of the process
 * @return uint16_t the process id
 */
uint16_t syscall_createProcess(void* entry);

/**
 * @brief kill a process
 * 
 * @param pid the proces id
 */
void syscall_killProcess(uint16_t pid);

/**
 * @brief get info of the current task
 * 
 * @return void* a pointer to the task info
 */
void* syscall_getTaskInfo();
