/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

#pragma once
#include "../../VersionConf.h"
#include <stdint.h>

// This structure contains the value of one GDT entry.
// We use the attribute 'packed' to tell GCC not to change
// any of the alignment in the structure.
struct gdt_entry_struct
{
    uint16_t limit_low;           // The lower 16 bits of the limit.
    uint16_t base_low;            // The lower 16 bits of the base.
    uint8_t  base_middle;         // The next 8 bits of the base.
    uint8_t  access;              // Access flags, determine what ring this segment can be used in.
    uint8_t  granularity;		  // segment granularity and os mode (proteced/long | 16bit/32bit)
    uint8_t  base_high;           // The last 8 bits of the base.
} __attribute__((packed));

typedef struct gdt_entry_struct gdt_entry_t;

// Access-Byte
#define GDT_ACCESS_NOT_PRESENT 		0x00
#define GDT_ACCESS_PRESENT 			0x80

#define GDT_ACCESS_PRIVILEGE_RING0	0x00
#define GDT_ACCESS_PRIVILEGE_RING1	0x40
#define GDT_ACCESS_PRIVILEGE_RING2	0x20
#define GDT_ACCESS_PRIVILEGE_RING3	0x60

#define GDT_ACCESS_GATE_TSS			0x00
#define GDT_ACCESS_CODE_DATA		0x10

#define GDT_ACCESS_DATA 			0x00
#define GDT_ACCESS_CODE 			0x08

#define GDT_ACCESS_DIRECTION_UP		0x00
#define GDT_ACCESS_DIRECTION_DOWN	0x04

#define GDT_ACCESS_NOT_READ_WRITE	0x00
#define GDT_ACCESS_READ_WRITE		0x02

#define GDT_NOT_ACCESSED			0x00
#define GDT_ACCESSED				0x01

// Flags/Granularity
#define GDT_GRAN_BYTE	 			0x00
#define GDT_GRAN_4KIB	 			0x08

#define GDT_GRAN_16BIT_PROTECTED	0x00
#define GDT_GRAN_32BIT_PROTECTED	0x04

#define GDT_GRAN_PROTECTED			0x00
#define GDT_GRAN_LONG				0x02

// 0x1 unused

// This struct describes a GDT pointer. It points to the start of
// our array of GDT entries, and is in the format required by the
// lgdt instruction.
struct gdt_ptr_struct
{
    uint16_t limit;               // The upper 16 bits of all selector limits.
    uint32_t base;                // The address of the first gdt_entry_t struct.
} __attribute__((packed));

typedef struct gdt_ptr_struct gdt_ptr_t;

#define NUM_GDT_ENTRIES 6

/**
 * @brief a class to init the Global Descriptor Table
 * 
 */
class GDT
{
public:
	/**
	 * @brief Get the Instance object
	 * 
	 * @return GDT* a GDT instance
	 */
    static GDT* getInstance()
    {
		if (!_instance)
			_instance = new GDT();
		return _instance;
    }
	
	/**
	 * @brief init the Global Descriptor Table
	 * 
	 */
	void init();

	/// the TSS
	static uint32_t tss[32];
private:
	/// GDT instance
	static GDT* _instance;

	/// gdt entries
	gdt_entry_t m_gdt_entries[NUM_GDT_ENTRIES];

	/// gdt pointer for asm calls
	gdt_ptr_t m_gdt_ptr;
	
	/// ctor
	GDT(){ }

	/**
	 * @brief set an entry of the gdt
	 * 
	 * @param num number of the entry
	 * @param base base address of the entry
	 * @param limit limit address of the entry
	 * @param access access flag of the entry
	 * @param gran granularity flag of the entry
	 */
	void setgate(uint8_t num, uint32_t base, uint32_t limit, uint8_t access, uint8_t gran);
};