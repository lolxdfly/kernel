/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

#pragma once
#include "../../VersionConf.h"
#include <stdint.h>

/**
 * @brief a class to make simple sound output
 * 
 */
class Beep
{
public:

	/**
	 * @brief make a beep with Frequency freq for time time
	 * NOTE: Timer->Init() necessary
	 * 
	 * @param freq the frequency of the beep
	 * @param time the time of the beep
	 */
	static void makeBeep(uint32_t freq, uint32_t time);
private:

	/**
	 * @brief turns the speaker on
	 * 
	 */
	static void soundOn();
};