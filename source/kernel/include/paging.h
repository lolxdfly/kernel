/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

#pragma once
#include "../../VersionConf.h"
#include <stdint.h>

// Page Directory Entry
#define PAGE_CACHING			0x000
#define PAGE_NO_CACHING			0x010
#define PAGE_NOT_ACCESSED		0x000
#define PAGE_ACCESSED			0x020
#define PAGE_SIZE_KiB			0x000
#define PAGE_SIZE_MiB			0x080

// Page Table (ext)
#define PAGE_NOT_DIRTY			0x000
#define PAGE_DIRTY				0x040

// common
#define PAGE_NOT_PRESENT 		0x000
#define PAGE_PRESENT 			0x001
#define PAGE_READ_ONLY			0x000
#define PAGE_READ_WRITE			0x002
#define PAGE_ACCESS_SUPERVISOR	0x000
#define PAGE_ACCESS_ALL			0x004
#define PAGE_WRITE_BACK			0x000
#define PAGE_WRITE_THROUGH		0x008
#define PAGE_NOT_GLOBAL			0x000
#define PAGE_GLOBAL				0x100 // prevents the TLB from updating the address in its cache if CR3 is reset

#define GET_PAGE(x) (x & ~0xFFF)

/**
 * @brief a Paging implementation to manage memory access and mapping between physical and virtual memory
 * 
 */
class Paging
{
public:
	/**
	 * @brief Get the Instance object
	 * 
	 * @return Paging* a Paging instance
	 */
    static Paging* getInstance()
    {
		if (!_instance)
			_instance = new Paging();
		return _instance;
    }
	
	/**
	 * @brief init the Paging
	 * 
	 */
	void init();
	
	/**
	 * @brief Get the Kernel Page Directory object
	 * 
	 * @return uint32_t* the Kernel Page Directory object
	 */
	uint32_t* getKernelPageDirectory() const { return m_kernel_page_directory; }
	
	/**
	 * @brief creates a new Page Directory
	 * 
	 * @param flags the flags of the page directory
	 * @return uint32_t* a pointer to the page directory
	 */
	uint32_t* allocPageDirectory(uint8_t flags);
	
	/**
	 * @brief maps a Page
	 * 
	 * @param page_directory the page directory
	 * @param phyaddr the physical address
	 * @param virtaddr the virtual address
	 * @param flags the flags
	 */
	void mapPage(uint32_t* page_directory, uint32_t phyaddr, uint32_t virtaddr, uint8_t flags);
	
	/**
	 * @brief switches to a certain Page Directory
	 * 
	 * @param page_directory the page directory
	 */
	static void activatePageDirectory(uint32_t* page_directory);
	
	/**
	 * @brief free a Page Directory with all it's pages
	 * 
	 * @param page_directory the page directory
	 */
	void freePageDirectory(uint32_t* page_directory);
	
private:
	/// Paging instance
	static Paging* _instance;
	
	/// the Page Directory of the Kernel
	uint32_t* m_kernel_page_directory;
	
	/// ctor
	Paging(){ }
};
