/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

#pragma once
#include "../../VersionConf.h"
#include <stdint.h>
#include "../../lib/include/arraylist.hpp"
#include "../../lib/include/string.h"

// Scancodes
#define SC_BREAK		0x80
#define SC_EXT0			0xE0
#define SC_EXT1			0xE1

// Keycodes
enum KeyCodes : const uint8_t {
	KC_NUL			= 0x00,
	KC_ESC			= 0x01,
	KC_1			= 0x02,
	KC_2			= 0x03,
	KC_3			= 0x04,
	KC_4			= 0x05,
	KC_5			= 0x06,
	KC_6			= 0x07,
	KC_7			= 0x08,
	KC_8			= 0x09,
	KC_9			= 0x0A,
	KC_0			= 0x0B,
	KC_MINUS		= 0x0C,
	KC_EQUAL		= 0x0D,
	KC_BACKSPACE	= 0x0E,
	KC_TAB			= 0x0F,
	KC_Q			= 0x10,
	KC_W			= 0x11,
	KC_E			= 0x12,
	KC_R			= 0x13,
	KC_T			= 0x14,
	KC_Y			= 0x15,
	KC_U			= 0x16,
	KC_I			= 0x17,
	KC_O			= 0x18,
	KC_P			= 0x19,
	KC_SQ_BRACKET_O	= 0x1A,
	KC_SQ_BRACKET_C	= 0x1B,
	KC_ENTER		= 0x1C,
	KC_LEFT_CTRL	= 0x1D,
	KC_A			= 0x1E,
	KC_S			= 0x1F,
	KC_D			= 0x20,
	KC_F			= 0x21,
	KC_G			= 0x22,
	KC_H			= 0x23,
	KC_J			= 0x24,
	KC_K			= 0x25,
	KC_L			= 0x26,
	KC_SEMICOLON	= 0x27,
	KC_SINGLE_QUOTE	= 0x28,
	KC_BACK_TICK	= 0x29,
	KC_LEFT_SHIFT	= 0x2A,
	KC_BACK_SLASH	= 0x2B,
	KC_Z			= 0x2C,
	KC_X			= 0x2D,
	KC_C			= 0x2E,
	KC_V			= 0x2F,
	KC_B			= 0x30,
	KC_N			= 0x31,
	KC_M			= 0x32,
	KC_COMMA		= 0x33,
	KC_DOT			= 0x34,
	KC_SLASH		= 0x35,
	KC_RIGHT_SHIFT	= 0x36,
	KC_KP_MARK		= 0x37,
	KC_LEFT_ALT		= 0x38,
	KC_SPACE		= 0x39,
	KC_CAPS_LOCK	= 0x3A,
	KC_F1			= 0x3B,
	KC_F2			= 0x3C,
	KC_F3			= 0x3D,
	KC_F4			= 0x3E,
	KC_F5			= 0x3F,
	KC_F6			= 0x40,
	KC_F7			= 0x41,
	KC_F8			= 0x42,
	KC_F9			= 0x43,
	KC_F10			= 0x44,
	KC_NUM_LOCK		= 0x45,
	KC_SCROLL_LOCK	= 0x46,
	KC_KP_7			= 0x47,
	KC_KP_8			= 0x48,
	KC_KP_9			= 0x49,
	KC_KP_MINUS		= 0x4A,
	KC_KP_4			= 0x4B,
	KC_KP_5			= 0x4C,
	KC_KP_6			= 0x4D,
	KC_KP_PLUS		= 0x4E,
	KC_KP_1			= 0x4F,
	KC_KP_2			= 0x50,
	KC_KP_3			= 0x51,
	KC_KP_0			= 0x52,
	KC_KP_DOT		= 0x53,
	KC_F11			= 0x54,
	KC_F12			= 0x55,
	KC_KP_ENTER		= 0x56,
	KC_RIGHT_CTRL	= 0x57,
	KC_KP_SLASH		= 0x58,
	KC_RIGHT_ALT	= 0x59,
	KC_HOME			= 0x5A,
	KC_CUR_UP		= 0x5B,
	KC_PAGE_UP		= 0x5C,
	KC_CUR_LEFT		= 0x5D,
	KC_CUR_RIGHT	= 0x5E,
	KC_END			= 0x5F,
	KC_CUR_DOWN		= 0x60,
	KC_PAGE_DOWN	= 0x61,
	KC_INSERT		= 0x62,
	KC_DELETE		= 0x63,
	KC_LEFT_GUI		= 0x64,
	KC_RIGHT_GUI	= 0x65,
	KC_APPS			= 0x66,
	KC_PRINT		= 0x67,
	KC_PAUSE		= 0x68
};

typedef void (*key_handler_t)(uint8_t, bool);

/**
 * @brief a basic driver for PS/2 Keyboard
 * 
 */
class Keyboard
{
public:
	/**
	 * @brief Get the Instance object
	 * 
	 * @return Keyboard* a Keyboard driver instance
	 */
    static Keyboard* getInstance()
    {
		if (!_instance)
			_instance = new Keyboard();
		return _instance;
    }
	
	/**
	 * @brief init the Keyboard
	 * 
	 */
	void init();
	
	/**
	 * @brief get Input until end occurs (default: Enter) (!this blocks the current thread)
	 * 
	 * @param end the key to end the input
	 * @return string the input
	 */
	string getInput(uint8_t end = KC_ENTER);
	
	/**
	 * @brief Get the current pressed key
	 * 
	 * @return uint8_t the keycode of the pressed key
	 */
	uint8_t getKey() const { return m_pressedKeycode; }
	
	/**
	 * @brief register an input handler
	 * 
	 * @param handler the input handler
	 */
	void register_key_handler(key_handler_t handler);
	
	/**
	 * @brief unregister an input handler
	 * 
	 * @param handler the input handler
	 */
	void unregister_key_handler(key_handler_t handler);
	
	/**
	 * @brief converts keycode to char
	 * 
	 * @param kc the keycode
	 * @return char the converted char or '\0' if no mapping found
	 */
	char kctoc(uint8_t kc) const;
	
	/**
	 * @brief Get the Shift flag
	 * 
	 * @return true if Shift is pressed
	 * @return false if Shift is not pressed
	 */
	bool getShift() const { return m_shift || m_caps_lock; }

	/**
	 * @brief Get the Alt flag
	 * 
	 * @return true if Alt is pressed
	 * @return false if Alt is not pressed
	 */
	bool getAlt() const { return m_alt; }

	/**
	 * @brief Get the Ctrl flag
	 * 
	 * @return true if Ctrl is pressed
	 * @return false if Ctrl is not pressed
	 */
	bool getCtrl() const { return m_ctrl; }

	/**
	 * @brief Get the ScrollLock flag
	 * 
	 * @return true if ScrollLock is pressed
	 * @return false if ScrollLock is not pressed
	 */
	bool getScrollLock() const { return m_scroll_lock; }
	
//private:
	/**
	 * @brief converts scancode into keycode
	 * 
	 * @param set the scancode set
	 * @param scancode the scancode
	 * @return uint8_t the keycode
	 */
	static uint8_t sctokc(uint8_t set, uint16_t scancode);

	/**
	 * @brief test if a key is a key on the numpad
	 * 
	 * @param scancode the scancode
	 * @return true if the key is on the numpad
	 * @return false if the key is not on the numpad
	 */
	static bool isNumpad(uint8_t scancode);
	
	/*
		Issue:
			If shift is pressed and at the same time a Numpad-Key is pressed
			it will unshift the Numpad-Keycode
	*/
	// keyboard flags
	static bool m_shift;
	static bool m_alt;
	static bool m_ctrl;
	static bool m_caps_lock;
	static bool m_scroll_lock;
	static bool m_num_lock;
	
	/**
	 * @brief sends the Keycode to all handlers
	 * 
	 * @param keycode the keycode
	 * @param breakcode the breakcode
	 */
	void handleKeycode(uint8_t keycode, bool breakcode);
	
private:
	/// Keyboard instance
	static Keyboard* _instance;
	
	/// used ps/2 port
	uint8_t m_ps2_port;
	
	/// actual pressed key or 0 if none
	uint8_t m_pressedKeycode;
	
	/// handlers
	ArrayList<key_handler_t> m_handlers;
	
	/// ctor
	Keyboard() : m_ps2_port(0), m_pressedKeycode(0) { }
	
	/**
	 * @brief init PS/2 Keyboard
	 * 
	 */
	void initPS2();
};