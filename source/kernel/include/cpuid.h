/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

#pragma once
#include "../../VersionConf.h"
#include <stdint.h>
#include "../../lib/include/string.h"

/**
 * @brief a CPU info structure
 * 
 */
typedef struct CPU_INFO
{
	string name;
	uint32_t signature;
	int16_t model;
	int16_t	family;
	int16_t type;
	int16_t brand;
	int16_t stepping;
	int16_t reserved;
	int16_t extended_family;
} CPU_INFO_t;

/// cpuid command
#define cpuid(in, a, b, c, d) __asm__("cpuid": "=a" (a), "=b" (b), "=c" (c), "=d" (d) : "a" (in));

// cpuid defines
#define MAGIC_INTEL	0x756E6547
#define MAGIC_AMD	0x68747541

/**
 * @brief a class to get information about the CPU
 * 
 */
class CPUID
{
public:
	/**
	 * @brief Get the Instance object
	 * 
	 * @return CPUID* a CPUID instance
	 */
    static CPUID* getInstance()
    {
		if (!_instance)
			_instance = new CPUID();
		return _instance;
    }
	
	/**
	 * @brief get the CPU info
	 * 
	 * @return CPU_INFO_t a CPU info structure
	 */
	CPU_INFO_t getCPUInfo();
	
	/**
	 * @brief print the CPU info on screen
	 * 
	 */
	void printCPUInfo();
	
private:
	/// store the cpu info
	CPU_INFO_t* m_cpu;
	
	/**
	 * @brief converts the registers into a readable string
	 * 
	 * @param eax the eax register
	 * @param ebx the ebx register
	 * @param ecx the ecx register
	 * @param edx the edx register
	 * @return string the string
	 */
	string getregs(int eax, int ebx, int ecx, int edx) const;
	
	/**
	 * @brief detect the CPU
	 * 
	 */
	void detectCPU();
	
	/**
	 * @brief read the intel CPU info
	 * 
	 */
	void getIntelCPU() const;
	
	/**
	 * @brief read the AMD CPU info
	 * 
	 */
	void getAMDCPU() const;

	/// CPUID instance
	static CPUID* _instance;
	
	/// ctor
	CPUID() : m_cpu(nullptr) { }
};