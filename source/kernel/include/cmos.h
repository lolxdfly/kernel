/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

#pragma once
#include "../../VersionConf.h"
#include <stdint.h>

//Convert BCD to BIN
#define BCD2BIN(bcd) ((((bcd) & 0x0F) + ((bcd) >> 4 ) * 10))

//Ports
#define CMOS_PORT_ADDRESS 0x70
#define CMOS_PORT_DATA    0x71

//registers/offesets for read/write
#define CMOS_OFFSET_SECONDS				0x00		//(BCD)
#define CMOS_OFFSET_ALERTSECOND			0x01		//(BCD)
#define CMOS_OFFSET_MINUTE				0x02		//(BCD)
#define CMOS_OFFSET_ALERTMINUTE			0x03		//(BCD)
#define CMOS_OFFSET_HOUR				0x04		//(BCD)
#define CMOS_OFFSET_ALERTHOUR			0x05		//(BCD)
#define CMOS_OFFSET_DAY					0x06		//(BCD)
#define CMOS_OFFSET_DAYOFMONTH			0x07		//(BCD)
#define CMOS_OFFSET_MONTH				0x08		//(BCD)
#define CMOS_OFFSET_YEAR				0x09		//(last two digits)(BCD)
#define CMOS_OFFSET_STATUS_A			0x0A
#define CMOS_OFFSET_STATUS_B			0x0B
#define CMOS_OFFSET_STATUS_C			0x0C 		//(read-only)
#define CMOS_OFFSET_STATUS_D			0x0D 		//(read-only)
#define CMOS_OFFSET_POST_DIAG			0x0E
#define CMOS_OFFSET_SHUTDOWN			0x0F
#define CMOS_OFFSET_FLOPPY_TYPE			0x10
//#define CMOS_OFFSET_RESERVED			0x11
#define CMOS_OFFSET_HDD					0x12
//#define CMOS_OFFSET_RESERVED			0x13
#define CMOS_OFFSET_DEVICE				0x14
#define CMOS_OFFSET_BASE_MEM_LOW		0x15 		//(kB)
#define CMOS_OFFSET_BASE_MEM_HIGH		0x16 		//(kB)
#define CMOS_OFFSET_EXT_MEM_LOW			0x17 		//(kB)
#define CMOS_OFFSET_EXT_MEM_HIGH		0x18 		//(kB)
#define CMOS_OFFSET_EXT_HDD_1			0x19
#define CMOS_OFFSET_EXT_HDD_2			0x1A
//#define CMOS_OFFSET_RESERVED			0x1B - 0x2D //BIOS dependent
#define CMOS_OFFSET_CHECKSUM_HIGH		0x2E
#define CMOS_OFFSET_CHECKSUM_LOW		0x2F
#define CMOS_OFFSET_EXT_STORAGE_LOW		0x30
#define CMOS_OFFSET_EXT_STORAGE_HIGH	0x31
#define CMOS_OFFSET_CENTURY				0x32 		//(BCD)
//#define CMOS_OFFSET_RESERVED			0x33 - 0x3F	//BIOS dependent

#define CURRENT_YEAR        2017		// Change this each year! (just for year guess if not supported)

/**
 * @brief a timestamp structure
 * 
 */
typedef struct timestamp
{
	uint8_t second;
	uint8_t minute;
	uint8_t hour;
	uint8_t day;
	uint8_t month;
	uint16_t year;
}timestamp_t;

/**
 * @brief a class to communicate with the CMOS
 * 
 */
class CMOS
{
public:
	/**
	 * @brief Get the Instance object
	 * 
	 * @return CMOS* a CMOS instance
	 */
    static CMOS* getInstance()
    {
		if (!_instance)
			_instance = new CMOS();
		return _instance;
    }
	
	/**
	 * @brief read from the CMOS
	 * 
	 * @param offset the offset to read from
	 * @return uint8_t the answer
	 */
	uint8_t read(uint8_t offset) const;
	
	/**
	 * @brief write to the CMOS
	 * 
	 * @param offset the offset to write to
	 * @param val the value to write
	 */
	void write(uint8_t offset, uint8_t val);
	
	/**
	 * @brief Get the Timestamp object
	 * 
	 * @return timestamp_t 
	 */
	timestamp_t getTimestamp() const;

private:
	/// CMOS instance
	static CMOS* _instance;
	
	/// ctor
	CMOS(){ }
	
	/**
	 * @brief Get the update in progress flag
	 * 
	 * @return int the update in progress flag
	 */
	int get_update_in_progress_flag() const;
};