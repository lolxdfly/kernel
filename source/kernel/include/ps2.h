/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

#pragma once
#include "../../VersionConf.h"
#include <stdint.h>
#include "video.h"

// Ports
#define PS2_PORT_IO			0x60 // read or write data
#define PS2_PORT_STAT_CMD	0x64 // get status or send commands

// Status Register
#define PS2_STAT_OUT_FULL	0x01
#define PS2_STAT_IN_FULL	0x02
#define PS2_STAT_SYS_FLAG	0x04
#define PS2_STAT_CONTROL	0x08
#define PS2_STAT_UNKNOWN1	0x10 // may be keyboard lock
#define PS2_STAT_UNKNOWN2	0x20 // may be receive time-out or output buffer full
#define PS2_STAT_TIMEOUT	0x40
#define PS2_STAT_PARITY		0x60

// Controller Command Bytes
#define PS2_READ_CONF					0x20
#define PS2_READ_RAM(x)					(PS2_READ_CONF | (x & 0x1F))
#define PS2_WRITE_CONF					0x60
#define PS2_WRITE_RAM(x)				(PS2_WRITE_CONF | (x & 0x1F))
#define PS2_DISABLE_2ND_PS2				0xA7
#define PS2_ENABLE_2ND_PS2				0xA8
#define PS2_TEST_2ND_PS2				0xA9
#define PS2_TEST						0xAA
#define PS2_TEST_1ST_PS2				0xAB
#define PS2_DIAG_DUMP					0xAC
#define PS2_DISABLE_1ST_PS2				0xAD
#define PS2_ENABLE_1ST_PS2				0xAE
#define PS2_READ_CONT_IN_PORT			0xC0
#define PS2_CPY_BIT_IN_PORT_TO_STAT_0_3	0xC1
#define PS2_CPY_BIT_IN_PORT_TO_STAT_4_7	0xC2
#define PS2_READ_CONT_OUT_PORT			0xD0
#define PS2_WRITE_CONT_OUT_PORT			0xD1
#define PS2_WRITE_1ST_OUT_BUFF			0xD2
#define PS2_WRITE_2ND_OUT_BUFF			0xD3
#define PS2_WRITE_2ND_IN_BUFF			0xD4

// Responses (Controller)
#define PS2_TEST_SUCESS		0x55
#define PS2_TEST_FAIL		0xFC

// Device Command Bytes
#define PS2_SET_INDICATORS  0xED
#define PS2_ECHO         	0xEE
#define PS2_IDENTIFY        0xF2
#define PS2_SET_TYPEMATIC   0xF3
#define PS2_SCAN_EN         0xF4
#define PS2_SCAN_DS         0xF5   // Also RESET_PARAMS
#define PS2_RESET_PARAMS    0xF6   // Doesn't disable scanning
#define PS2_RESET_TEST      0xFF

// Responses (Device)
#define PS2_TEST_SUCCESS  	0xAA
#define PS2_ECHO_RES      	0xEE
#define PS2_ACK         	0xFA
#define PS2_TEST_FAIL1   	0xFC
#define PS2_TEST_FAIL2   	0xFD
#define PS2_RESEND      	0xFE

// Controller Configuration Byte
#define PS2_CONF_1ST_INT			0x01
#define PS2_CONF_2ND_INT			0x02
#define PS2_CONF_SYS_PASSED_POST	0x04
#define PS2_CONF_1ST_CLOCK			0x10
#define PS2_CONF_2ND_CLOCK			0x20 
#define PS2_CONF_1ST_PORT_TRANS		0x40

// Controller Output Port
#define PS2_OUT_SYS_RESET	0x01
#define PS2_OUT_A20			0x02
#define PS2_OUT_2ND_CLOCK	0x04
#define PS2_OUT_2ND_DATA	0x08
#define PS2_OUT_FULL_1ST	0x10
#define PS2_OUT_FULL_2ND	0x20
#define PS2_OUT_1ST_CLOCK	0x40
#define PS2_OUT_1ST_DATA	0x60

// PS2_1ST <=> IRQ1
// PS2_2ND <=> IRQ12

/**
 * @brief a basic PS/2 driver
 * 
 */
class PS2
{
public:
	/**
	 * @brief Get the Instance object
	 * 
	 * @return PS2* a PS/2 driver instance
	 */
    static PS2* getInstance()
    {
		if (!_instance)
			_instance = new PS2();
		return _instance;
    }
	
	/**
	 * @brief init the PS/2
	 * 
	 */
	void init();
	
	// Search for devices
	#define ANCIENT_AT_KEYBOARD_TRANS	0XFF
	#define STANDARD_PS2_MOUSE 			0X00
	#define MOUSE_SCROLL				0X03 
	#define _5_BUTTON_MOUSE				0X04 
	#define MF2_KEYBOARD_TRANS1			0X01
	#define MF2_KEYBOARD_TRANS2			0X81
	#define MF2_KEYBOARD				0X83 
		
	#define PORT_NOT_SUPPORTED			0XFC
	#define IDENTIFY_FAILED				0XFD
	#define SCAN_DISABLE_FAILED			0XFE

	/**
	 * @brief detect a PS/2 device
	 * 
	 * @param port the port of the device
	 * @return uint8_t the type of device
	 */
	uint8_t detectDevice(uint8_t port);
	
	/**
	 * @brief Get the Number Of Ports
	 * 
	 * @return uint8_t the Number of PS/2 Ports
	 */
	uint8_t getNumberOfPorts() const { return m_channels; }
	
	/// internal input-status of ports
	static uint8_t m_in_port1;
	static uint8_t m_in_port2;

	/**
	 * @brief read from Device
	 * 
	 * @param port port of the device
	 * @return int16_t data read from the device
	 */
	int16_t readDev(uint8_t port);
	
	/**
	 * @brief write to PS/2 Device
	 * 
	 * @param data the data to write
	 */
	void writeDev(uint8_t data);
	
private:
	/// PS/2 instance
	static PS2* _instance;
	
	// channels:
	// 	0 - uninitialized
	// 	1 - 1 port
	// 	2 - 2 ports
	uint8_t m_channels;
		
	/// ctor
	PS2() : m_channels(0) { }
	
	/**
	 * @brief read from Controller
	 * 
	 * @return uint8_t the data read
	 */
	uint8_t read() const;
	
	/**
	 * @brief write to PS/2 Controller
	 * 
	 * @param port the port to write to
	 * @param data the data to write
	 */
	void write(uint8_t port, uint8_t data);
};