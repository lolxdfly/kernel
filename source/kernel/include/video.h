/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

#pragma once
#include "../../VersionConf.h"
#include <stddef.h>
#include <stdint.h>
#include "../../lib/include/string.h"

// Text mode memory address
#define VGA_BUFF_ADDR	0xB8000

// Text mode screen size
#define VGA_WIDTH 	80
#define VGA_HEIGHT 	25

// Cursor defines
#define VGA_CUR_PORT_CMD	0x3D4 // assumed
#define VGA_CUR_PORT_DATA	0x3D5

#define VGA_CUR_START	0x0A
#define VGA_CUR_END		0x0B

/**
 * @brief Hardware text mode color constants
 * 
 */
enum class VGA_Color : const uint8_t {
	VGA_COLOR_BLACK			= 0x0,
	VGA_COLOR_BLUE 			= 0x1,
	VGA_COLOR_GREEN 		= 0x2,
	VGA_COLOR_CYAN 			= 0x3,
	VGA_COLOR_RED 			= 0x4,
	VGA_COLOR_MAGENTA 		= 0x5,
	VGA_COLOR_BROWN 		= 0x6,
	VGA_COLOR_LIGHT_GREY 	= 0x7,
	VGA_COLOR_DARK_GREY 	= 0x8,
	VGA_COLOR_LIGHT_BLUE 	= 0x9,
	VGA_COLOR_LIGHT_GREEN 	= 0xA,
	VGA_COLOR_LIGHT_CYAN	= 0xB,
	VGA_COLOR_LIGHT_RED 	= 0xC,
	VGA_COLOR_LIGHT_MAGENTA = 0xD,
	VGA_COLOR_LIGHT_BROWN 	= 0xE,
	VGA_COLOR_WHITE 		= 0xF,
};

/**
 * @brief escape sequences
 * 
 */
enum escapes : const char {
	endl		= '\n',
	alert		= '\a',
	tab			= '\t',
	backspace 	= '\b',
	carriageret	= '\r',
};

/**
 * @brief a basic text mode based video driver
 * 
 */
class Video
{
public:
    ///ctor
    Video();
   
    /**
     * @brief Init Text Mode driver
     * 
     */
    void init();

    /**
     * @brief clear the screen
     * 
     */
    void clear();
    
    /**
     * @brief set textcolor
     * 
     * @param color the text color
     */
    void setColor(VGA_Color color);
    
    /**
     * @brief set backgroundcolor
     * 
     * @param color the background color
     */
    void setBackgroundColor(VGA_Color color);
    
    /**
     * @brief togles the text mode cursor
     * 
     */
    void disableCursor() const;
    
    /**
     * @brief enables cursor with size (max: enableCursor(0, 15))
     * 
     * @param start the start position of the cursor
     * @param end the end position of the cursor
     */
    void enableCursor(uint8_t start, uint8_t end) const;

    /// write string
    Video& operator << (const char* str);
    
    /// write char
    Video& operator << (const char c);
    
    /// write escape
    Video& operator << (escapes e);

    /// write int
    Video& operator << (int n);
    
    /**
     * @brief write a string on the screen (with escape sequences)
     * 
     * @param str the string
     */
    void write(const char* str);
    
    /**
     * @brief write a char on the screen (with escape sequences)
     * 
     * @param c the char
     */
    void put(char c);

private:
  
    /**
     * @brief updates cursor
     * 
     */
    void updateCursor() const;
    
    /// videomemory-pointer
    uint16_t* m_videomem;
    
    /// Graphics IO Port
    uint16_t m_gPort;

    // y-pos
    unsigned int m_off;

    // x-pos
    unsigned int m_pos;

    // text and background color
    uint16_t m_color;
};

// global instance
extern Video screen;