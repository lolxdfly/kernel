#pragma once
#include "../../VersionConf.h"
#include <stdint.h>

///////////////////////////////////////////Header///////////////////////////////////////////
#define ELF_MAGIC 0x464C457F //magic backwards!?

#define ELF_BITCNT_32BIT	1
#define ELF_BITCNT_64BIT	2

#define ELF_LITTLE_ENDIAN	1
#define ELF_BIG_ENDIAN		2

#define ELF_TYPE_RELOCATABLE	1
#define ELF_TYPE_EXECUTABLE		2
#define ELF_TYPE_SHARED			3
#define ELF_TYPE_CORE			4

#define ELF_MACHINE_NO_SPECIFIC	0
#define ELF_MACHINE_SPARC		2
#define ELF_MACHINE_X86			3
#define ELF_MACHINE_MIPS		8
#define ELF_MACHINE_POWERPC		0x14
#define ELF_MACHINE_ARM			0x28
#define ELF_MACHINE_SUPERH		0x2A
#define ELF_MACHINE_IA64		0x32
#define ELF_MACHINE_X86_64		0x3E
#define ELF_MACHINE_AARCH64		0xB7

typedef struct __attribute__ ((__packed__)) elf_header_32bit
{
	// e_ident:
	uint32_t	magic; 		// 0x7'E''L''F'
	uint8_t		bitcnt;		// 1 = 32bit; 2 = 64bit
	uint8_t		endian;		// 1 = little endian; 2 = big endian
	uint8_t		version1;
	uint8_t		OSABI;		// 0 = System V
	uint64_t	reserved;
	
	uint16_t	e_type;		// 1 = relocatable; 2 = executable; 3 = shared; 4 = core 
	uint16_t	e_machine;	// 0 = No Specific; 2 = Sparc; 3 = x86; 8 = MIPS; 0x14 = PowerPC; 0x28 = ARM; 0x2A = SuperH; 0x32 = IA-64; 0x3E = x86-64; 0xB7 = AArch64
	uint32_t	e_version;
	uint32_t	e_entry;
	uint32_t	e_phoff;
	uint32_t	e_shoff;
	uint32_t	e_flags;
	uint16_t	e_ehsize;
	uint16_t	e_phentsize;
	uint16_t	e_phnum;
	uint16_t	e_shentsize;
	uint16_t	e_shnum;
	uint16_t	e_shstrndex;
} elf_header_32bit_t;
//////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////Programm///////////////////////////////////////////
#define ELF_PH_SEGTYPE_NULL			0
#define ELF_PH_SEGTYPE_LOAD			1
#define ELF_PH_SEGTYPE_DYNAMIC		2
#define ELF_PH_SEGTYPE_INTERP		3
#define ELF_PH_SEGTYPE_NOTE_SECTION	4
#define ELF_PH_SEGTYPE_SHLIB		5
#define ELF_PH_SEGTYPE_PHDIR		6
#define ELF_PH_SEGTYPE_TLS			7

#define ELF_PH_FLAG_EXECUTABLE	1
#define ELF_PH_FLAG_WRITABLE	2
#define ELF_PH_FLAG_READABLE	4

typedef struct __attribute__ ((__packed__)) elf_program_header_32bit
{
	uint32_t    p_type;				// 0 = null; 1 = load; 2 = dynamic; 3 = interp; 4 = note section
	uint32_t    p_offset;
	uint32_t    p_vaddr;
	uint32_t    p_paddr;
	uint32_t    p_filesz;
	uint32_t    p_memsz;
	uint32_t    p_flags;			// 1 = executable; 2 = writable; 4 = readable
	uint32_t    p_align;
} elf_program_header_32bit_t;
//////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////Section///////////////////////////////////////////
#define ELF_SH_TYPE_NULL			0x00
#define ELF_SH_TYPE_PROGBITS		0x01
#define ELF_SH_TYPE_SYMTAB			0x02
#define ELF_SH_TYPE_STRTAB			0x03
#define ELF_SH_TYPE_RELA			0x04
#define ELF_SH_TYPE_HASH			0x05
#define ELF_SH_TYPE_DYNAMIC			0x06
#define ELF_SH_TYPE_NOTE			0x07
#define ELF_SH_TYPE_NOBITS			0x08
#define ELF_SH_TYPE_REL				0x09
#define ELF_SH_TYPE_SHLIB			0x0A
#define ELF_SH_TYPE_DYNSYM			0x0B
#define ELF_SH_TYPE_INIT_ARRAY		0x0E
#define ELF_SH_TYPE_FINI_ARRAY		0x0F
#define ELF_SH_TYPE_PREINIT_ARRAY	0x10
#define ELF_SH_TYPE_GROUP			0x11
#define ELF_SH_TYPE_SYMTAB_SHNDX	0x12
#define ELF_SH_TYPE_NUM				0x13
#define ELF_SH_TYPE_LOOS			0x60000000

#define ELF_SH_FLAGS_WRITE				0x001
#define ELF_SH_FLAGS_ALLOC				0x002
#define ELF_SH_FLAGS_EXECINSTR			AB0x004
#define ELF_SH_FLAGS_MERGE				0x010
#define ELF_SH_FLAGS_STRINGS			0x020
#define ELF_SH_FLAGS_INFO_LINK			0x040
#define ELF_SH_FLAGS_LINK_ORDER			0x080
#define ELF_SH_FLAGS_OS_NONCONFORMING	0x100
#define ELF_SH_FLAGS_GROUP				0x200
#define ELF_SH_FLAGS_TLS				0x400
#define ELF_SH_FLAGS_MASKOS				0x0ff00000
#define ELF_SH_FLAGS_MASKPROC			0xf0000000
#define ELF_SH_FLAGS_ORDERED			0x4000000
#define ELF_SH_FLAGS_EXCLUDE			0x8000000

typedef struct __attribute__ ((__packed__)) elf_section_header_32bit
{
	uint32_t	sh_name;
	uint32_t	sh_type;
	uint32_t	sh_flags;
	uint32_t	sh_addr;
	uint32_t	sh_offset;
	uint32_t	sh_size;
	uint32_t	sh_link;
	uint32_t	sh_info;
	uint32_t	sh_addralign;
	uint32_t	sh_entsize;
} elf_section_header_32bit_t;
//////////////////////////////////////////////////////////////////////////////////////
