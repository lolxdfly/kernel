/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

#pragma once
#include "../../VersionConf.h"
#include <stdint.h>
#include "idt.h"
#include "elf.h"
#include "../../lib/include/unique.hpp"

#define TASK_STACK_SIZE 4096

typedef void (*task_func_t)();

/**
 * @brief a tasktype structure
 * 
 */
enum class tasktype_t : const uint8_t
{
	kernel,
	user,
	//vm86
};

/**
 * @brief a task info structure
 * 
 */
typedef struct task_start_info
{
public:
	/// ctors of task
	elf_section_header_32bit_t* ctors;

	/// dtors of task
	elf_section_header_32bit_t* dtors;
	
	// entry point
	task_func_t		func;
	
	// identifier
	uint16_t		pid;
	
} task_start_info_t;

/**
 * @brief a task structure
 * 
 */
typedef struct task
{
public:
	/// stack of task
	uint8_t* 		stack;

	/// user stack of task
	uint8_t*		user_stack;

	/// cpu registers
	registers_t* 	state;
	
	// Page Directory
	uint32_t*		page_directory;
	
	// task start information
	task_start_info_t* info;
	
	/// the next task
	task*			next;

	/// type of task
	tasktype_t		type;

	/// blocked flag
	bool			blocked;
} task_t;

/**
 * @brief a basic multitasking Manager
 * 
 */
class TaskManager
{
public:
	/**
	 * @brief Get the Instance object
	 * 
	 * @return TaskManager* a TaskManager Instance
	 */
    static TaskManager* getInstance()
    {
		if (!_instance)
			_instance = new TaskManager();
		return _instance;
    }
	
	/// PID generator
	unique<uint16_t> m_PIDGen;
	
	/// current task
	static task_t* m_current_task;
	
	/// first task
	static task_t* m_first_task;
		
	/**
	 * @brief init TaskManager
	 * 
	 */
	void init();
	
	/**
	 * @brief 
	 * 
	 * @param regs 
	 * @return registers_t& 
	 */
	static registers_t& schedule_task(registers_t &regs);
	
	/**
	 * @brief register and start task (kernel mode)
	 * 
	 * @param task the entry point for the task
	 * @param blocked the blocked flag
	 * @return int the process id
	 */
	int register_kernel_task(task_func_t task, bool blocked = false);
	
	/**
	 * @brief register and start task (user mode)
	 * 
	 * @param task the entry point for the task
	 * @param blocked the blocked flag
	 * @return int the process id
	 */
	int register_user_task(task_func_t task, bool blocked = false);
	
	/**
	 * @brief register task by ELF image with kernel or user access
	 * 
	 * @param image the elf image
	 * @param kernel if kernel mode or user mode
	 * @return int the process id
	 */
	int register_elf_task(void* image, bool kernel);
	
	/**
	 * @brief remove and stop task
	 * 
	 * @param pid the process id
	 */
	void unregister_task(uint16_t pid);
		
	/**
	 * @brief blocks a task
	 * 
	 * @param pid the process id
	 */
	void block_task(uint16_t pid) const;
	
	/**
	 * @brief unblocks a task
	 * 
	 * @param pid the process id
	 */
	void unblock_task(uint16_t pid) const;
	
private:
	/// TaskManager instance
	static TaskManager* _instance;
	
	/// ctor
	TaskManager(){ }
	
	/**
	 * @brief register and start task (kernel mode)
	 * 
	 * @param task the entry point for the task
	 * @param blocked the blocked flag
	 * @return task_t* the task structure
	 */
	task_t* create_kernel_task(task_func_t task, bool blocked = false);
	
	/**
	 * @brief register and start task (user mode)
	 * 
	 * @param task the entry point for the task
	 * @param blocked the blocked flag
	 * @return task_t* the task structure
	 */
	task_t* create_user_task(task_func_t task, bool blocked = false);
	
	/**
	 * @brief delete a task: free it's pid/memory
	 * 
	 * @param tbefore the task before (in the linked queue) of the task to be removed
	 */
	void delete_task(task_t* tbefore);
};
