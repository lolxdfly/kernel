/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */
#pragma once
#include "../../VersionConf.h"

// this should be called if kernel gets into crucial state and cannot move on
// dont use string here cause we are not sure if malloc still works
void PANIC(const char* message, const char* file, const char* function, int line);
