/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

#pragma once
#include "../../VersionConf.h"
#include <stdint.h>
#include "syscall.h"

// Syscall Interrupt Port
#define SYSCALL_INT 0x80

/**
 * @brief a class for managing the syscalls
 * 
 */
class SyscallMng
{
public:
	/**
	 * @brief Get the Instance object
	 * 
	 * @return SyscallMng* a SyscallMng instance
	 */
    static SyscallMng* getInstance()
    {
		if (!_instance)
			_instance = new SyscallMng();
		return _instance;
    }
	
	/**
	 * @brief init the Syscall Manager
	 * 
	 */
	void init();
	
private:
	///SyscallMng instance
	static SyscallMng* _instance;
	
	/// ctor
	SyscallMng() { }
};
