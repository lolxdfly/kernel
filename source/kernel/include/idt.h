/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

#pragma once
#include "../../VersionConf.h"
#include <stdint.h>

/**
 * @brief CPU register structure
 * 
 */
typedef struct registers
{
	// idtentries.s   
	uint32_t   eax;
	uint32_t   ebx;
	uint32_t   ecx;
	uint32_t   edx;
	uint32_t   esi;
	uint32_t   edi;
	uint32_t   ebp;

	uint32_t   intr;
	uint32_t   error;

	// Cpu saved
	uint32_t   eip;
	uint32_t   cs;
	uint32_t   eflags;
	uint32_t   esp;
	uint32_t   ss;

} registers_t;

// Enables registration of callbacks for interrupts or IRQs.
typedef void (*int_handler_t)(registers_t &);

/**
 * @brief interrupt structure
 * 
 */
typedef struct int_
{
	int_handler_t handler;
	int_* next;
	
} int_t;

// A struct describing an interrupt gate.
struct idt_entry_struct
{
	uint16_t offset_1; //  offset bits 0..15
	uint16_t selector; //  a code segment selector in GDT or LDT
	uint8_t zero;      //  unused, set to 0
	uint8_t type_attr; //  type and attributes
	uint16_t offset_2; //  offset bits 16..31
} __attribute__((packed));

// type
#define IDT_NOT_PRESENT 		0x00
#define IDT_PRESENT 			0x80

#define IDT_PRIVILEGE_RING0		0x00
#define IDT_PRIVILEGE_RING1		0x40
#define IDT_PRIVILEGE_RING2		0x20
#define IDT_PRIVILEGE_RING3		0x60

#define IDT_INT_TRAP	 		0x00
#define IDT_TASK	 			0x10

#define IDT_16BIT_TRAP			0x07
#define IDT_16BIT_INT			0x06
#define IDT_32BIT_TRAP			0x0F
#define IDT_32BIT_INT			0x0E
#define IDT_32BIT_TASK			0x05

// selector
#define IDT_SEL_PRIVILEGE_RING0	0x00
#define IDT_SEL_PRIVILEGE_RING1	0x01
#define IDT_SEL_PRIVILEGE_RING2	0x02
#define IDT_SEL_PRIVILEGE_RING3	0x03

#define IDT_GDT					0x00
#define IDT_LDT					0x04

#define GDT_ENTRY_0				(0x00 << 3)
#define GDT_ENTRY_1				(0x01 << 3)
#define GDT_ENTRY_2				(0x02 << 3)
#define GDT_ENTRY_3				(0x03 << 3)
#define GDT_ENTRY_4				(0x04 << 3)

typedef struct idt_entry_struct idt_entry_t;

// loads the idt
static inline void lidt(void* base, uint16_t size)
{   //  This function works in 32 and 64bit mode
    struct {
        uint16_t length;
        void*    base;
    } __attribute__((packed)) IDTR = { size, base };
 
    asm ( "lidt %0" : : "m"(IDTR) );  //  let the compiler choose an addressing mode
}

// some defines
#define PIC_EOI			0x20		// End-of-interrupt command code

#define PIC1			0x20		// IO base address for master PIC
#define PIC2			0xA0		// IO base address for slave PIC
#define PIC1_COMMAND	PIC1
#define PIC1_DATA		(PIC1 + 1)
#define PIC2_COMMAND	PIC2
#define PIC2_DATA		(PIC2 + 1)

#define ICW1_ICW4		0x01		// ICW4 (not) needed
#define ICW1_SINGLE		0x02		// Single (cascade) mode
#define ICW1_INTERVAL4	0x04		// Call address interval 4 (8)
#define ICW1_LEVEL		0x08		// Level triggered (edge) mode
#define ICW1_INIT		0x10		// Initialization - required!

#define ICW4_8086		0x01		// 8086/88 (MCS-80/85) mode
#define ICW4_AUTO		0x02		// Auto (normal) EOI
#define ICW4_BUF_SLAVE	0x08		// Buffered mode/slave
#define ICW4_BUF_MASTER	0x0C		// Buffered mode/master
#define ICW4_SFNM		0x10		// Special fully nested (not)

#define NUM_IDT_ENTRIES 256

// ISR IDs
#define ISR00	 0
#define ISR01	 1
#define ISR02	 2
#define ISR03	 3
#define ISR04	 4
#define ISR05	 5
#define ISR06	 6
#define ISR07	 7
#define ISR08	 8
#define ISR09	 9
#define ISR10	10
#define ISR11	11
#define ISR12	12
#define ISR13	13
#define ISR14	14
#define ISR15	15
#define ISR16	16
#define ISR17	17
#define ISR18	18
#define ISR19	19
#define ISR20	20
#define ISR21	21
#define ISR22	22
#define ISR23	23
#define ISR24	24
#define ISR25	25
#define ISR26	26
#define ISR27	27
#define ISR28	28
#define ISR29	29
#define ISR30	30
#define ISR31	31

// IRQ IDs
#define IRQ00	32
#define IRQ01	33
#define IRQ02	34
#define IRQ03	35
#define IRQ04	36
#define IRQ05	37
#define IRQ06	38
#define IRQ07	39
#define IRQ08	40
#define IRQ09	41
#define IRQ10	42
#define IRQ11	43
#define IRQ12	44
#define IRQ13	45
#define IRQ14	46
#define IRQ15	47

// ISR handlers
extern "C" void isr$0();
extern "C" void isr$1();
extern "C" void isr$2();
extern "C" void isr$3();
extern "C" void isr$4();
extern "C" void isr$5();
extern "C" void isr$6();
extern "C" void isr$7();
extern "C" void isr$8();
extern "C" void isr$9();
extern "C" void isr$10();
extern "C" void isr$11();
extern "C" void isr$12();
extern "C" void isr$13();
extern "C" void isr$14();
extern "C" void isr$15();
extern "C" void isr$16();
extern "C" void isr$17();
extern "C" void isr$18();
extern "C" void isr$19();
extern "C" void isr$20();
extern "C" void isr$21();
extern "C" void isr$22();
extern "C" void isr$23();
extern "C" void isr$24();
extern "C" void isr$25();
extern "C" void isr$26();
extern "C" void isr$27();
extern "C" void isr$28();
extern "C" void isr$29();
extern "C" void isr$30();
extern "C" void isr$31();
// IRQ handlers
extern "C" void irq$0();
extern "C" void irq$1();
extern "C" void irq$2();
extern "C" void irq$3();
extern "C" void irq$4();
extern "C" void irq$5();
extern "C" void irq$6();
extern "C" void irq$7();
extern "C" void irq$8();
extern "C" void irq$9();
extern "C" void irq$10();
extern "C" void irq$11();
extern "C" void irq$12();
extern "C" void irq$13();
extern "C" void irq$14();
extern "C" void irq$15();
// Syscall
extern "C" void syscall$128();

/**
 * @brief a class to manage interrupts and init the Interrupt Descriptor Table
 * 
 */
class IDT
{
public:
	/**
	 * @brief Get the Instance object
	 * 
	 * @return IDT* an IDT instance
	 */
    static IDT* getInstance()
    {
		if (!_instance)
			_instance = new IDT();
		return _instance;
    }
	
	/// interrupt handlers
	static int_t* m_interrupt_handlers[NUM_IDT_ENTRIES];

	/**
	 * @brief init the Interrupt Descriptor Table
	 * 
	 */
	void init();
	
	/**
	 * @brief adds irq handler
	 * 
	 * @param num id of the irq
	 * @param handler the handler
	 */
	void register_interrupt_handler(uint8_t num, int_handler_t handler);
	
	/**
	 * @brief unregister all irq handlers
	 * 
	 * @param num the number of the irq
	 */
	void unregister_interrupt_handler(uint8_t num);

private:
	/// IDT instance
    static IDT* _instance;
	/// idt entries
	idt_entry_t m_idt_entries[NUM_IDT_ENTRIES];

	// ctor
	IDT(){ }
	
	/**
	 * @brief set an entry of the idt
	 * 
	 * @param num irq number
	 * @param base base address of the handler
	 * @param sel selection flags
	 * @param type_attr type and attribute flags
	 */
	void setgate(uint8_t num, uint32_t base, uint16_t sel, uint8_t type_attr);
	
};