/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

#pragma once
#include "../../VersionConf.h"
#include <stdint.h>

/**
 * @brief a stucture to hold information about the disk
 * 
 */
typedef struct __attribute__ ((__packed__)) diskinfo
{
	uint8_t calibrationStatus;				// 0x3E
	uint8_t engineStatus;					// 0x3F
	uint8_t engineTimeout;					// 0x40
	uint8_t driveStatus;					// 0x41
	uint8_t disk_floppyStatus;				// 0x42
	uint8_t floppyStatus1;					// 0x43
	uint8_t floppyStatus2;					// 0x44
	uint8_t floppyCylinderNum;				// 0x45
	uint8_t floppyHeadNum;					// 0x46
	uint8_t floppySectorNum;				// 0x47
	uint8_t floppyWrittenBytes;				// 0x48
} diskinfo_t;

/**
 * @brief a stucture to hold information about cursor position of the disk
 * 
 */
typedef struct __attribute__ ((__packed__)) curpos_t
{
	uint8_t column;
	uint8_t row;
} curpos_t;

/**
 * @brief a stucture to hold information about the graphics card
 * 
 */
typedef struct __attribute__ ((__packed__)) graphicinfo
{
	uint8_t 	currentVideoMode;			// 0x49
	uint16_t 	columns;					// 0x4A
	uint16_t 	siteSize;					// 0x4C
	uint16_t 	addrCurrentSite;			// 0x4E
	curpos_t 	cursorpos[8];				// 0x50 - 0x5F (site 0 - 7)
	uint16_t 	cursortype;					// 0x60
	uint8_t 	currenSite;					// 0x62
	uint16_t 	graphicport;				// 0x63
	uint8_t 	modeSelectReg;				// 0x65
	uint8_t 	CGAReg;						// 0x66
} graphicinfo_t;

/**
 * @brief a stucture to hold the BIOS Data Area
 * more info: https://www.lowlevel.eu/wiki/BIOS_Data_Area
 * 
 */
typedef struct __attribute__ ((__packed__)) BDA // 0x400 to 0x4FF
{
	uint16_t 		COMPort[4];				// 0x00 - 0x06
	uint16_t 		LPTPort[3];				// 0x08 - 0x0C
	uint16_t 		ptrEBDA;				// 0x0E
	uint16_t		EQWord;					// 0x10
	uint8_t 		POSTStatus;				// 0x12
	uint16_t 		memsize;				// 0x13
	uint16_t 		POSTSysFlag;			// 0x15
	uint8_t 		KBCFlags[2];			// 0x17 - 0x18
	uint8_t 		alt_num;				// 0x19
	uint16_t 		nextCharKBC;			// 0x1A
	uint16_t 		lastCharKBC;			// 0x1C
	uint8_t			KBCBuff[32];			// 0x1E - 0x3D
	diskinfo_t		DiskInfo;				// 0x3E - 0x48
	graphicinfo_t	GraphicInfo;			// 0x49 - 0x66
	uint32_t		jmpAddrCPUReset;		// 0x67
	uint8_t			cassetteInfo;			// 0x6B (only in AT.. otherwise unused)
	uint32_t		INT1ACounter;			// 0x6C
	uint8_t			counter24h;				// 0x70
	uint8_t			KBCBreakFlag;			// 0x71
	uint16_t		softResetFlag;			// 0x72
	uint32_t		DiskInfo2;				// 0x74
	uint32_t		LPTTimeout;				// 0x78
	uint32_t		COMTimeout;				// 0x7C
	uint16_t		startPS2KBCBuff;		// 0x80
	uint16_t		endPS2KBCBuff;			// 0x82
	uint8_t			GraphicInfo2[7];		// 0x84 - 0x8A
	uint8_t			DiskInfo3[11];			// 0x8B - 0x95 (is this diskinfo_t again?)
	uint16_t		KBCFlags2;				// 0x96 (are this 2 separate bytes again?)
	uint32_t		ptrUserFlag;			// 0x98
	uint32_t		UserTimeCounter;		// 0x9C
	uint8_t			UserTimeFlag;			// 0xA0
	uint8_t			reservedNetwork[7];		// 0xA1 - 0xA7
	uint32_t		GraphicInfo3;			// 0xA8
	uint8_t			reserved[68];			// 0xAC - 0xEF
	uint8_t			appCommunication[16];	// 0xF0 - 0xFF
} BDA_t;

/**
 * @brief a stucture to hold the Extended BIOS Data Area
 * 
 */
typedef struct EBDA // 0x9FC00(ptrEBDA) to 9FFFF
{
	// not standardized :/
} EBDA_t;

// Lower memory
#define IVT_START				0x00000
#define IVT_END					0x003FF
#define BDA_START 				0x00400
#define BDA_END					0x004FF
#define	CONV_FREE_MEM_1_START 	0x00500
#define CONV_FREE_MEM_1_END		0x07BFF
#define BOOT_SECTOR_START 		0x07C00 // typical loc
#define BOOT_SECTOR_END			0x07DFF
#define CONV_FREE_MEM_2_START	0x07E00
#define CONV_FREE_MEM_2_END		0x7FFFF
#define CONV_FREE_MEM_3_START	0x80000
#define CONV_FREE_MEM_3_END		0x9FBFF // depends on ptrEBDA
#define EBDA_START 				0x9FC00 // use ptrEBDA for exact value!
#define EBDA_END				0x9FFFF
#define VID_MEM_ROM_START		0xA0000
#define VID_MEM_ROM_END			0xFFFFF

// Upper memory
#define EXT_MEM_1_START			0x00100000
#define EXT_MEM_1_END			0x00EFFFFF
#define ISA_HOLE_START			0x00F00000
#define ISA_HOLE_END			0x00FFFFFF
#define EXT_MEM_2_START			0x01000000
#define EXT_MEM_2_END			0xBFFFFFFF // ???
#define HARDWARE_USED_START		0xC0000000 // depends on motherboard and devices
#define HARDWARE_USED_END		0xFFFFFFFF

/**
 * @brief a MemoryMap class to manage information from the bios and the current memory state
 * 
 */
class MemoryMap
{
public:
	/**
	 * @brief Get the Instance object
	 * 
	 * @return MemoryMap* a MemoryMap instance
	 */
    static MemoryMap* getInstance()
    {
		if (!_instance)
			_instance = new MemoryMap();
		return _instance;
    }
		
	/**
	 * @brief init the Memory Map
	 * 
	 */
	void init();

	/**
	 * @brief Get Iterrupt Vector Table
	 * 
	 * @param interrupt the interrupt
	 * @return uint16_t* the pointer to the table field
	 */
	uint16_t* getIVTEntry(uint16_t interrupt) { return m_ivt[interrupt]; }
	
	/// BIOS Data Area Pointer
	BDA_t* m_ptrBDA;	
	
	/// Extended BIOS Data Area Pointer
	EBDA_t* m_ptrEBDA;
		
	/// points to end of used memory
	static uintptr_t memoryEnd;
private:
	/// MemoryMap instance
	static MemoryMap* _instance;
	
	/// Real Mode Interrupt Vector Table
    uint16_t m_ivt[256][2];

	/// ctor
	MemoryMap() : m_ptrBDA(nullptr), m_ptrEBDA(nullptr) { }
};