/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

#pragma once
#include "../../VersionConf.h"
#include <stdint.h>

/**
 * @brief a heap block structure
 * 
 */
typedef struct _KHEAPBLOCKBM {
	struct _KHEAPBLOCKBM*		next;	// pointer to next block
	uint32_t					size; 	// size of block
	uint32_t					used; 	// how much is used
	uint32_t					bsize;	// blocksize
    uint32_t					lfb;
} KHEAPBLOCKBM;
 
/**
 * @brief a heap structure
 * 
 */
typedef struct _KHEAPBM {
	KHEAPBLOCKBM	*fblock;
} KHEAPBM;

#define KHEAP_SIZE	0x100000

/**
 * @brief a BitmapHeap implementation to handle memory allocation and deallocation
 * 
 */
class Heap
{
public:	
	/**
	 * @brief sets up the kernel heap
	 * 
	 */
	static void kinit();
	
	/**
	 * @brief init the heap structure
	 * 
	 * @param heap 
	 */
	static void init(KHEAPBM *heap = &m_kheap);
	
	/**
	 * @brief allocate memory
	 * 
	 * @param size size of memory
	 * @param bound boundary of memory
	 * @param heap the heap instance
	 * @return void* a poiter to the allocated memory
	 */
	static void* malloc(uint32_t size, uint8_t bound = 0, KHEAPBM *heap = &m_kheap);
	
	/**
	 * @brief free memory
	 * 
	 * @param ptr a pointer to the memory to be freed
	 * @param heap the heap instance
	 */
	static void free(void *ptr, KHEAPBM *heap = &m_kheap);
	
	/**
	 * @brief add a heap block
	 * 
	 * @param addr start address
	 * @param size size of memory
	 * @param bsize blocksize
	 * @param heap the heap instance
	 */
	static void AddBlock(uintptr_t addr, uint32_t size, uint32_t bsize, KHEAPBM *heap = &m_kheap);

private:
	/// ctor
	Heap();

	/// the kernel heap
	static KHEAPBM m_kheap;
	
	/// find blocks
	static uint8_t GetNID(uint8_t a, uint8_t b);
};