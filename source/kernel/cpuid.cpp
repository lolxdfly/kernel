/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

#include "../VersionConf.h"
#include "include/cpuid.h"
#include "include/video.h"

CPUID* CPUID::_instance = nullptr;

/////////////CPU STRING MAPS/////////////
/* 	Intel Specific brand list */
const char *Intel[] = {
	"Brand ID Not Supported.", 
	"Intel(R) Celeron(R) processor", 
	"Intel(R) Pentium(R) III processor", 
	"Intel(R) Pentium(R) III Xeon(R) processor", 
	"Intel(R) Pentium(R) III processor", 
	"Reserved", 
	"Mobile Intel(R) Pentium(R) III processor-M", 
	"Mobile Intel(R) Celeron(R) processor", 
	"Intel(R) Pentium(R) 4 processor", 
	"Intel(R) Pentium(R) 4 processor", 
	"Intel(R) Celeron(R) processor", 
	"Intel(R) Xeon(R) Processor", 
	"Intel(R) Xeon(R) processor MP", 
	"Reserved", 
	"Mobile Intel(R) Pentium(R) 4 processor-M", 
	"Mobile Intel(R) Pentium(R) Celeron(R) processor", 
	"Reserved", 
	"Mobile Genuine Intel(R) processor", 
	"Intel(R) Celeron(R) M processor", 
	"Mobile Intel(R) Celeron(R) processor", 
	"Intel(R) Celeron(R) processor", 
	"Mobile Geniune Intel(R) processor", 
	"Intel(R) Pentium(R) M processor", 
	"Mobile Intel(R) Celeron(R) processor"
};

/* 	This table is for those brand strings that have two values depending on the processor signature.
	It should have the same number of entries as the above table. */
const char *Intel_Other[] = {
	"Reserved", 
	"Reserved", 
	"Reserved", 
	"Intel(R) Celeron(R) processor", 
	"Reserved", 
	"Reserved", 
	"Reserved", 
	"Reserved", 
	"Reserved", 
	"Reserved", 
	"Reserved", 
	"Intel(R) Xeon(R) processor MP", 
	"Reserved", 
	"Reserved", 
	"Intel(R) Xeon(R) processor", 
	"Reserved", 
	"Reserved", 
	"Reserved", 
	"Reserved", 
	"Reserved", 
	"Reserved", 
	"Reserved", 
	"Reserved", 
	"Reserved"
};
///////////////////////////////////////

void CPUID::printCPUInfo()
{
	getCPUInfo();
	
	// print name
	screen << m_cpu->name << endl;
	
	if(m_cpu->name[0] == 'I') // Intel
	{
		screen << "Type " << m_cpu->type << " - ";
		switch(m_cpu->type)
		{
			case 0:
			screen << "Original OEM";
			break;
			case 1:
			screen << "Overdrive";
			break;
			case 2:
			screen << "Dual-capable";
			break;
			case 3:
			screen << "Reserved";
			break;
		}
		screen << endl;
		screen << "Family " << m_cpu->family << " - ";
		switch(m_cpu->family)
		{
			case 3:
			screen << "i386";
			break;
			case 4:
			screen << "i486";
			break;
			case 5:
			screen << "Pentium";
			break;
			case 6:
			screen << "Pentium Pro";
			break;
			case 15:
			screen << "Pentium 4";
		}
		screen << endl;
		if(m_cpu->family == 15) 
			screen << "Extended family " << m_cpu->extended_family << endl;
		screen << "Model " << m_cpu->model << " - ";
		switch(m_cpu->family)
		{
			// case 3:
			// break;
			case 4:
			switch(m_cpu->model)
			{
				case 0:
				case 1:
				screen << "DX";
				break;
				case 2:
				screen << "SX";
				break;
				case 3:
				screen << "487/DX2";
				break;
				case 4:
				screen << "SL";
				break;
				case 5:
				screen << "SX2";
				break;
				case 7:
				screen << "Write-back enhanced DX2";
				break;
				case 8:
				screen << "DX4";
				break;
			}
			break;
			case 5:
			switch(m_cpu->model)
			{
				case 1:
				screen << "60/66";
				break;
				case 2:
				screen << "75-200";
				break;
				case 3:
				screen << "for 486 system";
				break;
				case 4:
				screen << "MMX";
				break;
			}
			break;
			case 6:
			switch(m_cpu->model)
			{
				case 1:
				screen << "Pentium Pro";
				break;
				case 3:
				screen << "Pentium II Model 3";
				break;
				case 5:
				screen << "Pentium II Model 5/Xeon/Celeron";
				break;
				case 6:
				screen << "Celeron";
				break;
				case 7:
				screen << "Pentium III/Pentium III Xeon - external L2 cache";
				break;
				case 8:
				screen << "Pentium III/Pentium III Xeon - internal L2 cache";
				break;
			}
			break;
			// case 15:
			// break;
		}
		screen << endl;
	}
	else if(m_cpu->name[0] == 'I') // AMD
	{
		screen << "Family: " << m_cpu->family << "Model: " << m_cpu->model << " [";
		switch(m_cpu->family)
		{
			case 4:
			screen << "486 Model " << m_cpu->model;
			break;
			case 5:
			switch(m_cpu->model)
			{
				case 0:
				case 1:
				case 2:
				case 3:
				case 6:
				case 7:
				screen << "K6 Model " << m_cpu->model;
				break;
				case 8:
				screen << "K6-2 Model 8";
				break;
				case 9:
				screen << "K6-III Model 9";
				break;
				default:
				screen << "K5/K6 Model " << m_cpu->model;
				break;
			}
			break;
			case 6:
			switch(m_cpu->model)
			{
				case 1:
				case 2:
				case 4:
				screen << "Athlon Model " << m_cpu->model;
				break;
				case 3:
				screen << "Duron Model 3";
				break;
				case 6:
				screen << "Athlon MP/Mobile Athlon Model 6";
				break;
				case 7:
				screen << "Mobile Duron Model 7";
				break;
				default:
				screen << "Duron/Athlon Model " << m_cpu->model;
				break;
			}
			break;
		}
		screen << ']' << endl;
	}
}

CPU_INFO_t CPUID::getCPUInfo()
{
	if(m_cpu == nullptr)
		detectCPU();
	
	return *m_cpu;
}

void CPUID::detectCPU()
{
	m_cpu = new CPU_INFO_t();
	
	uint32_t ebx, unused;
	cpuid(0, unused, ebx, unused, unused);
	switch(ebx) 
	{
		case MAGIC_INTEL:
			getIntelCPU();
			break;
		case MAGIC_AMD:
			getAMDCPU();
			break;
		default:
			m_cpu->name = "Unknown x86 CPU";
			break;
	}
}

string CPUID::getregs(int eax, int ebx, int ecx, int edx) const
{
	char* ret = new char[17];
	ret[16] = '\0';
	for(int j = 0; j < 4; j++)
	{
		ret[j] = eax >> (8 * j);
		ret[j + 4] = ebx >> (8 * j);
		ret[j + 8] = ecx >> (8 * j);
		ret[j + 12] = edx >> (8 * j);
	}
	return ret;
}


void CPUID::getIntelCPU() const
{
	// set default name
	m_cpu->name = "Intel ";	
	
	// read info
	uint32_t eax, ebx, ecx, edx, unused;	
	cpuid(1, eax, ebx, unused, unused);
	
	// set info to struct
	m_cpu->model = (eax >> 4) & 0xF;
	m_cpu->family = (eax >> 8) & 0xF;
	m_cpu->type = (eax >> 12) & 0x3;
	m_cpu->brand = ebx & 0xFF;
	m_cpu->stepping = eax & 0xF;
	m_cpu->reserved = eax >> 14;
	m_cpu->signature = eax;
	
	if(m_cpu->family == 15) 
		m_cpu->extended_family = (eax >> 20) & 0xFF;

	/* Quok said: If the max extended eax value is high enough to support the processor brand string
	(values 0x80000002 to 0x80000004), then we'll use that information to return the brand information. 
	Otherwise, we'll refer back to the brand tables above for backwards compatibility with older processors. 
	According to the Sept. 2006 Intel Arch Software Developer's Guide, if extended eax values are supported, 
	then all 3 values for the processor brand string are supported, but we'll test just to make sure and be safe. */
	uint32_t max_eax;
	cpuid(0x80000000, max_eax, unused, unused, unused);

	if(max_eax >= 0x80000004) 
	{
		if(max_eax >= 0x80000002)
		{
			cpuid(0x80000002, eax, ebx, ecx, edx);
			m_cpu->name += getregs(eax, ebx, ecx, edx);
		}
		if(max_eax >= 0x80000003)
		{
			cpuid(0x80000003, eax, ebx, ecx, edx);
			m_cpu->name += getregs(eax, ebx, ecx, edx);
		}
		if(max_eax >= 0x80000004)
		{
			cpuid(0x80000004, eax, ebx, ecx, edx);
			m_cpu->name += getregs(eax, ebx, ecx, edx);
		}
	} 
	else if(m_cpu->brand > 0) 
	{
		if(m_cpu->brand < 0x18) 
		{
			if(m_cpu->signature == 0x000006B1 || m_cpu->signature == 0x00000F13) 
				m_cpu->name = Intel_Other[m_cpu->brand];
			else 
				m_cpu->name = Intel[m_cpu->brand];
		}
		else 
			m_cpu->name += "Reserved";
	}
}

void CPUID::getAMDCPU() const
{
	// set default name
	m_cpu->name = "AMD ";
	
	// read info
	uint32_t eax, ebx, ecx, edx, unused, extended;	
	cpuid(1, eax, unused, unused, unused);
	
	// set info to struct
	m_cpu->model = (eax >> 4) & 0xF;
	m_cpu->family = (eax >> 8) & 0xF;
	m_cpu->stepping = eax & 0xF;
	m_cpu->reserved = eax >> 12;
	
	cpuid(0x80000000, extended, unused, unused, unused);
	
	if(extended == 0)
		return;
	
	if(extended >= 0x80000002)
	{
		for(uint32_t j = 0x80000002; j <= 0x80000004; j++)
		{
			cpuid(j, eax, ebx, ecx, edx);
			m_cpu->name += getregs(eax, ebx, ecx, edx);
		}
	}
	if(extended >= 0x80000007)
	{
		cpuid(0x80000007, unused, unused, unused, edx);
		if(edx & 1)
			m_cpu->name += "with Temperature Sensing Diode!";
	}
}
