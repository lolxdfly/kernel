/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

#include "../VersionConf.h"
#include <memory.h>
#include "include/idt.h"
#include "include/port.h"
#include "include/video.h"
#include "include/task.h"
#include "include/gdt.h"
#include "include/syscallmng.h"

IDT* IDT::_instance = nullptr;
int_t* IDT::m_interrupt_handlers[NUM_IDT_ENTRIES];

void IDT::init()
{	
    // Remap the irq table.
	outb(PIC1_COMMAND, ICW1_INIT + ICW1_ICW4);  // write ICW1 to PICM, we are gonna write commands to PICM
	outb(PIC2_COMMAND, ICW1_INIT + ICW1_ICW4);  // write ICW1 to PICS, we are gonna write commands to PICS
	outb(PIC1_DATA, IRQ00);                     // ICW2: Master PIC vector offset (32)
	outb(PIC2_DATA, IRQ08);                     // ICW2: Slave PIC vector offset (40)
	outb(PIC1_DATA, 0x04);                      // ICW3: tell Master PIC that there is a slave PIC at IRQ2 (0000 0100)
	outb(PIC2_DATA, 0x02);                      // ICW3: tell Slave PIC its cascade identity (0000 0010)
	outb(PIC1_DATA, ICW4_8086);                 // write ICW4 to PICM, we are gonna write commands to PICM
	outb(PIC2_DATA, ICW4_8086);                 // write ICW4 to PICS, we are gonna write commands to PICS
	outb(PIC1_DATA, 0x0);                       // enable all IRQs on PICM (no mask)
	outb(PIC2_DATA, 0x0);                       // enable all IRQs on PICS (no mask)
	
	// reset idt entries
	memset((uint8_t *)&m_idt_entries, 0, sizeof(idt_entry_t) * NUM_IDT_ENTRIES);
	
	// ISR
    setgate( ISR00, (uint32_t)isr$0, IDT_SEL_PRIVILEGE_RING0 | IDT_GDT | GDT_ENTRY_1, IDT_PRESENT | IDT_INT_TRAP | IDT_32BIT_INT | IDT_PRIVILEGE_RING3);    // Divide by Zero
    setgate( ISR01, (uint32_t)isr$1, IDT_SEL_PRIVILEGE_RING0 | IDT_GDT | GDT_ENTRY_1, IDT_PRESENT | IDT_INT_TRAP | IDT_32BIT_INT | IDT_PRIVILEGE_RING3);    // Debug
    setgate( ISR02, (uint32_t)isr$2, IDT_SEL_PRIVILEGE_RING0 | IDT_GDT | GDT_ENTRY_1, IDT_PRESENT | IDT_INT_TRAP | IDT_32BIT_INT | IDT_PRIVILEGE_RING3);    // Non-maskable Interrupt
    setgate( ISR03, (uint32_t)isr$3, IDT_SEL_PRIVILEGE_RING0 | IDT_GDT | GDT_ENTRY_1, IDT_PRESENT | IDT_INT_TRAP | IDT_32BIT_INT | IDT_PRIVILEGE_RING3);    // Breakpoint
    setgate( ISR04, (uint32_t)isr$4, IDT_SEL_PRIVILEGE_RING0 | IDT_GDT | GDT_ENTRY_1, IDT_PRESENT | IDT_INT_TRAP | IDT_32BIT_INT | IDT_PRIVILEGE_RING3);    // Overflow
    setgate( ISR05, (uint32_t)isr$5, IDT_SEL_PRIVILEGE_RING0 | IDT_GDT | GDT_ENTRY_1, IDT_PRESENT | IDT_INT_TRAP | IDT_32BIT_INT | IDT_PRIVILEGE_RING3);    // Bound Range Exceeded
    setgate( ISR06, (uint32_t)isr$6, IDT_SEL_PRIVILEGE_RING0 | IDT_GDT | GDT_ENTRY_1, IDT_PRESENT | IDT_INT_TRAP | IDT_32BIT_INT | IDT_PRIVILEGE_RING3);    // Invalid Opcode
    setgate( ISR07, (uint32_t)isr$7, IDT_SEL_PRIVILEGE_RING0 | IDT_GDT | GDT_ENTRY_1, IDT_PRESENT | IDT_INT_TRAP | IDT_32BIT_INT | IDT_PRIVILEGE_RING3);    // Device Not Available
    setgate( ISR08, (uint32_t)isr$8, IDT_SEL_PRIVILEGE_RING0 | IDT_GDT | GDT_ENTRY_1, IDT_PRESENT | IDT_INT_TRAP | IDT_32BIT_INT | IDT_PRIVILEGE_RING3);    // Double Fault
    setgate( ISR09, (uint32_t)isr$9, IDT_SEL_PRIVILEGE_RING0 | IDT_GDT | GDT_ENTRY_1, IDT_PRESENT | IDT_INT_TRAP | IDT_32BIT_INT | IDT_PRIVILEGE_RING3);    // Coprocessor Segment Overrun
    setgate( ISR10, (uint32_t)isr$10, IDT_SEL_PRIVILEGE_RING0 | IDT_GDT | GDT_ENTRY_1, IDT_PRESENT | IDT_INT_TRAP | IDT_32BIT_INT | IDT_PRIVILEGE_RING3);   // Invalid TSS
    setgate( ISR11, (uint32_t)isr$11, IDT_SEL_PRIVILEGE_RING0 | IDT_GDT | GDT_ENTRY_1, IDT_PRESENT | IDT_INT_TRAP | IDT_32BIT_INT | IDT_PRIVILEGE_RING3);   // Segment not present
    setgate( ISR12, (uint32_t)isr$12, IDT_SEL_PRIVILEGE_RING0 | IDT_GDT | GDT_ENTRY_1, IDT_PRESENT | IDT_INT_TRAP | IDT_32BIT_INT | IDT_PRIVILEGE_RING3);   // Stack segment Fault
    setgate( ISR13, (uint32_t)isr$13, IDT_SEL_PRIVILEGE_RING0 | IDT_GDT | GDT_ENTRY_1, IDT_PRESENT | IDT_INT_TRAP | IDT_32BIT_INT | IDT_PRIVILEGE_RING3);   // General Protection Fault
    setgate( ISR14, (uint32_t)isr$14, IDT_SEL_PRIVILEGE_RING0 | IDT_GDT | GDT_ENTRY_1, IDT_PRESENT | IDT_INT_TRAP | IDT_32BIT_INT | IDT_PRIVILEGE_RING3);   // Page Fault
    setgate( ISR15, (uint32_t)isr$15, IDT_SEL_PRIVILEGE_RING0 | IDT_GDT | GDT_ENTRY_1, IDT_PRESENT | IDT_INT_TRAP | IDT_32BIT_INT | IDT_PRIVILEGE_RING3);   // (reserved)
    setgate( ISR16, (uint32_t)isr$16, IDT_SEL_PRIVILEGE_RING0 | IDT_GDT | GDT_ENTRY_1, IDT_PRESENT | IDT_INT_TRAP | IDT_32BIT_INT | IDT_PRIVILEGE_RING3);   // x87 Floating-Point Exception
    setgate( ISR17, (uint32_t)isr$17, IDT_SEL_PRIVILEGE_RING0 | IDT_GDT | GDT_ENTRY_1, IDT_PRESENT | IDT_INT_TRAP | IDT_32BIT_INT | IDT_PRIVILEGE_RING3);   // Alignment Check
    setgate( ISR18, (uint32_t)isr$18, IDT_SEL_PRIVILEGE_RING0 | IDT_GDT | GDT_ENTRY_1, IDT_PRESENT | IDT_INT_TRAP | IDT_32BIT_INT | IDT_PRIVILEGE_RING3);   // Machine Check
    setgate( ISR19, (uint32_t)isr$19, IDT_SEL_PRIVILEGE_RING0 | IDT_GDT | GDT_ENTRY_1, IDT_PRESENT | IDT_INT_TRAP | IDT_32BIT_INT | IDT_PRIVILEGE_RING3);   // SIMD Floating-Point Exception
    setgate( ISR20, (uint32_t)isr$20, IDT_SEL_PRIVILEGE_RING0 | IDT_GDT | GDT_ENTRY_1, IDT_PRESENT | IDT_INT_TRAP | IDT_32BIT_INT | IDT_PRIVILEGE_RING3);   // Virtualization Exception
    setgate( ISR21, (uint32_t)isr$21, IDT_SEL_PRIVILEGE_RING0 | IDT_GDT | GDT_ENTRY_1, IDT_PRESENT | IDT_INT_TRAP | IDT_32BIT_INT | IDT_PRIVILEGE_RING3);   // (reserved)
    setgate( ISR22, (uint32_t)isr$22, IDT_SEL_PRIVILEGE_RING0 | IDT_GDT | GDT_ENTRY_1, IDT_PRESENT | IDT_INT_TRAP | IDT_32BIT_INT | IDT_PRIVILEGE_RING3);   // (reserved)
    setgate( ISR23, (uint32_t)isr$23, IDT_SEL_PRIVILEGE_RING0 | IDT_GDT | GDT_ENTRY_1, IDT_PRESENT | IDT_INT_TRAP | IDT_32BIT_INT | IDT_PRIVILEGE_RING3);   // (reserved)
    setgate( ISR24, (uint32_t)isr$24, IDT_SEL_PRIVILEGE_RING0 | IDT_GDT | GDT_ENTRY_1, IDT_PRESENT | IDT_INT_TRAP | IDT_32BIT_INT | IDT_PRIVILEGE_RING3);   // (reserved)
    setgate( ISR25, (uint32_t)isr$25, IDT_SEL_PRIVILEGE_RING0 | IDT_GDT | GDT_ENTRY_1, IDT_PRESENT | IDT_INT_TRAP | IDT_32BIT_INT | IDT_PRIVILEGE_RING3);   // (reserved)
    setgate( ISR26, (uint32_t)isr$26, IDT_SEL_PRIVILEGE_RING0 | IDT_GDT | GDT_ENTRY_1, IDT_PRESENT | IDT_INT_TRAP | IDT_32BIT_INT | IDT_PRIVILEGE_RING3);   // (reserved)
    setgate( ISR27, (uint32_t)isr$27, IDT_SEL_PRIVILEGE_RING0 | IDT_GDT | GDT_ENTRY_1, IDT_PRESENT | IDT_INT_TRAP | IDT_32BIT_INT | IDT_PRIVILEGE_RING3);   // (reserved)
    setgate( ISR28, (uint32_t)isr$28, IDT_SEL_PRIVILEGE_RING0 | IDT_GDT | GDT_ENTRY_1, IDT_PRESENT | IDT_INT_TRAP | IDT_32BIT_INT | IDT_PRIVILEGE_RING3);   // (reserved)
    setgate( ISR29, (uint32_t)isr$29, IDT_SEL_PRIVILEGE_RING0 | IDT_GDT | GDT_ENTRY_1, IDT_PRESENT | IDT_INT_TRAP | IDT_32BIT_INT | IDT_PRIVILEGE_RING3);   // (reserved)
    setgate( ISR30, (uint32_t)isr$30, IDT_SEL_PRIVILEGE_RING0 | IDT_GDT | GDT_ENTRY_1, IDT_PRESENT | IDT_INT_TRAP | IDT_32BIT_INT | IDT_PRIVILEGE_RING3);   // Security Exception
    setgate( ISR31, (uint32_t)isr$31, IDT_SEL_PRIVILEGE_RING0 | IDT_GDT | GDT_ENTRY_1, IDT_PRESENT | IDT_INT_TRAP | IDT_32BIT_INT | IDT_PRIVILEGE_RING3);   // (reserved)

	// IRQ	
	// PIC1
    setgate( IRQ00, (uint32_t)irq$0, IDT_SEL_PRIVILEGE_RING0 | IDT_GDT | GDT_ENTRY_1, IDT_PRESENT | IDT_INT_TRAP | IDT_32BIT_INT | IDT_PRIVILEGE_RING3);    // Programmable Interval Timer
    setgate( IRQ01, (uint32_t)irq$1, IDT_SEL_PRIVILEGE_RING0 | IDT_GDT | GDT_ENTRY_1, IDT_PRESENT | IDT_INT_TRAP | IDT_32BIT_INT | IDT_PRIVILEGE_RING3);    // PS/2 Keyboard
    setgate( IRQ02, (uint32_t)irq$2, IDT_SEL_PRIVILEGE_RING0 | IDT_GDT | GDT_ENTRY_1, IDT_PRESENT | IDT_INT_TRAP | IDT_32BIT_INT | IDT_PRIVILEGE_RING3);    // Connection to 2nd PIC
    setgate( IRQ03, (uint32_t)irq$3, IDT_SEL_PRIVILEGE_RING0 | IDT_GDT | GDT_ENTRY_1, IDT_PRESENT | IDT_INT_TRAP | IDT_32BIT_INT | IDT_PRIVILEGE_RING3);    // Serial Port(RS-232) Port 2/4
    setgate( IRQ04, (uint32_t)irq$4, IDT_SEL_PRIVILEGE_RING0 | IDT_GDT | GDT_ENTRY_1, IDT_PRESENT | IDT_INT_TRAP | IDT_32BIT_INT | IDT_PRIVILEGE_RING3);    // Serial Port(RS-232) Port 1/3
    setgate( IRQ05, (uint32_t)irq$5, IDT_SEL_PRIVILEGE_RING0 | IDT_GDT | GDT_ENTRY_1, IDT_PRESENT | IDT_INT_TRAP | IDT_32BIT_INT | IDT_PRIVILEGE_RING3);    // Line Printing Terminal 2
    setgate( IRQ06, (uint32_t)irq$6, IDT_SEL_PRIVILEGE_RING0 | IDT_GDT | GDT_ENTRY_1, IDT_PRESENT | IDT_INT_TRAP | IDT_32BIT_INT | IDT_PRIVILEGE_RING3);    // Floppy Disk Controller
    setgate( IRQ07, (uint32_t)irq$7, IDT_SEL_PRIVILEGE_RING0 | IDT_GDT | GDT_ENTRY_1, IDT_PRESENT | IDT_INT_TRAP | IDT_32BIT_INT | IDT_PRIVILEGE_RING3);    // Line Printing Terminal 1 and Spurious Interrupt

	// PIC2
    setgate( IRQ08, (uint32_t)irq$8, IDT_SEL_PRIVILEGE_RING0 | IDT_GDT | GDT_ENTRY_1, IDT_PRESENT | IDT_INT_TRAP | IDT_32BIT_INT | IDT_PRIVILEGE_RING3);    // CMOS Real Time Clock
    setgate( IRQ09, (uint32_t)irq$9, IDT_SEL_PRIVILEGE_RING0 | IDT_GDT | GDT_ENTRY_1, IDT_PRESENT | IDT_INT_TRAP | IDT_32BIT_INT | IDT_PRIVILEGE_RING3);    // free
    setgate( IRQ10, (uint32_t)irq$10, IDT_SEL_PRIVILEGE_RING0 | IDT_GDT | GDT_ENTRY_1, IDT_PRESENT | IDT_INT_TRAP | IDT_32BIT_INT | IDT_PRIVILEGE_RING3);   // 4th ATA/ATAPI/(E)IDE
    setgate( IRQ11, (uint32_t)irq$11, IDT_SEL_PRIVILEGE_RING0 | IDT_GDT | GDT_ENTRY_1, IDT_PRESENT | IDT_INT_TRAP | IDT_32BIT_INT | IDT_PRIVILEGE_RING3);   // 3rd ATA/ATAPI/(E)IDE
    setgate( IRQ12, (uint32_t)irq$12, IDT_SEL_PRIVILEGE_RING0 | IDT_GDT | GDT_ENTRY_1, IDT_PRESENT | IDT_INT_TRAP | IDT_32BIT_INT | IDT_PRIVILEGE_RING3);   // PS/2 Mouse
    setgate( IRQ13, (uint32_t)irq$13, IDT_SEL_PRIVILEGE_RING0 | IDT_GDT | GDT_ENTRY_1, IDT_PRESENT | IDT_INT_TRAP | IDT_32BIT_INT | IDT_PRIVILEGE_RING3);   // Float Processing Unit
    setgate( IRQ14, (uint32_t)irq$14, IDT_SEL_PRIVILEGE_RING0 | IDT_GDT | GDT_ENTRY_1, IDT_PRESENT | IDT_INT_TRAP | IDT_32BIT_INT | IDT_PRIVILEGE_RING3);   // 1st ATA/ATAPI/(E)IDE
    setgate( IRQ15, (uint32_t)irq$15, IDT_SEL_PRIVILEGE_RING0 | IDT_GDT | GDT_ENTRY_1, IDT_PRESENT | IDT_INT_TRAP | IDT_32BIT_INT | IDT_PRIVILEGE_RING3);   // 2nd ATA/ATAPI/(E)IDE

	// Syscalls
    setgate( SYSCALL_INT, (uint32_t)syscall$128, IDT_SEL_PRIVILEGE_RING0 | IDT_GDT | GDT_ENTRY_1, IDT_PRESENT | IDT_INT_TRAP | IDT_32BIT_INT | IDT_PRIVILEGE_RING3); // Syscalls
	
	// flush the idt	
	// amout of mem - 1
    uint16_t len = (sizeof(idt_entry_t) * NUM_IDT_ENTRIES) - 1;
    lidt(&m_idt_entries, len);
	
	// reset interrupt handlers
    memset((uint8_t *)*m_interrupt_handlers, 0, sizeof(int_t) * NUM_IDT_ENTRIES);
	
	// enable interrupts
	asm volatile("sti");
}

void IDT::setgate(uint8_t num, uint32_t base, uint16_t sel, uint8_t type_attr)
{
	// more info of entry-structure: http:// wiki.osdev.org/Interrupts_Descriptor_Table
    m_idt_entries[num].offset_1 = base & 0xFFFF;
    m_idt_entries[num].offset_2 = (base >> 16) & 0xFFFF;

    m_idt_entries[num].selector = sel;
    m_idt_entries[num].zero = 0;

    m_idt_entries[num].type_attr = type_attr;
}

void IDT::register_interrupt_handler(uint8_t num, int_handler_t handler)
{
	// create handler entry
	int_t* i = new int_t();
	i->next = nullptr;
	i->handler = handler;
	
	if(m_interrupt_handlers[num] == nullptr) // first entry
		m_interrupt_handlers[num] = i;
	else
	{
		// insert at end of handler line
		int_t* tmp = m_interrupt_handlers[num];
		while(tmp->next != nullptr) tmp = tmp->next;
		tmp->next = i;
	}
}
void IDT::unregister_interrupt_handler(uint8_t num)
{
	// delete ALL old handlers
	int_t* tmp = IDT::m_interrupt_handlers[num];
	while(tmp != nullptr)
	{
		int_t* t = tmp->next;
		delete tmp;
		tmp = t;
	}
	
    m_interrupt_handlers[num] = nullptr;
}

// Note: handler can access IDT in static way (faster)
// IDT->Init() will flush idt which will enable handler calls => IDT instanced

extern "C" registers_t& isr_handler(registers_t &regs)
{
#ifdef __DEBUG
    screen << "recieved isr:" << (int)regs.intr << " code: " << (int)regs.error << endl;
#endif // __DEBUG

	// call all handlers
	int_t* tmp = IDT::m_interrupt_handlers[regs.intr];
	while(tmp != nullptr)
	{
		tmp->handler(regs);
		tmp = tmp->next;
	}
	
	return regs;
}

extern "C" registers_t& irq_handler(registers_t &regs)
{
    // Send an EOI (end of interrupt) signal to the PICs.
    // If this interrupt involved the slave.
    if (regs.intr >= IRQ00 + 8)
    {
        // Send reset signal to slave.
        outb(PIC2_COMMAND, PIC_EOI);
    }
    // Send reset signal to master. (As well as slave, if necessary).
    outb(PIC1_COMMAND, PIC_EOI);
	
	// call all handlers
	int_t* tmp = IDT::m_interrupt_handlers[regs.intr];
	while(tmp != nullptr)
	{
		tmp->handler(regs);
		tmp = tmp->next;
	}	
	
	// multitasking
	if(regs.intr == IRQ00)
	{
		registers_t& ret = TaskManager::schedule_task(regs);
		GDT::tss[1] = (uint32_t) (&ret + 1);
		return ret;
	}
	
	return regs;
}