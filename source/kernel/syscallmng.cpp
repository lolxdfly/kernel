/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

#include "../VersionConf.h"
#include "include/syscallmng.h"
#include "include/idt.h"

#include "include/video.h"
#include "include/keyboard.h"
#include "include/task.h"

SyscallMng* SyscallMng::_instance = nullptr;

void syscall_handler(registers_t &regs)
{
	switch(regs.eax) // eax is syscallnr
	{
		case SYS_SCREEN_WRITE:
		{
			screen << (const char)regs.ebx;
			break;
		}
			
		case SYS_SCREEN_READ:
		{
			asm volatile("sti"); // tmp reenable interrupts for kbc
			string input = Keyboard::getInstance()->getInput();
			asm volatile("cli"); // disable ints
			regs.eax = (uint32_t)input.c_str();
			break;
		}
		
		case SYS_CREATE_PROC:
		{
			regs.eax = TaskManager::getInstance()->register_user_task((task_func_t)regs.ebx);
			break;
		}
			
		case SYS_KILL_PROC:
		{
			TaskManager::getInstance()->unregister_task((uint16_t)regs.ebx);
			break;
		}
			
		case SYS_GET_TASK_INFO:
		{
			regs.eax = (uint32_t)TaskManager::m_current_task->info;
			break;
		}
		
		default:
		{
			screen << "Unknown syscall: " << (int)regs.eax << endl;
			regs.eax = -1; // possible syscall return -1
			break;
		}
	}
} 

void SyscallMng::init()
{
	// Register our syscall handler
    IDT::getInstance()->register_interrupt_handler(SYSCALL_INT, &syscall_handler);
}
