/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

#include "../VersionConf.h"
#include "include/ps2.h"
#include "include/port.h"
#include "include/panic.h"
#include "include/idt.h"

PS2* PS2::_instance = nullptr;
uint8_t PS2::m_in_port1 = 0;
uint8_t PS2::m_in_port2 = 0;

static void ps2_port1(registers_t &regs)
{
	(void)regs;
	PS2::m_in_port1++;
}

static void ps2_port2(registers_t &regs)
{
	(void)regs;
	PS2::m_in_port2++;
}

void PS2::init()
{
	// 1. Disable USB
	// no USB support yet
	
	// 2. Determine if PS/2 Controller exists
	// no ACPI support yet
	
	// 3. Disable Devices
	outb(PS2_PORT_STAT_CMD, PS2_DISABLE_1ST_PS2);
	outb(PS2_PORT_STAT_CMD, PS2_DISABLE_2ND_PS2);
	
	// 4. Flush Output Buffer
	while (inb(PS2_PORT_STAT_CMD) & PS2_STAT_OUT_FULL)
        inb(PS2_PORT_IO);
	
	// 5. Set Controller Configuration Byte
	outb(PS2_PORT_STAT_CMD, PS2_READ_CONF);
	uint8_t conf = read();
	if(!(conf & PS2_CONF_2ND_CLOCK))
		m_channels = 1;
	
	// 6. Controller Self Test
	outb(PS2_PORT_STAT_CMD, PS2_TEST);
	if(read() != PS2_TEST_SUCESS)
		PANIC("PS/2 Self Test Failed", __FILE__, __FUNCTION__, __LINE__);
	
	// 7. Determine if there are 2 Channels
	if(m_channels == 0)
	{
		outb(PS2_PORT_STAT_CMD, PS2_ENABLE_2ND_PS2);
		outb(PS2_PORT_STAT_CMD, PS2_READ_CONF);
		conf = read();
		if(!(conf & PS2_CONF_2ND_CLOCK))
			m_channels = 1;
		else
			m_channels = 2;
		
		if(m_channels == 2)
			outb(PS2_PORT_STAT_CMD, PS2_DISABLE_2ND_PS2);
			
	}
	
	// 8. Interface Tests
	outb(PS2_PORT_STAT_CMD, PS2_TEST_1ST_PS2);
	if(read())
		PANIC("PS/2 Port 1 Test Failed", __FILE__, __FUNCTION__, __LINE__);
	
	if(m_channels == 2)
	{
		outb(PS2_PORT_STAT_CMD, PS2_TEST_2ND_PS2);
		if(read())
			PANIC("PS/2 Port 2 Test Failed", __FILE__, __FUNCTION__, __LINE__);
	}
	
	// 9. Enable Devices
	outb(PS2_PORT_STAT_CMD, PS2_ENABLE_1ST_PS2);
	outb(PS2_PORT_STAT_CMD, PS2_ENABLE_2ND_PS2);
		
	// register Interrupt Handlers
    IDT::getInstance()->register_interrupt_handler(IRQ01, &ps2_port1);
    IDT::getInstance()->register_interrupt_handler(IRQ12, &ps2_port2);
}

uint8_t PS2::read() const
{
	// clear buff
    while (!(inb(PS2_PORT_STAT_CMD) & PS2_STAT_OUT_FULL)) {}
	
	// return answer
    return inb(PS2_PORT_IO);
}

void PS2::write(uint8_t port, uint8_t data)
{
	// set port
	if(port == 2)
		outb(PS2_PORT_STAT_CMD, PS2_WRITE_2ND_IN_BUFF);
  
	// clear buffer
	while ((inb(PS2_PORT_STAT_CMD) & PS2_STAT_IN_FULL)) {}
	
	// send command
	outb(PS2_PORT_IO, data);
}

int16_t PS2::readDev(uint8_t port)
{
	if(port == 1)
	{
		for(int i = 0; m_in_port1 == 0; i++)
		{
			asm volatile("hlt");
			if(i == 25) // wait for 25 irqs
				return -1;
		}
		m_in_port1--;
		return inb(PS2_PORT_IO);
	}
	else
	{
		for(int i = 0; m_in_port2 == 0; i++)
		{
			asm volatile("hlt");
			if(i == 25) // wait for 25 irqs
				return -1;
		}
		m_in_port2--;
		return inb(PS2_PORT_IO);
	}
}

void PS2::writeDev(uint8_t data)
{
	// clear buffer
	while ((inb(PS2_PORT_STAT_CMD) & PS2_STAT_IN_FULL)) {}
	
	// write command
    outb(PS2_PORT_IO, data);
}

/*
	0xFF		Ancient AT keyboard with translation enabled
	0x00		Standard PS/2 mouse 
	0x03 		Mouse with scroll wheel
	0x04 		5-button mouse
	0x01 | 0x81	MF2 keyboard with translation enabled
	0x83	 	MF2 keyboard
	
	0xFC		Port not supported or PS/2 not initialized
	0xFD		Identify failed
	0xFE		Scan Disable failed
*/
uint8_t PS2::detectDevice(uint8_t port)
{
	// check channel
	if(port > m_channels)
		return -4; // port not supported
	
	// enable port
	outb(PS2_PORT_STAT_CMD, port == 1 ? PS2_ENABLE_1ST_PS2 : PS2_ENABLE_2ND_PS2);

	// disable scanning
	writeDev(PS2_SCAN_DS);
	if(readDev(port) != PS2_ACK)
		return -2; // scan disable failed
	
	// ask for identifikation byte
	writeDev(PS2_IDENTIFY);
	if(readDev(port) != PS2_ACK)
		return -3; // indetify failed
	
	return readDev(port) & readDev(port);
}
