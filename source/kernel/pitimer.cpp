/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

#include "../VersionConf.h"
#include <limits.h>
#include <memory.h>
#include "include/pitimer.h"

PITimer* PITimer::_instance = nullptr;
uint32_t PITimer::m_tick = 0;
pithandler_info_t* PITimer::m_timer_handlers[MAX_TIMER_HANDLER];

static void timer_callback(registers_t &regs)
{
	(void)regs;
	
	if(PITimer::m_tick == UINT_MAX)
		PITimer::m_tick = 0;
		
	for(int i = 0; i < MAX_TIMER_HANDLER; i++)
	{
		if(PITimer::m_timer_handlers[i] != nullptr)
		{
		    pithandler_info_t* pit_inf = PITimer::m_timer_handlers[i];
			if(pit_inf->isCooldown)
			{
				if(pit_inf->time <= PITimer::m_tick)
				{
					pit_inf->handler(pit_inf->time - PITimer::m_tick); // dtime should be 0
					
					delete PITimer::m_timer_handlers[i];
					PITimer::m_timer_handlers[i] = nullptr; // remove handler
				}
			}
			else
			{
				if(pit_inf->nexttick <= PITimer::m_tick)
				{
					pit_inf->handler(PITimer::m_tick - pit_inf->nexttick);
					
					if(PITimer::m_tick >= UINT_MAX - pit_inf->time) // overflow check
						pit_inf->nexttick = pit_inf->time - (INT_MAX - PITimer::m_tick);
					else
						pit_inf->nexttick = PITimer::m_tick + pit_inf->time;
				}
			}
		}
	}
	
	PITimer::m_tick++;
}

void PITimer::init()
{	
    // Firstly, register our timer callback.
    IDT::getInstance()->register_interrupt_handler(IRQ00, &timer_callback);
}

int PITimer::register_timeout_handler(uint32_t time, pitcall_t handler)
{
	// scale time
	time /= PIT_TICK_TIME;
	
	pithandler_info_t* pit_inf = new pithandler_info_t();
	pit_inf->isCooldown = true;
	if(m_tick >= UINT_MAX - time) // overflow check
		pit_inf->time = UINT_MAX - m_tick;
	else
		pit_inf->time = m_tick + time;
	pit_inf->handler = handler;
	// pit_inf->nexttick = -1; // unused
	
	for(int i = 0; i < MAX_TIMER_HANDLER; i++)
	{
		if(m_timer_handlers[i] == nullptr)
		{
			m_timer_handlers[i] = pit_inf;
			return i;
		}
	}
	return -1;
}

int PITimer::register_update_handler(uint32_t dtime, pitcall_t handler)
{
	// scale time
	dtime /= PIT_TICK_TIME;
	
	pithandler_info_t* pit_inf = new pithandler_info_t();
	pit_inf->isCooldown = false;
	pit_inf->time = dtime;
	pit_inf->handler = handler;	
	pit_inf->nexttick = m_tick + dtime;
	
	for(int i = 0; i < MAX_TIMER_HANDLER; i++)
	{
		if(m_timer_handlers[i] == 0)
		{
			m_timer_handlers[i] = pit_inf;
			return i;
		}
	}
	return -1;
}

void PITimer::unregister_handler(int id)
{
	delete PITimer::m_timer_handlers[id];
	PITimer::m_timer_handlers[id] = nullptr;
}