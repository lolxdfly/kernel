::include libc
set CPATH=%CPATH%;%CD%\src\include

::compile all libc files
i686-elf-g++ -c src/memory.cpp -o out/memory.o -D __KERNEL -fstack-protector -fno-use-cxa-atexit -ffreestanding -O2 -Wall -Wextra -fno-exceptions -fno-rtti
i686-elf-g++ -c src/string.cpp -o out/string.o -D __KERNEL -fstack-protector -fno-use-cxa-atexit -ffreestanding -O2 -Wall -Wextra -fno-exceptions -fno-rtti
i686-elf-g++ -c src/stdlib.cpp -o out/stdlib.o -D __KERNEL -fstack-protector -fno-use-cxa-atexit -ffreestanding -O2 -Wall -Wextra -fno-exceptions -fno-rtti
i686-elf-g++ -c src/stdio.cpp -o out/stdio.o -D __KERNEL -fstack-protector -fno-use-cxa-atexit -ffreestanding -O2 -Wall -Wextra -fno-exceptions -fno-rtti

::link into single obj
i686-elf-g++ -r ^
	out/memory.o ^
	out/string.o ^
	out/stdlib.o ^
	out/stdio.o ^
	-o ../out/libkc.a -fstack-protector -fno-use-cxa-atexit -ffreestanding -O2 -nostdlib -lgcc 

PAUSE