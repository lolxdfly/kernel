::include libc
set CPATH=%CPATH%;%CD%\src\include

::compile all libc files
i686-elf-g++ -c src/memory.cpp -o out/memory.o -fstack-protector -fno-use-cxa-atexit -ffreestanding -O2 -Wall -Wextra -fno-exceptions -fno-rtti
i686-elf-g++ -c src/string.cpp -o out/string.o -fstack-protector -fno-use-cxa-atexit -ffreestanding -O2 -Wall -Wextra -fno-exceptions -fno-rtti
i686-elf-g++ -c src/stdlib.cpp -o out/stdlib.o -fstack-protector -fno-use-cxa-atexit -ffreestanding -O2 -Wall -Wextra -fno-exceptions -fno-rtti
i686-elf-g++ -c src/stdio.cpp -o out/stdio.o -fstack-protector -fno-use-cxa-atexit -ffreestanding -O2 -Wall -Wextra -fno-exceptions -fno-rtti

::link into single obj
i686-elf-g++ -r ^
	../out/syscall.o ^
	out/memory.o ^
	out/string.o ^
	out/stdlib.o ^
	out/stdio.o ^
	-o ../out/libc.a -fstack-protector -fno-use-cxa-atexit -ffreestanding -O2 -nostdlib -lgcc 

PAUSE