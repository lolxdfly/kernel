/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

#include "include/memory.h"
#include <stdint.h>

int memcmp(const void* aptr, const void* bptr, size_t size) 
{
	const unsigned char* a = (const unsigned char*) aptr;
	const unsigned char* b = (const unsigned char*) bptr;
	while(size--)
	{
		if (*a != *b)
			return *a - *b;
		else
			a++, b++;
	}
	return 0;
}

void* memcpy(void* __restrict dstptr, const void* __restrict srcptr, size_t size)
{
	unsigned char* dst = (unsigned char*) dstptr;
	const unsigned char* src = (const unsigned char*) srcptr;
	while(size--)
		*dst++ = *src++;
	return dstptr;
}

void* memmove(void* dstptr, const void* srcptr, size_t size)
{
	unsigned char* dst = (unsigned char*) dstptr;
	const unsigned char* src = (const unsigned char*) srcptr;
	if (src < dst) 
	{
        for (dst += size, src += size; size--;)
            *--dst = *--src;
	}
	else 
	{
		while(size--)
			*dst++ = *src++;
	}
	return dstptr;
}

void* memset(void* bufptr, int value, size_t size)
{
	unsigned char* buf = (unsigned char*) bufptr;
	while(size--)
		*buf++ = (unsigned char)value;
	return bufptr;
}

void* memchr(const void *s, int c, size_t n)
{
    unsigned char *p = (unsigned char*)s;
    while( n-- )
        if( *p != (unsigned char)c )
            p++;
        else
            return p;
    return nullptr;
}

// TODO: offer a heap for userspace to allocate memory
extern uint8_t end;
void* malloc(size_t size)
{
	uint32_t tmp = end;
	end += size;
	return (void*)tmp;
}

void free(void* p)
{
	(void)p;
}
