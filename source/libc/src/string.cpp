/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

#include "include/string.h"

size_t strlen(const char* str)
{
    const char *p = str;
    while (*str) ++str;
    return str - p;
}

int strcmp(char *str1, char *str2)
{
    while (*str1 && (*str1 == *str2))
        str1++, str2++;
    return *(const unsigned char*)str1 - *(const unsigned char*)str2;
}

char* strcpy(char *dest, const char *src)
{
    char *ret = dest;
    while ( (*dest++ = *src++) );
    return ret;
}

char* strcat(char *dest, const char *src)
{
    char *ret = dest;
    while (*dest)
        dest++;
    while ( (*dest++ = *src++) );
    return ret;
}

char *strchr(const char *s, int c)
{
    while (*s != (char)c)
        if (!*s++)
            return nullptr;
    return (char *)s;
}
