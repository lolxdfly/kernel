/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

#include "include/stdlib.h"
#ifdef __KERNEL
#include "../../kernel/include/panic.h"
#else
#include "include/stdio.h"
#endif // __KERNEL

__attribute__((__noreturn__)) void abort()
{
#ifdef __KERNEL
	PANIC("abort()", __FILE__, __FUNCTION__, __LINE__);
#else
	printf("abort()\n");
	for(;;);
#endif // __KERNEL
	__builtin_unreachable();
}

const char* itoa(int num, int base)
{
    // Handle 0 explicitely, otherwise empty string is printed for 0
    if (num == 0)
	{
        return "0";
	}
 
	char* str = new char[64]; // max len of 63 chars + '\0'
    int i = 0;
    bool isNegative = false;
    // In standard itoa(), negative numbers are handled only with 
    // base 10. Otherwise numbers are considered unsigned.
    if (num < 0 && base == 10)
    {
        isNegative = true;
        num = -num;
    }
 
    // Process individual digits
    while (num != 0)
    {
        int rem = num % base;
        str[i++] = (rem > 9) ? (rem - 10) + 'A' : rem + '0';
        num /= base;
    }
 
    // If number is negative, append '-'
    if (isNegative)
        str[i++] = '-';
 
    str[i] = '\0'; // Append string terminator
 
    // Reverse the string
	i -= 1; //don't reverse '\0' to start
    for (int j = 0; j < i; j++, i--) 
	{
        char c = str[j];
        str[j] = str[i];
        str[i] = c;
    }
 
    return str;
}

const char* ltoa(long num, int base)
{
    // Handle 0 explicitely, otherwise empty string is printed for 0
    if (num == 0)
	{
        return "0";
	}
 
	char* str = new char[64]; // max len of 63 chars + '\0'
    int i = 0;
    bool isNegative = false;
    // In standard itoa(), negative numbers are handled only with 
    // base 10. Otherwise numbers are considered unsigned.
    if (num < 0 && base == 10)
    {
        isNegative = true;
        num = -num;
    }
 
    // Process individual digits
    while (num != 0)
    {
        long long rem = num % base;
        str[i++] = (rem > 9) ? (rem - 10) + 'A' : rem + '0';
        num /= base;
    }
 
    // If number is negative, append '-'
    if (isNegative)
        str[i++] = '-';
 
    str[i] = '\0'; // Append string terminator
 
    // Reverse the string
	i -= 1; //don't reverse '\0' to start
    for (int j = 0; j < i; j++, i--) 
	{
        char c = str[j];
        str[j] = str[i];
        str[i] = c;
    }
 
    return str;
}

const char* lltoa(long long num, int base)
{
    // Handle 0 explicitely, otherwise empty string is printed for 0
    if (num == 0)
	{
        return "0";
	}
 
	char* str = new char[64]; // max len of 63 chars + '\0'
    int i = 0;
    bool isNegative = false;
    // In standard itoa(), negative numbers are handled only with 
    // base 10. Otherwise numbers are considered unsigned.
    if (num < 0 && base == 10)
    {
        isNegative = true;
        num = -num;
    }
 
    // Process individual digits
    while (num != 0)
    {
        long long rem = num % base;
        str[i++] = (rem > 9) ? (rem - 10) + 'A' : rem + '0';
        num /= base;
    }
 
    // If number is negative, append '-'
    if (isNegative)
        str[i++] = '-';
 
    str[i] = '\0'; // Append string terminator
 
    // Reverse the string
	i -= 1; //don't reverse '\0' to start
    for (int j = 0; j < i; j++, i--) 
	{
        char c = str[j];
        str[j] = str[i];
        str[i] = c;
    }
 
    return str;
}

const char* utoa(unsigned int num, int base)
{
    // Handle 0 explicitely, otherwise empty string is printed for 0
    if (num == 0)
	{
        return "0";
	}
 
	char* str = new char[64]; // max len of 63 chars + '\0'
    int i = 0;
 
    // Process individual digits
    while (num != 0)
    {
        int rem = num % base;
        str[i++] = (rem > 9) ? (rem - 10) + 'A' : rem + '0';
        num /= base;
    }
     str[i] = '\0'; // Append string terminator
 
    // Reverse the string
	i -= 1; //don't reverse '\0' to start
    for (int j = 0; j < i; j++, i--) 
	{
        char c = str[j];
        str[j] = str[i];
        str[i] = c;
    }
 
    return str;
}

int atoi(const char* str)
{
	uint8_t i = 0;
	int n = 0;
	bool isNegative = false;
	if(str[0] == '-')
	{
		isNegative = true;
		i = 1; // skip '-'
	}
	while (str[i] != '\0')
		n = 10 * n + str[i++] - '0';
	if(isNegative)
		n = -n;
	return n;
}

long atol(const char* str)
{
	uint8_t i = 0;
	long n = 0;
	bool isNegative = false;
	if(str[0] == '-')
	{
		isNegative = true;
		i = 1; // skip '-'
	}
	while (str[i] != '\0')
		n = 10 * n + str[i++] - '0';
	if(isNegative)
		n = -n;
	return n;
}

long long atoll(const char* str)
{
	uint8_t i = 0;
	long long n = 0;
	bool isNegative = false;
	if(str[0] == '-')
	{
		isNegative = true;
		i = 1; // skip '-'
	}
	while (str[i] != '\0')
		n = 10 * n + str[i++] - '0';
	if(isNegative)
		n = -n;
	return n;
}

unsigned int atou(const char* str)
{
	uint8_t i = 0;
	unsigned int n = 0;
	while (str[i] != '\0')
		n = 10 * n + str[i++] - '0';
	return n;
}
