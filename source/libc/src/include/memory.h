/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

#pragma once
#include <stddef.h>

/**
 * @brief Compares the first size bytes of the block of memory pointed by aptr to the first size bytes pointed by bptr
 * 
 * @param aptr the pointer to memory block a
 * @param bptr the pointer to memory block b
 * @param size the number of bytes
 * @return 0 if they all match
 * @return int a value different from zero representing which is greater if they do not. 
 */
int memcmp(const void* aptr, const void* bptr, size_t size);

/**
 * @brief copies the values of size bytes from the location pointed to by srcptr directly to the memory block pointed to by dstptr
 * 
 * @param dstptr the destination pointer
 * @param srcptr the source pointer
 * @param size  the number of bytes
 * @return void* a pointer to the memory (same is dstptr)
 */
void* memcpy(void* __restrict dstptr, const void* __restrict srcptr, size_t size);

/**
 * @brief copies the values of size bytes from the location pointed by srcptr to the memory block pointed by dstptr
 * 
 * @param dstptr the destination pointer
 * @param srcptr the source pointer
 * @param size  the number of bytes
 * @return void* a pointer to the memory (same is dstptr)
 */
void* memmove(void* dstptr, const void* srcptr, size_t size);

/**
 * @brief sets the first size bytes of the block of memory pointed by bufptr to the specified value value
 * 
 * @param bufptr the pointer to the desired memory
 * @param value the value which is set
 * @param size the number of bytes which are set
 * @return void* a pointer to the memory (same is bufptr)
 */
void* memset(void* bufptr, int value, size_t size);

/**
 * @brief search c in the first n bytes of s
 * 
 * @param s the memory to be searched in
 * @param c the value to be searched
 * @param n the area where c is searched
 * @return void* a pointer to the location of c or nullptr if c is not found
 */
void* memchr(const void *s, int c, size_t n);

/**
 * @brief allocate memory (tmp implementation!)
 * 
 * @param size the size of the block to be allocated
 * @return void* a pointer to the allocated memory
 */
void* malloc(size_t size);

/**
 * @brief free memory (tmp implementation!)
 * 
 * @param p a pointer to the memory to be freed
 */
void free(void* p);
