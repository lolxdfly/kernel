/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

#pragma once
#include "string.h"
#include <stdint.h>

/**
 * @brief abort the current process and PANIC
 * 
 */
__attribute__((__noreturn__)) void abort();

/**
 * @brief convert an int to a string with given base
 * 
 * @param num the number to be converted
 * @param base the base of the convertion
 * @return const char* the string version of num
 */
const char* itoa(int num, int base = 10);

/**
 * @brief convert a long to a string with given base
 * 
 * @param num the number to be converted
 * @param base the base of the convertion
 * @return const char* the string version of num
 */
const char* ltoa(long num, int base = 10);

/**
 * @brief convert a long long to a string with given base
 * 
 * @param num the number to be converted
 * @param base the base of the convertion
 * @return const char* the string version of num
 */
const char* lltoa(long long num, int base = 10);

/**
 * @brief convert an unsigned int to a string with given base
 * 
 * @param num the number to be converted
 * @param base the base of the convertion
 * @return const char* the string version of num
 */
const char* utoa(unsigned int num, int base = 10);

/**
 * @brief convert a string to an int
 * 
 * @param str the string to be converted
 * @return int the int version of str
 */
int atoi(const char* str);

/**
 * @brief convert a string to a long
 * 
 * @param str the string to be converted
 * @return long the long version of str
 */
long atol(const char* str);

/**
 * @brief convert a string to a long long
 * 
 * @param str the string to be converted
 * @return long long the long long version of str
 */
long long atoll(const char* str);

/**
 * @brief convert a string to an unsigned int
 * 
 * @param str the string to be converted
 * @return unsigned int the unsigned int version of str
 */
unsigned int atou(const char* str);
