/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

#pragma once
#include "string.h"

#define EOF (-1)

/**
 * @brief prints a formated string to the screen
 * 
 * @param format the format string
 * @param ... the params which will be bind to the format string
 * @return int number of written characters
 */
int printf(const char* __restrict format, ...);

/**
 * @brief prints a char to the screen
 * 
 * @param ic the char to be print
 * @return int the printed char (same as ic)
 */
int putchar(int ic);

/**
 * @brief prints a string to the screen
 * 
 * @param str the string to be print
 * @return int number of written characters
 */
int puts(const char* str);