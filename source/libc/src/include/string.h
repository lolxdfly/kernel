/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

#pragma once
#include <stddef.h>

/**
 * @brief get the length of a string
 * 
 * @param str the string
 * @return size_t the length of the string
 */
size_t strlen(const char* str);

/**
 * @brief compare two strings
 * 
 * @param str1 the first string 
 * @param str2 the second string
 * @return -1 if str1 < str2
 * @return  0 if str1 == str2
 * @return  1 if str1 > str2
 */
int strcmp(char *str1, char *str2);

/**
 * @brief copy the NULL-terminated string src into dest
 * 
 * @param dest the destination string
 * @param src the source string
 * @return char* the destination string (same as dest)
 */
char *strcpy(char *dest, const char *src);

/**
 * @brief concatenate the NULL-terminated string src onto the end of dest
 * 
 * @param dest the destination string
 * @param src the source string
 * @return char* the destination string (same as dest)
 */
char *strcat(char *dest, const char *src);

/**
 * @brief finds first occurrence of c in s
 * 
 * @param s the string to be searched in
 * @param c the value to be searched
 * @return char* a pointer to the position of c or nullptr if c not found
 */
char *strchr(const char *s, int c);
