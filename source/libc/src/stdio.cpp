/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

#include "include/stdio.h"
#include <limits.h>
#include <stdarg.h>
/*#ifdef __KERNEL
#include "../../kernel/include/video.h"
#else*/
#include "../../kernel/include/syscall.h"
//#endif // __KERNEL
#include "include/stdlib.h"
#include "include/errno.h"

int putchar(int ic) 
{
	char c = (char)ic;

	// the kernel could write directly to the screen.. however this seems to cause issues
/*#ifdef __KERNEL
	screen.put(c);
#else*/
	syscall_write(c);
//#endif // __KERNEL
	return ic;
}

static bool print(const char* data, size_t length)
{
	const char* bytes = (const char*) data;
	for (size_t i = 0; i < length; i++)
		if (putchar(bytes[i]) == EOF)
			return false;
	return true;
}
 
int printf(const char* __restrict format, ...)
{
	va_list parameters;
	va_start(parameters, format);
 
	int written = 0;
 
	while (*format != '\0') 
	{
		size_t maxrem = INT_MAX - written;
 
		if (format[0] != '%' || format[1] == '%') 
		{
			if (format[0] == '%')
				format++;
			size_t amount = 1;
			while (format[amount] && format[amount] != '%')
				amount++;
			if (maxrem < amount) 
				return EOVERFLOW;
			if (!print(format, amount))
				return -1;
			format += amount;
			written += amount;
			continue;
		}
 
		const char* format_begun_at = format++;
 
		if (*format == 'c') // char
		{
			format++;
			char c = (char) va_arg(parameters, int /* char promotes to int */);
			if (!maxrem)
				return EOVERFLOW;
			if (!print(&c, sizeof(c)))
				return -1;
			written++;
		}
		else if (*format == 's') // string
		{
			format++;
			const char* str = va_arg(parameters, const char*);
			size_t len = strlen(str);
			if (maxrem < len)
				return EOVERFLOW;
			if (!print(str, len))
				return -1;
			written += len;
		}
		else if (*format == 'o' || *format == 'O') // oct
		{
			format++;
			const char* str = itoa(va_arg(parameters, int), 8);
			size_t len = strlen(str);
			if (maxrem < len)
				return EOVERFLOW;
			if (!print(str, len))
				return -1;
			written += len;
		}
		else if (*format == 'i' || *format == 'd') // dec
		{
			format++;
			const char* str = itoa(va_arg(parameters, int));
			size_t len = strlen(str);
			if (maxrem < len)
				return EOVERFLOW;
			if (!print(str, len))
				return -1;
			written += len;
		}
		else if (*format == 'u' || *format == 'U') // unsigned int (dec)
		{
			format++;
			const char* str = utoa(va_arg(parameters, unsigned int));
			size_t len = strlen(str);
			if (maxrem < len)
				return EOVERFLOW;
			if (!print(str, len))
				return -1;
			written += len;
		}
		else if (*format == 'x' || *format == 'X') // hex
		{
			format++;
			const char* str = itoa(va_arg(parameters, int), 16);
			size_t len = strlen(str);
			if (maxrem < len)
				return EOVERFLOW;
			// print 0x
			if(!print("0x", 2))
				return -1;
			written += 2;
			if (!print(str, len))
				return -1;
			written += len;
		}
		else 
		{
			format = format_begun_at;
			size_t len = strlen(format);
			if (maxrem < len)
				return EOVERFLOW;
			if (!print(format, len))
				return -1;
			written += len;
			format += len;
		}
	}
 
	va_end(parameters);
	return written;
}

int puts(const char* str)
{
	return printf("%s\n", str);
}