::include libc
set CPATH=%CPATH%;%CD%\..\libc\src\include

::compile runtime stuff
i686-elf-g++ -c startup.cpp -o out/startup.o -fstack-protector -fno-use-cxa-atexit -ffreestanding -O2 -Wall -Wextra -fno-exceptions -fno-rtti

::compile module "test"
i686-elf-g++ -c test/test.cpp -o out/test.o -fstack-protector -fno-use-cxa-atexit -ffreestanding -O2 -Wall -Wextra -fno-exceptions -fno-rtti
i686-elf-g++ -T module_link.ld -o ../../modules/test.bin -fstack-protector -fno-use-cxa-atexit -ffreestanding -O2 -nostdlib out/startup.o ../out/libc.a out/test.o -lgcc

PAUSE