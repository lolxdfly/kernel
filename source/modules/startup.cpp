/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

#include "../VersionConf.h"
//#include "../kernel/include/panic.h"
#include <memory.h>
#include <stdint.h>
#include <stddef.h>

// Stack Smahing Protector
#if UINT32_MAX == UINTPTR_MAX
#define STACK_CHK_GUARD 0XE2DEE396
#else
#define STACK_CHK_GUARD 0X595E9FBD94FDA766
#endif
 
uintptr_t __stack_chk_guard = STACK_CHK_GUARD;

// TODO: check this.. this seems to work on ubuntu with g++ 8.1
/*__attribute__((noreturn)) //GCC expect somehow __stack_chk_fail_ to be non function
void __stack_chk_fail_()
{
	PANIC("SSP", __FILE__, __FUNCTION__, __LINE__);
	__builtin_unreachable();
}*/

// malloc/free
void *operator new(size_t size)
{
	return malloc(size);
} 
void *operator new[](size_t size)
{
	return malloc(size);
}
void operator delete(void *p)
{
	free(p);
} 
void operator delete[](void *p)
{
	free(p);
}
void operator delete(void *p, size_t size)
{
	(void)size;
    ::operator delete(p);
} 
void operator delete[](void *p, size_t size)
{
	(void)size;
    ::operator delete[](p);
}
