/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

#include <stdint.h>
#include <stdio.h>

int i = 0;
extern "C" void _main()
{
	int j = i + 5;
	for (; i < j; i++)
		printf("%i", i);
}

class test
{
public:
   test();
   ~test();
};

test::test()
{
	i = 1;
	printf("test ctor\n");
}

test::~test()
{
	printf("test dtor\n");
}

test t;
