#!/bin/bash
# setup.sh
# this script starts qemu and loads the kernel
# parameters: d => debug | iso => boot from iso, nomods => load without modules
# v1.0

if [ "$1" == "iso" ]; then
    qemu-system-i386 -boot d -cdrom ISO/bootcd.iso
    exit
fi

if [ "$1" == "d" ]; then
    if [ "$2" == "nomods" ]; then
        i686-elf-tools-linux/bin/i686-elf-gdb -s kernel.sym -ex "target remote | qemu-system-i386 -S -gdb stdio -kernel kernel.bin"
    else
        i686-elf-tools-linux/bin/i686-elf-gdb -s kernel.sym -ex "target remote | qemu-system-i386 -S -gdb stdio -kernel kernel.bin -initrd modules/test.bin,modules/test.bin"
    fi
else
    if [ "$2" == "nomods" ]; then
        qemu-system-i386 -kernel kernel.bin
    else
        qemu-system-i386 -kernel kernel.bin -initrd modules/test.bin,modules/test.bin
    fi
fi
