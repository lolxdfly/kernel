#!/bin/bash
# setup.sh
# this script will setup a working build environment
# v1.0

# versions
BINUTILS_VERSION=2.30
GCC_VERSION=8.1.0
GDB_VERSION=8.1.1

# install dir
export PREFIX="$PWD/i686-elf-tools-linux/"
export TARGET=i686-elf
export PATH="$PREFIX/bin:$PATH"

# installing dependencies
sudo apt update
sudo zapt install build-essential bison flex libgmp3-dev libmpc-dev libmpfr-dev texinfo

# installing qemu
echo -n "Do you wish to install qemu-system-i386? (y/n) "
read answer
if [ "$answer" != "${answer#[Yy]}" ] ;then
    sudo apt install qemu-system-i386
fi

# temp folder for downloading, extracting and compiling
mkdir temp
cd temp

# downloading sources
wget https://ftp.gnu.org/gnu/binutils/binutils-$BINUTILS_VERSION.tar.gz
wget http://ftp.gnu.org/gnu/gcc/gcc-$GCC_VERSION/gcc-$GCC_VERSION.tar.gz
wget https://ftp.gnu.org/gnu/gdb/gdb-$GDB_VERSION.tar.gz

# extracting sources
tar -xzf binutils-$BINUTILS_VERSION.tar.gz
tar -xzf gcc-$GCC_VERSION.tar.gz
tar -xzf gdb-$GDB_VERSION.tar.gz

# compile binutils
cd binutils-$BINUTILS_VERSION
mkdir build
cd build
../configure --target=$TARGET --prefix="$PREFIX" --with-sysroot --disable-nls --disable-werror
make -j$(nproc)
make install
cd ..
cd ..

# compile gcc
# The $PREFIX/bin dir _must_ be in the PATH. We did that above.
which -- $TARGET-as || echo $TARGET-as is not in the PATH
cd gcc-$GCC_VERSION
mkdir build
cd build
../configure --target=$TARGET --prefix="$PREFIX" --disable-nls --enable-languages=c,c++ --without-headers
make all-gcc -j$(nproc)
make all-target-libgcc -j$(nproc)
make install-gcc
make install-target-libgcc
cd ..
cd ..

# compile gdb
cd gdb-$GDB_VERSION
mkdir build
cd build
../configure --target=$TARGET --prefix="$PREFIX" --with-sysroot --disable-nls --disable-werror
make -j$(nproc)
make install
cd ..
cd ..

# delete temp folder
cd ..
rm -rf temp
